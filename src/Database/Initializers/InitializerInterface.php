<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 9/7/16
 * Time: 11:40 AM
 */

namespace Smorken\Ext\Database\Initializers;

interface InitializerInterface
{

    /**
     * @param \PDO $connection
     * @param $config
     * @param $options
     * @return null
     */
    public function init($connection, $config, $options);
}
