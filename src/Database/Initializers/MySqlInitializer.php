<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 9/7/16
 * Time: 11:48 AM
 */

namespace Smorken\Ext\Database\Initializers;

class MySqlInitializer implements InitializerInterface
{

    /**
     * @param \PDO $connection
     * @param $config
     * @param $options
     * @return null
     */
    public function init($connection, $config, $options)
    {
        if (isset($config['unix_socket'])) {
            $connection->exec("use `{$config['database']}`;");
        }

        $collation = $config['collation'];

        // Next we will set the "names" and "collation" on the clients connections so
        // a correct character set will be used by this client. The collation also
        // is set on the server but needs to be set here on this client objects.
        $charset = $config['charset'];

        $names = "set names '$charset'".
            (! is_null($collation) ? " collate '$collation'" : '');

        $connection->prepare($names)->execute();

        // Next, we will check to see if a timezone has been specified in this config
        // and if it has we will issue a statement to modify the timezone with the
        // database. Setting this DB timezone is an optional configuration item.
        if (isset($config['timezone'])) {
            $connection->prepare(
                'set time_zone="'.$config['timezone'].'"'
            )->execute();
        }

        // If the "strict" option has been configured for the connection we will setup
        // strict mode for this session. Strict mode enforces some extra rules when
        // using the MySQL database system and is a quicker way to enforce them.
        if (isset($config['strict'])) {
            if ($config['strict']) {
                $connection->prepare("set session sql_mode='STRICT_ALL_TABLES'")->execute();
            } else {
                $connection->prepare("set session sql_mode=''")->execute();
            }
        }
    }
}
