<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 9/7/16
 * Time: 11:42 AM
 */

namespace Smorken\Ext\Database\Connectors;

use Illuminate\Database\Connectors\Connector;
use Illuminate\Database\Connectors\ConnectorInterface;
use Smorken\Ext\Database\Initializers\InitializerInterface;

class OdbcConnector extends Connector implements ConnectorInterface
{

    /**
     * Establish a database connection.
     *
     * @param  array  $config
     * @return \PDO
     */
    public function connect(array $config)
    {
        $dsn = $this->getDsn($config);
        $options = $this->getOptions($config);

        // We need to grab the PDO options that should be used while making the brand
        // new connection instance. The PDO options control various aspects of the
        // connection's behavior, and some might be specified by the developers.
        $connection = $this->createConnection("odbc:$dsn", $config, $options);

        $initializer = $this->getInitializer($config);
        if ($initializer) {
            $i = new $initializer;
            $i->init($connection, $config, $options);
        }
        return $connection;
    }

    protected function getDsn($config)
    {
        $dsn = $config['dsn'] ?? null;
        if (!$dsn) {
            throw new \InvalidArgumentException("DSN is required for an ODBC connection.");
        }
        return $dsn;
    }

    /**
     * @param $config
     * @return InitializerInterface
     */
    protected function getInitializer($config)
    {
        return $config['initializer'] ?? null;
    }
}
