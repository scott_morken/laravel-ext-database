<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 9/7/16
 * Time: 11:59 AM
 */

namespace Smorken\Ext\Database;

use Illuminate\Database\Connection;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\Schema\Builder as SchemaBuilder;

class OdbcConnection extends Connection implements ConnectionInterface
{

    use QueryTrait;

    /**
     * Create a new database connection instance.
     *
     * @param  \PDO  $pdo
     * @param  string  $database
     * @param  string  $tablePrefix
     * @param  array  $config
     * @return void
     */
    public function __construct(\PDO $pdo, $database = '', $tablePrefix = '', array $config = [])
    {
        parent::__construct($pdo, $database, $tablePrefix, $config);
        $this->setupGrammars($config);
        $this->setupPostProcessor($config);
    }

    /**
     * Get a schema builder instance for the connection.
     *
     * @return \Illuminate\Database\Schema\Builder
     */
    public function getSchemaBuilder()
    {
        if (is_null($this->schemaGrammar)) {
            $this->useDefaultSchemaGrammar();
        }
        $builder = $this->config['schema_builder'] ?? null;
        if ($builder) {
            return new $builder($this);
        }
        return new SchemaBuilder($this);
    }

    public function setupGrammars($config)
    {
        $query = $config['query_grammar'] ?? null;
        if ($query) {
            $this->setQueryGrammar(new $query);
        }
        $schema = $config['schema_grammar'] ?? null;
        if ($schema) {
            $this->setSchemaGrammar(new $schema);
        }
    }

    public function setupPostProcessor($config)
    {
        $proc = $config['post_processor'] ?? null;
        if ($proc) {
            $this->setPostProcessor(new $proc);
        }
    }
}
