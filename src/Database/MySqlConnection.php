<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/8/14
 * Time: 7:02 AM
 */

namespace Smorken\Ext\Database;

use Illuminate\Database\MySqlConnection as Conn;
use Smorken\Ext\Database\Query\Grammars\MySqlGrammar;

class MySqlConnection extends Conn
{

    use QueryTrait;

    /**
     * @return \Illuminate\Database\Grammar|\Smorken\Ext\Database\Query\Grammars\MySqlGrammar
     */
    protected function getDefaultQueryGrammar()
    {
        return $this->withTablePrefix(new MySqlGrammar());
    }

} 
