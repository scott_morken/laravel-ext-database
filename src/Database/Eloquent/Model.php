<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/26/14
 * Time: 3:24 PM
 */

namespace Smorken\Ext\Database\Eloquent;

use Illuminate\Database\Eloquent\Model as EM;
use Illuminate\Support\Facades\Log;
use Smorken\Ext\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray;
use Smorken\Ext\Database\Eloquent\Relations\CompositeBelongsTo;
use Smorken\Ext\Database\Eloquent\Relations\CompositeHasMany;
use Smorken\Ext\Database\Eloquent\Relations\CompositeHasOne;
use Smorken\Ext\Database\Eloquent\Relations\HasManyThroughArray;
use Smorken\Ext\Database\Query\Builder as QueryBuilder;

class Model extends EM
{

    public function allowZeroDates(): ?string
    {
        if ($this->timestamps) {
            return $this->newQuery()->allowZeroDates();
        }
        return null;
    }

    /**
     * Define a many-to-many relationship where the keys are different on all the tables
     *
     * @param  string  $related
     * @param  array  $parentToMid
     * @param  array  $midToFar
     * @param  string  $table
     * @param  string  $relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function belongsToManyArray($related, array $parentToMid, array $midToFar, $table = null, $relation = null)
    {
        // If no relationship name was passed, we will pull backtraces to get the
        // name of the calling function. We will use that function name as the
        // title of this relation since that is a great convention to apply.
        if (is_null($relation)) {
            $relation = $this->guessBelongsToManyRelation();
        }

        // If no table name was provided, we can guess it by concatenating the two
        // models using underscores in alphabetical order. The two model names
        // are transformed to snake case from their default CamelCase also.
        if (is_null($table)) {
            $table = $this->joiningTable($related);
        }

        // Now we're ready to create a new query builder for the related model and
        // the relationship instances for the relation. The relations will set
        // appropriate query constraint and entirely manages the hydrations.
        $instance = $this->newRelatedInstance($related);
        $query = $instance->newQuery();

        return new BelongsToManyArray($query, $this, $table, $parentToMid, $midToFar, $relation);
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @param  string  $related
     * @param  array  $foreignKey
     * @param  array  $ownerKey
     * @param  string  $relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function compositeBelongsTo($related, array $foreignKey, array $ownerKey, $relation = null)
    {
        // If no relation name was given, we will use this debug backtrace to extract
        // the calling method's name and use that as the relationship name as most
        // of the time this will be what we desire to use for the relationships.
        if (is_null($relation)) {
            $relation = $this->guessBelongsToRelation();
        }

        $instance = $this->newRelatedInstance($related);

        // Once we have the foreign key names, we'll just create a new Eloquent query
        // for the related models and returns the relationship instance which will
        // actually be responsible for retrieving and hydrating every relations.
        $query = $instance->newQuery();

        return new CompositeBelongsTo($query, $this, $foreignKey, $ownerKey, $relation);
    }

    /**
     * Define a one-to-many relationship.
     *
     * @param  string  $related
     * @param  array  $foreignKey
     * @param  array  $localKey
     * @return CompositeHasMany
     */
    public function compositeHasMany($related, array $foreignKey, array $localKey)
    {
        $instance = $this->newRelatedInstance($related);

        return new CompositeHasMany($instance->newQuery(), $this, $foreignKey, $localKey);
    }

    /**
     * Define a one-to-one relationship.
     *
     * @param  string  $related
     * @param  array  $foreignKey
     * @param  array  $localKey
     * @return CompositeHasOne
     */
    public function compositeHasOne($related, array $foreignKey, array $localKey)
    {
        $instance = $this->newRelatedInstance($related);

        return new CompositeHasOne($instance->newQuery(), $this, $foreignKey, $localKey);
    }

    /**
     * @param  string|array  $keys
     * @param  array  $data
     * @param  array  $update
     * @return bool
     */
    public function createOrUpdate($keys, array $data, array $update = [])
    {
        $result = false;
        try {
            if ($result = $this->newQuery()->createOrUpdate($keys, $data, $update)) {
                $this->attributes = $data;
                $this->exists = true;
            }
        } catch (\Exception $e) {
            Log::error($e);
        }
        return $result;
    }

    public function dropTemp(string $suffix = '_temp'): void
    {
        $table = sprintf('%s%s', $this->getTable(), $suffix);
        $this->getConnection()->getSchemaBuilder()->dropIfExists($table);
    }

    /**
     * Perform a many through query when the keys are different on all the tables
     * ie: near->some_key => mid->some_other_key AND
     * mid->some_other_key2 => far->some_other_key3
     *
     * @param  string  $related
     * @param  string  $through
     * @param  array  $nearToMid
     * @param  array  $midToFar
     * @return HasManyThroughArray
     */
    public function hasManyThroughArray($related, $through, array $nearToMid, array $midToFar)
    {
        $through = new $through;
        $instance = $this->newRelatedInstance($related);
        return new HasManyThroughArray($instance->newQuery(), $this, $through, $nearToMid, $midToFar);
    }

    public function insertOrUpdateViaTemp(
        array $data,
        array $keys,
        array $insertColumns,
        array $updateColumns = [],
        string $suffix = '_temp'
    ): array {
        $temp = $this->temp($suffix);
        $this->getConnection()->table($temp)->insert($data);
        $updates = $this->newQuery()->updateFromComplex($temp, $keys, $updateColumns);
        $inserts = $this->newQuery()->insertFrom($temp, $keys, $insertColumns);
        return [$inserts, $updates];
    }

    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     * @return \Smorken\Ext\Database\Eloquent\Builder|static
     */
    public function newEloquentBuilder($query)
    {
        return new EloquentBuilder($query);
    }

    public function resetZeroDates(?string $original): void
    {
        if ($original && $this->timestamps) {
            $this->newQuery()->resetZeroDates($original);
        }
    }

    public function temp(string $suffix = '_temp'): string
    {
        $table = sprintf('%s%s', $this->getTable(), $suffix);
        if (!$this->getConnection()->getSchemaBuilder()->hasTable($table)) {
            $this->newQuery()->temp($this->getTable(), $table);
            return $table;
        }
        $this->getConnection()->table($table)->truncate();
        return $table;
    }

    /**
     * Get a new query builder instance for the connection.
     *
     * @return \Smorken\Ext\Database\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {
        $conn = $this->getConnection();

        $grammar = $conn->getQueryGrammar();

        return new QueryBuilder($conn, $grammar, $conn->getPostProcessor());
    }
}
