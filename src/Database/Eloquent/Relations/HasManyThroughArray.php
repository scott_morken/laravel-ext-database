<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/8/16
 * Time: 2:00 PM
 */

namespace Smorken\Ext\Database\Eloquent\Relations;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Expression;

class HasManyThroughArray extends HasManyThrough
{

    /**
     * The distance parent model instance.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $farParent;

    /**
     * The near key to mid key on the relationship.
     *
     * @var array
     */
    protected $firstToMid;

    /**
     * The mid key to far key on the relationship.
     *
     * @var array
     */
    protected $midToFar;

    /**
     * Create a new has many through relationship instance.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $farParent
     * @param  \Illuminate\Database\Eloquent\Model  $parent
     * @param  array  $firstToMid
     * @param  array  $midToFar
     * @return void
     */
    public function __construct(Builder $query, Model $farParent, Model $parent, array $firstToMid, array $midToFar)
    {
        $this->firstToMid = $firstToMid;
        $this->midToFar = $midToFar;
        $this->farParent = $farParent;

        Relation::__construct($query, $parent);
    }

    /**
     * Set the base constraints on the relation query.
     *
     * @return void
     */
    public function addConstraints()
    {
        $parentTable = $this->parent->getTable();

        $localValue = $this->farParent[$this->firstToMid[0]];

        $this->performJoin();

        if (static::$constraints) {
            $this->query->where($parentTable.'.'.$this->firstToMid[1], '=', $localValue);
        }
    }

    /**
     * Set the constraints for an eager load of the relation.
     *
     * @param  array  $models
     * @return void
     */
    public function addEagerConstraints(array $models)
    {
        $table = $this->parent->getTable();
        $keys = $this->getKeys($models, $this->firstToMid[0]);
        $this->query->whereIn($table.'.'.$this->firstToMid[1], $keys);
    }

    /**
     * Find a related model by its primary key.
     *
     * @param  mixed  $id
     * @param  array  $columns
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|null
     */
    public function find($id, $columns = ['*'])
    {
        if (is_array($id) || $id instanceof Arrayable) {
            return $this->findMany($id, $columns);
        }

        $relatedKey = $this->getRelated()->getTable().'.'.$this->midToFar[1];
        $this->where($relatedKey, '=', $id);

        return $this->first($columns);
    }

    /**
     * Find multiple related models by their primary keys.
     *
     * @param  mixed  $ids
     * @param  array  $columns
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findMany($ids, $columns = ['*'])
    {
        $ids = $ids instanceof Arrayable ? $ids->toArray() : $ids;

        if (empty($ids)) {
            return $this->getRelated()->newCollection();
        }

        $relatedKey = $this->getRelated()->getTable().'.'.$this->midToFar[1];
        $this->whereIn($relatedKey, $ids);

        return $this->get($columns);
    }

    /**
     * Get the qualified foreign key on the related model.
     *
     * @return string
     */
    public function getForeignKeyName()
    {
        return $this->related->getTable().'.'.$this->midToFar[1];
    }

    /**
     * Get the key for comparing against the parent key in "has" query.
     *
     * @return string
     */
    public function getHasCompareKey()
    {
        return $this->farParent->getTable().'.'.$this->firstToMid[0];
    }

    /**
     * Add the constraints for a relationship count query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Builder  $parentQuery
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getRelationExistenceCountQuery(Builder $query, Builder $parentQuery)
    {
        $parentTable = $this->parent->getTable();

        $this->performJoin($query);

        $query->select(new Expression('count(*)'));

        $key = $parentTable.'.'.$this->firstToMid[1];

        return $query->whereColumn($this->getHasCompareKey(), '=', $key);
    }

    /**
     * Add the constraints for a relationship query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Builder  $parentQuery
     * @param  array|mixed  $columns
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getRelationExistenceQuery(Builder $query, Builder $parentQuery, $columns = ['*'])
    {
        if ($parentQuery->getQuery()->from === $query->getQuery()->from) {
            return $this->getRelationExistenceQueryForSelfRelation($query, $parentQuery, $columns);
        }

        if ($parentQuery->getQuery()->from === $this->throughParent->getTable()) {
            return $this->getRelationExistenceQueryForThroughSelfRelation($query, $parentQuery, $columns);
        }

        $parentTable = $this->parent->getTable();

        $this->performJoin($query);

        $key = $parentTable.'.'.$this->firstToMid[1];

        return $query->select($columns)->whereColumn($this->getHasCompareKey(), '=', $key);
    }

    /**
     * Get the results of the relationship.
     *
     * @return mixed
     */
    public function getResults()
    {
        return !is_null($this->farParent->{$this->firstToMid[0]})
            ? $this->get()
            : $this->related->newCollection();
    }

    /**
     * Get the qualified foreign key on the "through" model.
     *
     * @return string
     */
    public function getThroughKey()
    {
        return $this->parent->getTable().'.'.$this->firstToMid[1];
    }

    /**
     * Match the eagerly loaded results to their parents.
     * Results are far with attached mid_ids from pivot table, Models are near
     *
     * @param  array  $models
     * @param  \Illuminate\Database\Eloquent\Collection  $results
     * @param  string  $relation
     * @return array
     */
    public function match(array $models, Collection $results, $relation)
    {
        $dictionary = $this->buildDictionaryKeyed($results, $this->firstToMid[1]);

        // Once we have the dictionary we can simply spin through the parent models to
        // link them up with their children using the keyed dictionary to make the
        // matching very convenient and easy work. Then we'll just return them.
        $key = $this->firstToMid[0];
        foreach ($models as $model) {
            $k = $model->{$key};

            if (isset($dictionary[$k])) {
                $value = $this->related->newCollection($dictionary[$k]);

                $model->setRelation($relation, $value);
            } else {
                $model->setRelation($relation, $this->related->newCollection([]));
            }
        }

        return $models;
    }

    /**
     * Determine whether close parent of the relation uses Soft Deletes.
     *
     * @return bool
     */
    public function throughParentSoftDeletes()
    {
        return in_array(SoftDeletes::class, class_uses_recursive(get_class($this->parent)));
    }

    /**
     * Build model dictionary keyed by the relation's foreign key.
     *
     * @param  \Illuminate\Database\Eloquent\Collection  $results
     * @return array
     */
    protected function buildDictionaryKeyed(Collection $results, $key)
    {
        $dictionary = [];

        // First we will create a dictionary of models keyed by the foreign key of the
        // relationship as this will allow us to quickly access all of the related
        // models without having to do nested looping which will be quite slow.
        foreach ($results as $result) {
            $subkey = $result->getKey();
            if ($subkey) {
                $dictionary[$result->{$key}][$subkey] = $result;
            } else {
                $dictionary[$result->{$key}][] = $result;
            }
        }

        return $dictionary;
    }

    /**
     * Set the join clause on the query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder|null  $query
     * @return void
     */
    protected function performJoin(Builder $query = null)
    {
        $query = $query ?: $this->query;
        $sub = $this->parent->newQuery();
        $foreignKey = $this->related->getTable().'.'.$this->midToFar[1];
        $midToFarKey = $this->parent->getTable().'.'.$this->midToFar[0];

        $sub->select($this->firstToMid[1], $this->midToFar[0])
            ->distinct();
        $subsql = sprintf('(%s) %s', $sub->toSql(), $this->parent->getTable());
        $query->join(new Expression($subsql), $midToFarKey, '=', $foreignKey);

        if ($this->throughParentSoftDeletes()) {
            $query->withGlobalScope('SoftDeletableHasManyThrough', function ($query) {
                $query->whereNull($this->parent->getQualifiedDeletedAtColumn());
            });
        }
    }

    /**
     * Set the select clause for the relation query.
     *
     * @param  array  $columns
     * @return array
     */
    protected function shouldSelect(array $columns = ['*'])
    {
        if ($columns == ['*']) {
            $columns = [$this->related->getTable().'.*'];
        }

        return array_merge($columns,
            [
                $this->parent->getTable().'.'.$this->firstToMid[1],
                $this->parent->getTable().'.'.$this->midToFar[0],
            ]
        );
    }
}
