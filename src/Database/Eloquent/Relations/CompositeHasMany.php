<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/5/16
 * Time: 12:20 PM
 */

namespace Smorken\Ext\Database\Eloquent\Relations;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Expression;

class CompositeHasMany extends HasMany
{

    use CompositeTrait;

    /**
     * @var array
     */
    protected $foreignKey = [];

    /**
     * @var array
     */
    protected $localKey = [];

    /**
     * Create a new belongs to relationship instance.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $parent
     * @param  array  $foreignKey
     * @param  array  $localKey
     * @return void
     */
    public function __construct(Builder $query, Model $parent, array $foreignKey, array $localKey)
    {
        $this->verifyKeyLengthsMatch($foreignKey, $localKey);
        $this->foreignKey = $foreignKey;
        $this->localKey = $localKey;
        Relation::__construct($query, $parent);
    }

    /**
     * Set the base constraints on the relation query.
     *
     * @return void
     */
    public function addConstraints()
    {
        if (static::$constraints) {
            $this->getWhereForValues($this->getNestedWhere(), $this->parent, $this->localKey, $this->foreignKey);
        }
    }

    /**
     * Set the constraints for an eager load of the relation.
     *
     * @param  array  $models
     * @return void
     */
    public function addEagerConstraints(array $models)
    {
        $models = $this->getEagerModelKeys($models);
        foreach ($models as $model) {
            $this->getWhereForValues($this->getNestedWhere(), $model, $this->localKey, $this->foreignKey);
        }
    }

    /**
     * Create a new instance of the related model.
     *
     * @param  array  $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $attributes = [])
    {
        // Here we will set the raw attributes to avoid hitting the "fill" method so
        // that we do not have to worry about a mass accessor rules blocking sets
        // on the models. Otherwise, some of these attributes will not get set.
        $instance = $this->related->newInstance($attributes);

        foreach ($this->localKey as $i => $localKey) {
            $val = $this->parent->$localKey;
            $instance->setAttribute($this->foreignKey[$i], $val);
        }

        $instance->save();

        return $instance;
    }

    /**
     * Find a model by its primary key or return new instance of the related model.
     *
     * @param  mixed  $id
     * @param  array  $columns
     * @return \Illuminate\Support\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function findOrNew($id, $columns = ['*'])
    {
        if (is_null($instance = $this->addNestedWhereQuery($this->getNestedWhere()
                                                                ->getQuery(), 'or')
                                     ->find($id, $columns))) {
            $instance = $this->related->newInstance();
            foreach ($this->localKey as $i => $localKey) {
                $val = $this->parent->$localKey;
                $instance->setAttribute($this->foreignKey[$i], $val);
            }
        }

        return $instance;
    }

    /**
     * Get the first related model record matching the attributes or instantiate it.
     *
     * @param  array  $attributes
     * @param  array  $values
     * @return Model
     */
    public function firstOrNew(array $attributes = [], array $values = [])
    {
        if (is_null($instance = $this->addNestedWhereQuery($this->getNestedWhere()
                                                                ->getQuery(), 'or')
                                     ->where($attributes)
                                     ->first())) {
            $instance = $this->related->newInstance($attributes);
            foreach ($this->localKey as $i => $localKey) {
                $val = $this->parent->$localKey;
                $instance->setAttribute($this->foreignKey[$i], $val);
            }
        }

        return $instance;
    }

    /**
     * Add the constraints for a relationship count query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Builder  $parent
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getRelationCountQuery(Builder $query, Builder $parent)
    {
        if ($parent->getQuery()->from === $query->getQuery()->from) {
            return $this->getRelationCountQueryForSelfRelation($query, $parent);
        }
        $query->select(new Expression('count(*)'));
        return $this->getWhereForColumns($query, $this->localKey, $this->foreignKey);
    }

    /**
     * Add the constraints for a relationship count query on the same table.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Builder  $parent
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getRelationCountQueryForSelfRelation(Builder $query, Builder $parent)
    {
        $query->select(new Expression('count(*)'));

        $query->from($query->getModel()
                           ->getTable().' as '.$hash = $this->getRelationCountHash());

        return $this->getWhereForColumns($query, $this->localKey, $this->foreignKey, $hash);
    }

    /**
     * Add the constraints for a relationship query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Builder  $parentQuery
     * @param  array|mixed  $columns
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getRelationExistenceQuery(Builder $query, Builder $parentQuery, $columns = ['*'])
    {
        if ($query->getQuery()->from == $parentQuery->getQuery()->from) {
            return $this->getRelationExistenceQueryForSelfRelation($query, $parentQuery, $columns);
        }
        $query->select($columns);
        return $this->getWhereForColumns($query, $this->localKey, $this->foreignKey);
    }

    /**
     * Add the constraints for a relationship query on the same table.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Builder  $parentQuery
     * @param  array|mixed  $columns
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getRelationExistenceQueryForSelfRelation(Builder $query, Builder $parentQuery, $columns = ['*'])
    {
        $query->from($query->getModel()
                           ->getTable().' as '.$hash = $this->getRelationCountHash());

        $query->getModel()
              ->setTable($hash);

        return $this->getWhereForColumns($query, $this->localKey, $this->foreignKey);
    }

    /**
     * Get the results of the relationship.
     *
     * @return mixed
     */
    public function getResults()
    {
        return $this->query->addNestedWhereQuery($this->getNestedWhere(true)
                                                      ->getQuery(), 'or')
                           ->get();
    }

    /**
     * Attach a model instance to the parent model.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return \Illuminate\Database\Eloquent\Model|false
     */
    public function save(Model $model)
    {
        foreach ($this->localKey as $i => $localKey) {
            $val = $this->parent->getAttribute($localKey);
            $model->setAttribute($this->foreignKey[$i], $val);
        }
        return $model->save() ? $model : false;
    }

    /**
     * Perform an update on all the related models.
     *
     * @param  array  $attributes
     * @return int
     */
    public function update(array $attributes)
    {
        if ($this->related->usesTimestamps()) {
            $attributes[$this->relatedUpdatedAt()] = $this->related->freshTimestampString();
        }

        return $this->query->addNestedWhereQuery($this->getNestedWhere()
                                                      ->getQuery(), 'or')
                           ->update($attributes);
    }

    /**
     * Build model dictionary keyed by the relation's foreign key.
     *
     * @param  \Illuminate\Database\Eloquent\Collection  $results
     * @return array
     */
    protected function buildDictionary(Collection $results)
    {
        $dictionary = [];

        // First we will create a dictionary of models keyed by the foreign key of the
        // relationship as this will allow us to quickly access all of the related
        // models without having to do nested looping which will be quite slow.
        foreach ($results as $result) {
            $id = $this->getIdentifierFromModel($this->foreignKey, $result);
            $dictionary[$id][] = $result;
        }

        return $dictionary;
    }

    /**
     * Gather the keys from an array of related models.
     *
     * @param  array  $models
     * @return array
     */
    protected function getEagerModelKeys(array $models)
    {
        $keys = [];

        // First we need to gather all of the keys from the parent models so we know what
        // to query for via the eager loading query. We will add them to an array then
        // execute a "where in" statement to gather up all of those related records.
        foreach ($models as $model) {
            $id = $this->getIdentifierFromModel($this->localKey, $model);
            if ($id) {
                $keys[(string) $id] = $model;
            }
        }

        return $keys;
    }

    /**
     * Match the eagerly loaded results to their many parents.
     *
     * @param  array  $models
     * @param  \Illuminate\Database\Eloquent\Collection  $results
     * @param  string  $relation
     * @param  string  $type
     * @return array
     */
    protected function matchOneOrMany(array $models, Collection $results, $relation, $type)
    {
        $dictionary = $this->buildDictionary($results);

        // Once we have the dictionary we can simply spin through the parent models to
        // link them up with their children using the keyed dictionary to make the
        // matching very convenient and easy work. Then we'll just return them.
        foreach ($models as $model) {
            $parent_id = $this->getIdentifierFromModel($this->localKey, $model);
            if (isset($dictionary[$parent_id])) {
                $value = $this->getRelationValue($dictionary, $parent_id, $type);

                $model->setRelation($relation, $value);
            }
        }

        return $models;
    }
}
