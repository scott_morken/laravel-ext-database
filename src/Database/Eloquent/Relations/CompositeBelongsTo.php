<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/5/16
 * Time: 10:14 AM
 */

namespace Smorken\Ext\Database\Eloquent\Relations;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Expression;

class CompositeBelongsTo extends BelongsTo
{

    use CompositeTrait;

    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $child;

    /**
     * @var array
     */
    protected $foreignKey = [];

    /**
     * @var array
     */
    protected $ownerKey = [];

    /**
     * Create a new belongs to relationship instance.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $child
     * @param  array  $foreignKey
     * @param  array  $ownerKey
     * @param $relationName
     */
    public function __construct(Builder $query, Model $child, array $foreignKey, array $ownerKey, $relationName)
    {
        $this->verifyKeyLengthsMatch($foreignKey, $ownerKey);
        $this->ownerKey = $ownerKey;
        $this->foreignKey = $foreignKey;
        $this->relationName = $relationName;
        $this->child = $child;
        Relation::__construct($query, $child);
    }

    /**
     * Set the base constraints on the relation query.
     *
     * @return void
     */
    public function addConstraints()
    {
        if (static::$constraints) {
            // For belongs to relationships, which are essentially the inverse of has one
            // or has many relationships, we need to actually query on the primary key
            // of the related models matching on the foreign key that's on a parent.

            $this->getWhereForValues($this->getNestedWhere(), $this->child, $this->foreignKey, $this->ownerKey);
        }
    }

    /**
     * Set the constraints for an eager load of the relation.
     *
     * @param  array  $models
     * @return void
     */
    public function addEagerConstraints(array $models)
    {
        // We'll grab the primary key name of the related models since it could be set to
        // a non-standard name and not "id". We will then construct the constraint for
        // our eagerly loading query so it returns the proper models from execution.
        foreach ($models as $model) {
            $this->getWhereForValues($this->getNestedWhere(), $model, $this->foreignKey, $this->ownerKey);
        }
    }

    /**
     * Associate the model instance to the given parent.
     *
     * @param  \Illuminate\Database\Eloquent\Model|int  $model
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function associate($model)
    {
        foreach ($this->ownerKey as $i => $otherKey) {
            if (!$model instanceof Model) {
                $model = $this->related->newQuery()
                                       ->findOrFail($model);
            }
            $otherVal = $model->getAttribute($otherKey);
            $this->child->setAttribute($this->foreignKey[$i], $otherVal);
        }
        if ($model instanceof Model) {
            $this->child->setRelation($this->relationName, $model);
        }

        return $this->child;
    }

    /**
     * Dissociate previously associated model from the given parent.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function dissociate()
    {
        foreach ($this->foreignKey as $i => $foreignKey) {
            $this->child->setAttribute($foreignKey, null);
        }
        return $this->child->setRelation($this->relationName, null);
    }

    /**
     * Add the constraints for a relationship count query on the same table.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Builder  $parent
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getRelationCountQueryForSelfRelation(Builder $query, Builder $parent)
    {
        $query->select(new Expression('count(*)'));

        $query->from($query->getModel()
                           ->getTable().' as '.$hash = $this->getRelationCountHash());

        $query->getModel()
              ->setTable($hash);

        return $this->getWhereForColumns($query, $this->foreignKey, $this->ownerKey, $hash);
    }

    /**
     * Add the constraints for a relationship count query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Builder  $parent
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getRelationExistenceCountQuery(Builder $query, Builder $parent)
    {
        if ($parent->getQuery()->from == $query->getQuery()->from) {
            return $this->getRelationCountQueryForSelfRelation($query, $parent);
        }

        $query->select(new Expression('count(*)'));

        return $this->getWhereForColumns($query, $this->foreignKey, $this->ownerKey);
    }

    /**
     * Add the constraints for a relationship query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Builder  $parentQuery
     * @param  array|mixed  $columns
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getRelationExistenceQuery(Builder $query, Builder $parentQuery, $columns = ['*'])
    {
        if ($parentQuery->getQuery()->from == $query->getQuery()->from) {
            return $this->getRelationExistenceQueryForSelfRelation($query, $parentQuery, $columns);
        }

        return $this->getWhereForColumns($query, $this->foreignKey, $this->ownerKey);
    }

    /**
     * Add the constraints for a relationship query on the same table.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Builder  $parentQuery
     * @param  array|mixed  $columns
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getRelationExistenceQueryForSelfRelation(Builder $query, Builder $parentQuery, $columns = ['*'])
    {
        $query->select($columns)
              ->from($query->getModel()
                           ->getTable().' as '.$hash = $this->getRelationCountHash());

        $query->getModel()
              ->setTable($hash);

        return $this->getWhereForColumns($query, $this->foreignKey, $this->ownerKey);
    }

    /**
     * Get the results of the relationship.
     *
     * @return mixed
     */
    public function getResults()
    {
        return $this->query->addNestedWhereQuery($this->getNestedWhere(true)
                                                      ->getQuery(), 'or')
                           ->first();
    }

    /**
     * Match the eagerly loaded results to their parents.
     *
     * @param  array  $models
     * @param  \Illuminate\Database\Eloquent\Collection  $results
     * @param  string  $relation
     * @return array
     */
    public function match(array $models, Collection $results, $relation)
    {
        $foreign = $this->foreignKey;

        $other = $this->ownerKey;

        // First we will get to build a dictionary of the child models by their primary
        // key of the relationship, then we can easily match the children back onto
        // the parents using that dictionary and the primary key of the children.
        $dictionary = [];

        foreach ($results as $result) {
            $relation_id = $this->getIdentifierFromModel($other, $result);
            $dictionary[$relation_id] = $result;
        }

        // Once we have the dictionary constructed, we can loop through all the parents
        // and match back onto their children using these keys of the dictionary and
        // the primary key of the children to map them onto the correct instances.
        foreach ($models as $model) {
            $parent_id = $this->getIdentifierFromModel($foreign, $model);
            if ($parent_id && isset($dictionary[$parent_id])) {
                $model->setRelation($relation, $dictionary[$parent_id]);
            }
        }

        return $models;
    }

    /**
     * Update the parent model on the relationship.
     *
     * @param  array  $attributes
     * @return mixed
     */
    public function update(array $attributes)
    {
        $instance = $this->getResults();

        return $instance->fill($attributes)
                        ->save();
    }
}
