<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/5/16
 * Time: 10:41 AM
 */

namespace Smorken\Ext\Database\Eloquent\Relations;

use Smorken\Ext\Database\Eloquent\Builder;
use Smorken\Ext\Database\Eloquent\ExpressionValue;

trait CompositeTrait
{

    /**
     * @var Builder
     */
    protected $nested_where;

    /**
     * Get the relationship for eager loading.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getEager()
    {
        return $this->query->addNestedWhereQuery($this->getNestedWhere(true)
                                                      ->getQuery())
                           ->get();
    }

    /**
     * Get the SQL representation of the query.
     *
     * @return string
     */
    public function toSql()
    {
        return $this->query->addNestedWhereQuery($this->getNestedWhere(true)
                                                      ->getQuery())
                           ->toSql();
    }

    protected function getExpressionOrString($key, $table = null, $qualify = false)
    {
        if ($key instanceof ExpressionValue) {
            return $key->getColumn();
        }
        if (!is_null($table)) {
            return ($qualify ? $this->getQualifiedKeyName($table, $key) : sprintf('%s.%s', $table, $key));
        }
        return $key;
    }

    protected function getIdentifierFromModel($keys, $model, $seperator = '')
    {
        $vals = $this->getValuesFromModel($keys, $model);
        return implode($seperator, $vals);
    }

    protected function getNestedWhere($executing = false)
    {
        if ($executing && !$this->nested_where) {
            throw new \OutOfBoundsException('No relations bound, cannot execute query.');
        }
        if (!$this->nested_where && !$executing) {
            $this->nested_where = $this->related->newQuery();
        }
        return $this->nested_where;
    }

    protected function getQualifiedKeyName($table, $key)
    {
        return $table.'.'.$key;
    }

    protected function getValueFromModel($key, $model)
    {
        if ($key instanceof ExpressionValue) {
            return $key->getValue($model);
        }
        return $model->$key;
    }

    protected function getValuesFromModel($keys, $model)
    {
        $vals = [];
        foreach ($keys as $k) {
            $vals[] = $this->getValueFromModel($k, $model);
        }
        return $vals;
    }

    protected function getWhereForColumns($query, $parent_keys, $related_keys, $related_table = null)
    {
        if (is_null($related_table)) {
            $related_table = $this->related->getTable();
        }
        return $query->where(function ($q) use ($related_table, $parent_keys, $related_keys) {
            $parent_table = $this->parent->getTable();
            foreach ($related_keys as $i => $otherKey) {
                $otherKey = $this->getExpressionOrString($otherKey, $related_table, true);
                $foreignKey = $this->getExpressionOrString($parent_keys[$i], $parent_table);
                $q->whereColumn($foreignKey, '=', $otherKey);
            }
        });
    }

    protected function getWhereForValues($query, $model, $parent_keys, $related_keys, $type = 'orWhere')
    {
        return $query->$type(function ($q) use ($model, $parent_keys, $related_keys) {
            $related_table = $this->related->getTable();
            $null_count = 0;
            foreach ($related_keys as $i => $otherKey) {
                $otherKey = $this->getExpressionOrString($otherKey, $related_table);
                $val = $this->getValueFromModel($parent_keys[$i], $model);
                $q->where($otherKey, '=', $val);
                if (is_null($val)) {
                    $null_count++;
                }
            }
            if ($null_count === count($parent_keys)) {
                throw new \OutOfBoundsException('No relation appears to have been loaded, check your keys.');
            }
        });
    }

    /**
     * @param $key1
     * @param $key2
     * @throws \InvalidArgumentException
     */
    protected function verifyKeyLengthsMatch($key1, $key2)
    {
        if (count($key1) !== count($key2)) {
            throw new \InvalidArgumentException('Number of columns in composite keys must match.');
        }
    }
}
