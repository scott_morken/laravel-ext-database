<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/26/16
 * Time: 10:57 AM
 */

namespace Smorken\Ext\Database\Eloquent;

use Illuminate\Database\Query\Expression;

class ExpressionValue
{

    /**
     * @var string
     */
    protected $column;
    /**
     * @var Expression
     */
    protected $expression;
    /**
     * @var null|\Closure
     */
    protected $value;

    /**
     * ExpressionValue constructor.
     * @param string $column
     * @param Expression|null $expression
     * @param null|\Closure $value
     */
    public function __construct($column, $expression = null, \Closure $value = null)
    {
        $this->column = $column;
        $this->expression = $expression;
        $this->value = $value;
    }

    public function getColumnName()
    {
        return $this->column;
    }

    /**
     * @return Expression|string
     */
    public function getColumn()
    {
        return $this->expression ?: $this->column;
    }

    public function getValue($model)
    {
        if (is_null($this->value)) {
            return $this->getActualValue($model);
        }
        return $this->evaluateFromClosure($model);
    }

    protected function getActualValue($model)
    {
        return $model->{$this->column};
    }

    protected function evaluateFromClosure($model)
    {
        return call_user_func($this->value, $this, $model);
    }

    protected function substr($model)
    {
        $v = $this->getActualValue($model);
        $args = $this->args;
        array_unshift($args, $v);
        return call_user_func_array('substr', $args);
    }
}
