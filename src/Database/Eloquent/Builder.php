<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/28/14
 * Time: 8:56 AM
 */

namespace Smorken\Ext\Database\Eloquent;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;

class Builder extends EloquentBuilder
{

    /**
     * The methods that should be returned from query builder.
     *
     * @var array
     */
    protected $passthru = [
        'average',
        'avg',
        'count',
        'dd',
        'doesntExist',
        'dump',
        'exists',
        'getBindings',
        'getConnection',
        'getGrammar',
        'insert',
        'insertGetId',
        'insertOrIgnore',
        'insertUsing',
        'max',
        'min',
        'raw',
        'sum',
        'toSql',
        'createOrUpdate',
        'concatenate',
        'temp',
        'updateFromComplex',
        'insertFrom',
        'allowZeroDates',
        'resetZeroDates',
    ];

    public function createOrUpdate($keys, array $values, array $update = [])
    {
        [$values, $update] = $this->addTimestampColumns($values, $update);
        return $this->query->createOrUpdate($keys, $values, $update);
    }

    protected function addTimestampColumns(array $values, array $update)
    {
        if (!$this->model->usesTimestamps()) {
            return [$values, $update];
        }
        $new_values = [];
        $createdcolumn = $this->model->getCreatedAtColumn();
        $updatedcolumn = $this->model->getUpdatedAtColumn();
        if (!in_array($updatedcolumn, $update, true)) {
            $update[] = $updatedcolumn;
        }
        foreach ($values as $value) {
            $updated = $value[$updatedcolumn] ?? $this->model->freshTimestampString();
            $value[$updatedcolumn] = $updated;
            $created = $value[$createdcolumn] ?? $this->model->freshTimestampString();
            $value[$createdcolumn] = $created;
            $new_values[] = $value;
        }
        return [$new_values, $update];
    }
}
