<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/26/14
 * Time: 3:28 PM
 */

namespace Smorken\Ext\Database;

use Illuminate\Database\SqlServerConnection as Conn;
use Smorken\Ext\Database\Query\Grammars\SqlServerGrammar;

class SqlServerConnection extends Conn
{

    use QueryTrait;

    /**
     * @return \Illuminate\Database\Grammar|\Smorken\Ext\Database\Query\Grammars\SqlServerGrammar
     */
    protected function getDefaultQueryGrammar()
    {
        return $this->withTablePrefix(new SqlServerGrammar());
    }
}
