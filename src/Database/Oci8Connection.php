<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/12/17
 * Time: 7:05 AM
 */

namespace Smorken\Ext\Database;

use Smorken\Ext\Database\Query\Grammars\Oci8Grammar;

class Oci8Connection extends \Yajra\Oci8\Oci8Connection
{

    use QueryTrait;

    /**
     * @return \Illuminate\Database\Grammar|\Smorken\Ext\Database\Query\Grammars\Oci8Grammar
     */
    protected function getDefaultQueryGrammar()
    {
        return $this->withTablePrefix(new Oci8Grammar());
    }
}
