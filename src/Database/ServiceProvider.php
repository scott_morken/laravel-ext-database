<?php namespace Smorken\Ext\Database;

use Illuminate\Database\Connection;
use Smorken\Ext\Database\Connectors\OdbcConnector;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->addSqlSrv();
        $this->addMySql();
        $this->addSqlite();
        $this->addOdbc();
        $this->addOracle();
    }

    protected function addMySql()
    {
        Connection::resolverFor('mysql', function ($connection, $database, $prefix, $config) {
            return new MySqlConnection($connection, $database, $prefix, $config);
        });
    }

    protected function addOdbc()
    {
        Connection::resolverFor('odbc', function ($connection, $database, $prefix, $config) {
            $conn = new OdbcConnector();
            $pdo = $conn->connect($config);
            $db = new OdbcConnection($pdo, $database, $prefix, $config);
            return $db;
        });
    }

    protected function addOracle()
    {
        if (function_exists('oci_connect') && class_exists(\Yajra\Oci8\Oci8Connection::class)) {
            Connection::resolverFor('oracle', function ($connection, $database, $prefix, $config) {
                $connector = new \Yajra\Oci8\Connectors\OracleConnector();
                $connection = $connector->connect($config);
                $db = new \Yajra\Oci8\Oci8Connection($connection, $database, $prefix, $config);
                if (!empty($config['skip_session_vars'])) {
                    return $db;
                }
                // set oracle session variables
                $sessionVars = [
                    'NLS_TIME_FORMAT'         => 'HH24:MI:SS',
                    'NLS_DATE_FORMAT'         => 'YYYY-MM-DD HH24:MI:SS',
                    'NLS_TIMESTAMP_FORMAT'    => 'YYYY-MM-DD HH24:MI:SS',
                    'NLS_TIMESTAMP_TZ_FORMAT' => 'YYYY-MM-DD HH24:MI:SS TZH:TZM',
                    'NLS_NUMERIC_CHARACTERS'  => '.,',
                ];
                // Like Postgres, Oracle allows the concept of "schema"
                if (isset($config['schema'])) {
                    $sessionVars['CURRENT_SCHEMA'] = $config['schema'];
                }
                if (isset($config['session'])) {
                    $sessionVars = array_merge($sessionVars, $config['session']);
                }
                if (isset($config['edition'])) {
                    $sessionVars = array_merge($sessionVars, ['EDITION' => $config['edition']]);
                }

                $db->setSessionVars($sessionVars);
                return $db;
            });
        }
    }

    protected function addSqlSrv()
    {
        Connection::resolverFor('sqlsrv', function ($connection, $database, $prefix, $config) {
            return new SqlServerConnection($connection, $database, $prefix, $config);
        });
    }

    protected function addSqlite()
    {
        Connection::resolverFor('sqlite', function ($connection, $database, $prefix, $config) {
            return new SQLiteConnection($connection, $database, $prefix, $config);
        });
    }
}
