<?php

namespace Smorken\Ext\Database;

use Smorken\Ext\Database\Query\Builder;

trait QueryTrait
{

    /**
     * Get a new query builder instance.
     *
     * @return Builder
     */
    public function query()
    {
        return new Builder(
            $this, $this->getQueryGrammar(), $this->getPostProcessor()
        );
    }
}
