<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/8/14
 * Time: 7:02 AM
 */

namespace Smorken\Ext\Database;

use Illuminate\Database\SQLiteConnection as Conn;
use Smorken\Ext\Database\Query\Grammars\SQLiteGrammar;

class SQLiteConnection extends Conn
{

    use QueryTrait;

    /**
     * @return \Illuminate\Database\Grammar|\Smorken\Ext\Database\Query\Grammars\SQLiteGrammar
     */
    protected function getDefaultQueryGrammar()
    {
        return $this->withTablePrefix(new SQLiteGrammar());
    }

} 
