<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 7:16 AM
 */

namespace Smorken\Ext\Database\Query;

use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Query\Expression;

class Builder extends QueryBuilder
{

    public function allowZeroDates(): ?string
    {
        if (method_exists($this->grammar, 'compileAllowZeroDates')) {
            return $this->grammar->compileAllowZeroDates($this);
        }
        return null;
    }

    public function resetZeroDates(string $original): void
    {
        if (method_exists($this->grammar, 'compileResetZeroDates')) {
            $this->grammar->compileResetZeroDates($this, $original);
        }
    }

    public function complexJoin($toTable, $fromTable, $keys, $joinType = 'join')
    {
        $keys = $this->qualifyKeys($toTable, $fromTable, $keys);
        $on = [];
        foreach ($keys as $local => $foreign) {
            $on[] = [$local, '=', $foreign];
        }
        $this->$joinType(
            $toTable,
            function ($join) use ($on) {
                foreach ($on as $j) {
                    call_user_func_array([$join, 'on'], $j);
                }
            }
        );
        return $this;
    }

    public function concatenate($values, $separator = null, $as_expression = false)
    {
        if (method_exists($this->grammar, 'compileConcatenate')) {
            return $this->grammar->compileConcatenate($values, $separator, $as_expression);
        }
        $expression = 'CONCAT(%s)';
        $new_vals = [];
        $val_count = count($values);
        if ($val_count === 1) {
            $r = implode(', ', $values);
        } else {
            for ($i = 0; $i < $val_count; $i++) {
                $new_vals[] = $values[$i];
                if ($separator && $i < $val_count - 1) {
                    $new_vals[] = $this->grammar->wrap($separator);
                }
            }
            $r = sprintf($expression, implode(', ', $new_vals));
        }
        if (!$as_expression) {
            return $r;
        }
        return new Expression($r);
    }

    public function createOrUpdate($keys, array $values, array $update = [])
    {
        if (!$keys || !$values) {
            return false;
        }
        [$sql, $params] = $this->grammar->compileCreateOrUpdate($this, $keys, $values, $update);
        $bindings = $this->cleanBindingsNamed($params, true);
        return $this->connection->affectingStatement($sql, $bindings);
    }

    /**
     * Set the limit and offset for a given page.
     *
     * @param  int  $page
     * @param  int  $perPage
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function forPage($page, $perPage = 15)
    {
        if ($this->limit && $this->limit != $perPage && $page > 1) {
            return $this->whereRaw('1 <> 1');
        }
        if ($this->limit && $this->limit < $perPage) {
            $perPage = $this->limit;
        }
        return $this->skip(($page - 1) * $perPage)->take($perPage);
    }

    public function insertFrom(string $from, array $keys, array $insertColumns = []): int
    {
        $qualifiedKeys = $this->qualifyKeys($from, $this->from, $keys);
        $qualifiedColumns = $this->qualifyKeys($from, $this->from, $insertColumns);
        $qualifiedColumnsNoTable = $this->qualifyKeys($from, $this->from, $insertColumns, false);
        $sub = $this->newQuery()->from($from)->select(array_keys($qualifiedColumns))
                    ->complexJoin($this->from, $from, $this->flipArray($keys), 'leftJoin');
        foreach ($qualifiedKeys as $foreign => $local) {
            $sub->whereNull($local);
        }
        return $this->insertUsing(array_values($qualifiedColumnsNoTable), $sub);
    }

    public function temp(string $original, string $temp)
    {
        [$sql, $params] = $this->grammar->compileTemp($this, $original, $temp);
        $params = $this->cleanBindingsNamed($params);
        return $this->connection->affectingStatement($sql, $params);
    }

    public function updateFromComplex(string $from, array $keys, array $updateColumns = []): int
    {
        if (method_exists($this->grammar, 'compileUpdateFromComplex')) {
            return $this->grammar->compileUpdateFromComplex(
                $this,
                $from,
                $this->qualifyKeys($from, $this->from, $keys),
                $this->qualifyKeys($from, $this->from, $updateColumns)
            );
        }
        return $this->complexJoin($from, $this->from, $keys)
                    ->update($this->getTempUpdateArray($from, $this->from, $updateColumns));
    }

    protected function cleanBindingsNamed(array $bindings, $named = false)
    {
        if (!$named) {
            return parent::cleanBindings($bindings);
        } else {
            $b = [];
            foreach ($bindings as $key => $value) {
                if (!$value instanceof Expression) {
                    $pref = strpos($key, ':') !== 0 ? ':' : '';
                    $key = $pref.$key;
                    $b[$key] = $value;
                }
            }
            return $b;
        }
    }

    protected function flipArray(array $arr): array
    {
        $flipped = [];
        foreach ($arr as $k => $v) {
            if (is_integer($k)) {
                $flipped[$k] = $v;
            } else {
                $flipped[$v] = $k;
            }
        }
        return $flipped;
    }

    protected function getTempUpdateArray(string $from, string $to, array $update): array
    {
        $update = $this->qualifyKeys($from, $to, $update);
        $updates = [];
        foreach ($update as $local => $foreign) {
            $updates[$foreign] = new Expression($this->getGrammar()->wrapTable($local));
        }
        return $updates;
    }

    protected function qualifyKeys(string $toTable, string $fromTable, array $keys, bool $addTable = true): array
    {
        $qualified = [];
        foreach ($keys as $foreign => $local) {
            if (is_int($foreign)) {
                $foreign = $local;
            }
            $f = $addTable ? $toTable.'.'.$foreign : $foreign;
            $l = $addTable ? $fromTable.'.'.$local : $local;
            $qualified[$f] = $l;
        }
        return $qualified;
    }
}
