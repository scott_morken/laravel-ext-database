<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/12/17
 * Time: 7:08 AM
 */

namespace Smorken\Ext\Database\Query\Grammars;

use Illuminate\Database\Query\Builder;
use Yajra\Oci8\Query\Grammars\OracleGrammar;

class Oci8Grammar extends OracleGrammar
{

    use GrammarTrait;

    public function compileTemp(Builder $query, string $original, string $temp)
    {
        $sql = sprintf(
            'CREATE TABLE %s AS (SELECT * FROM %s WHERE 1=2)',
            $this->wrapTable($temp),
            $this->wrapTable($original)
        );
        return [$sql, []];
    }

    protected function whereDate(Builder $query, $where)
    {
        $value = $this->parameter($where['value']);

        return "{$this->wrap($where['column'])} {$where['operator']} TO_DATE($value, 'YYYY-MM-DD')";
    }
}
