<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/17/15
 * Time: 9:40 AM
 */

namespace Smorken\Ext\Database\Query\Grammars;

use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Expression;
use Illuminate\Database\Query\Grammars\SQLiteGrammar as Grammar;

class SQLiteGrammar extends Grammar
{

    use GrammarTrait;

    public function compileConcatenate($values, $separator = null, $as_expression = false)
    {
        $new_vals = [];
        $val_count = count($values);
        if ($val_count === 1) {
            $r = head($values);
        } else {
            for ($i = 0; $i < $val_count; $i++) {
                $new_vals[] = $values[$i];
                if ($separator && $i < $val_count - 1) {
                    $new_vals[] = $this->wrap($separator);
                }
            }
            $r = implode(' || ', $new_vals);
        }
        if (!$as_expression) {
            return $r;
        }
        return new Expression($r);
    }

    public function compileCreateOrUpdate(Builder $query, $keys, $values, $update = [])
    {
        if (!is_array($keys)) {
            $keys = [$keys];
        }
        $values = $this->ensureValuesArrayOfArrays($values);
        $table = $this->wrapTable($query->from);
        $insert_columns = $this->columnize($this->getInsertKeysFromValues($values));
        $insert_params = $this->parameterizeMassValues($values, true);
        $sql = sprintf(
            "INSERT OR REPLACE INTO %s
          (%s)
          VALUES %s;",
            $table,
            $insert_columns,
            $insert_params
        );
        $bindparams = $this->createBindParams(
            [
                [$values],
            ]
        );
        return [$sql, $bindparams];
    }

    public function compileTemp(Builder $query, string $original, string $temp): array
    {
        $sql = sprintf(
            'CREATE TABLE %s AS SELECT * FROM %s WHERE 0',
            $this->wrapTable($temp),
            $this->wrapTable($original)
        );
        return [$sql, []];
    }

    public function compileUpdateFromComplex(Builder $query, string $fromTable, array $keys, array $updateColumns)
    {
        $updates = $this->getSetsForUpdateFrom($query, $fromTable, $keys, $updateColumns);
        return $query->whereExists(
            function ($q) use ($query, $fromTable, $keys, $updateColumns) {
                $this->addWheresToUpdateFrom($q, $keys);
                $q->from($fromTable)->select(array_keys($updateColumns));
            }
        )->update($updates);
    }

    protected function addWheresToUpdateFrom(Builder $query, array $keys): void
    {
        foreach ($keys as $local => $foreign) {
            $query->where($local, '=', new Expression($this->wrapTable($foreign)));
        }
    }

    protected function getSetSql($values, $named = false, $prefix = '')
    {
        $sets = [];
        foreach ($values as $key => $value) {
            //$param = $named ? $this->parameterizeAsKey($key, $prefix) : $this->parameter($value);
            $sets[] = $this->wrap($key).'='.$this->parameterizeAsKey($key, $prefix);
        }
        return implode(', ', $sets);
    }

    protected function getSetsForUpdateFrom(Builder $query, string $fromTable, array $keys, array $updateColumns): array
    {
        $sets = [];
        foreach ($updateColumns as $foreign => $local) {
            $cloned = clone $query;
            $cloned->from($fromTable);
            $this->addWheresToUpdateFrom($cloned, $keys);
            $sets[$local] = new Expression('('.$cloned->select($foreign)->toSql().')');
        }
        return $sets;
    }
}
