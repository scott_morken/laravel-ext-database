<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/12/17
 * Time: 6:54 AM
 */

namespace Smorken\Ext\Database\Query\Grammars;

trait GrammarTrait
{

    public function createBindParams($options)
    {
        $bindparams = [];
        foreach ($options as $opt) {
            $records = $this->ensureValuesArrayOfArrays($opt[0] ?? []);
            $prefix = $opt[1] ?? '';
            $count = 0;
            foreach ($records as $i => $values) {
                foreach ($values as $k => $v) {
                    $bindparams[$this->parameterizeAsKey($k, $this->prefix([$count, $prefix]))] = $v;
                }
                $count++;
            }
        }
        return $bindparams;
    }

    public function parameterize(array $values, $named = false, $prefix = '')
    {
        if (!$named) {
            return parent::parameterize($values);
        }
        $params = [];
        foreach ($values as $key => $value) {
            $params[$key] = $this->parameterizeAsKey($key, $prefix);
        }
        return implode(', ', $params);
    }

    public function parameterizeAsKey($key, $prefix = '')
    {
        return ':'.$prefix.$key;
    }

    protected function ensureValuesArrayOfArrays($values)
    {
        if (!is_array(reset($values))) {
            $values = [$values];
        }
        return $values;
    }

    protected function getInsertKeysFromValues($values)
    {
        return array_keys(reset($values));
    }

    protected function getInsertParamsFromCols($cols)
    {
        $p = [];
        foreach ($cols as $col) {
            $p[$col] = $this->wrap('source.'.$col);
        }
        return $p;
    }

    protected function getSetSql($values, $named = false, $prefix = '')
    {
        $sets = [];
        foreach ($values as $key => $value) {
            //$param = $named ? $this->parameterizeAsKey($key, $prefix) : $this->parameter($value);
            $sets[] = $this->wrap('target.'.$key).'='.$this->wrap('source.'.$key);
        }
        return implode(', ', $sets);
    }

    protected function getSourceValues($keys, $values)
    {
        return $this->getValues($keys, $values, true);
    }

    protected function getUpdateValues($keys, $values)
    {
        return $this->getValues($keys, $values);
    }

    protected function getValues($keys, $values, $inkeys = false)
    {
        $v = [];
        foreach ($values as $k => $value) {
            if ($inkeys && in_array($k, $keys)) {
                $v[$k] = $value;
            } else {
                if (!$inkeys && !in_array($k, $keys)) {
                    $v[$k] = $value;
                }
            }
        }
        return $v;
    }

    protected function getValuesAsQuoted($query, $values)
    {
        $v = [];
        foreach ($values as $val) {
            $v[] = $query->getConnection()->getPdo()->quote($val);
        }
        return $v;
    }

    protected function limitUpdate($values, $update)
    {
        $first = reset($values);
        if (empty($update)) {
            return $first;
        }
        $use = [];
        foreach ($update as $key) {
            $use[$key] = $first[$key] ?? null;
        }
        return $use;
    }

    protected function parameterizeMassValues(array $values, $named = false, $prefix = '', $implode = ', ')
    {
        $params = [];
        $count = 0;
        foreach ($values as $record) {
            $params[] = '('.$this->parameterize($record, $named, $this->prefix([$count, $prefix])).')';
            $count++;
        }
        return implode($implode, $params);
    }

    protected function prefix($items)
    {
        return implode('_', $items);
    }

    protected function targetToSourceKeys($keys)
    {
        $cols = [];
        foreach ($keys as $key) {
            $cols[] = $this->wrap('target.'.$key).'='.$this->wrap('source.'.$key);
        }
        return implode(' AND ', $cols);
    }
}
