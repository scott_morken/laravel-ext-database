<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 7:08 AM
 */

namespace Smorken\Ext\Database\Query\Grammars;

use Illuminate\Database\Query\Grammars\SqlServerGrammar as Grammar;
use Smorken\Ext\Database\Query\Builder;

class SqlServerGrammar extends Grammar
{

    use GrammarTrait;

    public function compileCreateOrUpdate(Builder $query, $keys, $values, $update = [])
    {
        if (!is_array($keys)) {
            $keys = [$keys];
        }
        $values = $this->ensureValuesArrayOfArrays($values);
        $table = $this->wrapTable($query->from);
        $insert_columns = $this->columnize($this->getInsertKeysFromValues($values));
        $insert_params = $this->parameterizeMassValues($values, true);
        $update_values = $this->getUpdateValues($keys, $this->limitUpdate($values, $update));
        $sql = sprintf(
            "MERGE %s AS target
        USING (VALUES(%s))
          AS source(%s)
          ON %s
        WHEN MATCHED THEN
          UPDATE
          SET %s
        WHEN NOT MATCHED THEN
          INSERT (%s)
          VALUES(%s);",
            $table,
            $insert_params,
            /*implode(', ', $this->getValuesAsQuoted($query, $values)),*/
            implode(', ', $this->wrapArray($this->getInsertKeysFromValues($values))),
            $this->targetToSourceKeys($keys),
            $this->getSetSql($update_values, true),
            $insert_columns,
            implode(', ', $this->getInsertParamsFromCols($this->getInsertKeysFromValues($values)))
        );
        $bindparams = $this->createBindParams(
            [
                [$values],
            ]
        );
        return [$sql, $bindparams];
    }

    public function compileTemp(Builder $query, string $original, string $temp)
    {
        $sql = sprintf('SELECT TOP 0 * INTO %s FROM %s', $this->wrapTable($temp), $this->wrapTable($original));
        return [$sql, []];
    }
}
