<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/8/14
 * Time: 6:56 AM
 */

namespace Smorken\Ext\Database\Query\Grammars;

use Illuminate\Database\Query\Grammars\MySqlGrammar as Grammar;
use Smorken\Ext\Database\Query\Builder;

class MySqlGrammar extends Grammar
{

    use GrammarTrait;

    public function compileAllowZeroDates(Builder $query): string
    {
        $sqlmodes = $query->getConnection()->selectOne('SELECT @@sql_mode as mode');
        $query->getConnection()->statement('SET SQL_MODE=?', ['NO_AUTO_VALUE_ON_ZERO']);
        $query->getConnection()->statement('SET time_zone=?', ['+00:00']);
        return $sqlmodes ? $sqlmodes->mode : '';
    }

    public function compileResetZeroDates(Builder $query, string $original): void
    {
        if ($original) {
            $query->getConnection()->statement('SET SQL_MODE=?', [$original]);
        }
    }

    public function compileCreateOrUpdate(Builder $query, $keys, $values, $update = [])
    {
        if (!is_array($keys)) {
            $keys = [$keys];
        }
        $values = $this->ensureValuesArrayOfArrays($values);
        $table = $this->wrapTable($query->from);
        $insert_columns = $this->columnize($this->getInsertKeysFromValues($values));
        $insert_params = $this->parameterizeMassValues($values, true);
        $update_values = $this->getUpdateValues($keys, $this->limitUpdate($values, $update));
        $sql = sprintf(
            "INSERT INTO %s
          (%s)
          VALUES %s
        ON DUPLICATE KEY UPDATE
          %s;",
            $table,
            $insert_columns,
            $insert_params,
            $this->getSetSql($update_values, true, 'u_')
        );
        $bindparams = $this->createBindParams(
            [
                [$values],
            ]
        );
        return [$sql, $bindparams];
    }

    public function compileTemp(Builder $query, string $original, string $temp)
    {
        $sql = sprintf('CREATE TABLE %s (LIKE %s)', $this->wrapTable($temp), $this->wrapTable($original));
        return [$sql, []];
    }

    protected function getSetSql($values, $named = false, $prefix = '')
    {
        $sets = [];
        foreach ($values as $key => $value) {
            //$param = $named ? $this->parameterizeAsKey($key, $prefix) : $this->parameter($value);
            $sets[] = $this->wrap($key) . '=VALUES(' . $this->wrap($key).')';
        }
        return implode(', ', $sets);
    }
}
