<?php

namespace Smorken\Ext\Database\Tests\Unit\Eloquent;

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/5/16
 * Time: 11:14 AM
 */

use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Eloquent\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\Eloquent\Model;
use Smorken\Ext\Database\Eloquent\Relations\CompositeBelongsTo;
use Smorken\Ext\Database\SQLiteConnection;

class CompositeBelongsToTest extends TestCase
{

    public function tearDown(): void
    {
        m::close();
    }

    public function testAddEagerConstraints()
    {
        [$sut, $parentpdo, $relatedpdo] = $this->getSutAndMocks();
        $models = [
            new ParentBelongsToStub(['p_1' => 2, 'p_2' => 2]),
            new ParentBelongsToStub(['p_1' => 3, 'p_2' => 3]),
            new ParentBelongsToStub(['p_1' => 4, 'p_2' => 4]),
        ];
        $results = [
            ['id' => 1, 'r_1' => 2, 'r_2' => 2],
            ['id' => 2, 'r_1' => 2, 'r_2' => 2],
        ];
        $pdostmt = m::mock('\PDOStatement[prepare,execute,fetchAll,bindValue,setFetchMode]');
        $relatedpdo->shouldReceive('prepare')->with(
            'select * from "related" where (("related"."r_1" = ? and "related"."r_2" = ?) or ("related"."r_1" = ? and "related"."r_2" = ?) or ("related"."r_1" = ? and "related"."r_2" = ?) or ("related"."r_1" = ? and "related"."r_2" = ?))'
        )->andReturn($pdostmt);
        $pdostmt->shouldReceive('setFetchMode')
            ->once()
            ->with(5);
        foreach ([0 => 1, 1 => 1, 2 => 2, 3 => 2, 4 => 3, 5 => 3, 6 => 4, 7 => 4] as $k => $v) {
            $pdostmt->shouldReceive('bindValue')->with($k + 1, $v, m::any());
        }
        $pdostmt->shouldReceive('execute')->once();
        $pdostmt->shouldReceive('fetchAll')->andReturn($results);
        $sut->addEagerConstraints($models);
        $r = $sut->getEager();
        $this->assertInstanceOf(RelatedBelongsToStub::class, $r->first());
        $this->assertCount(2, $r);
    }

    public function testAssociateSetsForeignKeysOnModel()
    {
        [$sut, $parentpdo, $relatedpdo] = $this->getSutAndMocks();
        $r = new RelatedBelongsToStub(['id' => 2, 'r_1' => 99, 'r_2' => 98]);
        $parent = $sut->associate($r);
        $this->assertEquals(99, $parent->p_1);
        $this->assertEquals(98, $parent->p_2);
        $this->assertEquals($r, $parent->relation);
    }

    public function testAssociateWithIdSetsForeignKeysOnModel()
    {
        [$sut, $parentpdo, $relatedpdo] = $this->getSutAndMocks();
        $r = [['id' => 2, 'r_1' => 99, 'r_2' => 98]];
        $pdostmt = m::mock('\PDOStatement[prepare,execute,fetchAll,bindValue,setFetchMode]');
        $relatedpdo->shouldReceive('prepare')->with(
            'select * from "related" where "related"."id" = ? limit 1'
        )->andReturn($pdostmt);
        $pdostmt->shouldReceive('setFetchMode')
        ->once()
        ->with(5);
        $pdostmt->shouldReceive('bindValue')->once()->with(1, 2, m::any());
        $pdostmt->shouldReceive('execute')->once();
        $pdostmt->shouldReceive('fetchAll')->andReturn($r);
        $parent = $sut->associate(2);
        $this->assertEquals(99, $parent->p_1);
        $this->assertEquals(98, $parent->p_2);
        $this->assertInstanceOf(RelatedBelongsToStub::class, $parent->relation);
    }

    public function testDissociateUnsetsForeignKeysOnModel()
    {
        [$sut, $parentpdo, $relatedpdo] = $this->getSutAndMocks();
        $parent = $sut->dissociate();
        $this->assertNull($parent->p_1);
        $this->assertNull($parent->p_2);
        $this->assertNull($parent->relation);
    }

    public function testGetRelationCountQuery()
    {
        [$sut, $parentpdo, $relatedpdo, $parent_model, $related_model] = $this->getSutAndMocks();
        $q = $sut->getRelationExistenceCountQuery($related_model->newQuery(), $parent_model->newQuery());
        $sql = $q->toSql();
        $expected = 'select count(*) from "related" where ("parent"."p_1" = "related"."r_1" and "parent"."p_2" = "related"."r_2")';
        $this->assertEquals($expected, $sql);
    }

    public function testGetRelationCountQueryForSelf()
    {
        [$sut, $parentpdo, $relatedpdo, $parent_model, $related_model] = $this->getSutAndMocks();
        $q = $sut->getRelationExistenceCountQuery($parent_model->newQuery(), $parent_model->newQuery());
        $sql = $q->toSql();
        $expected = 'select count(*) from "parent" as "laravel_reserved_0" where ("laravel_reserved_0"."p_1" = "laravel_reserved_0"."r_1" and "laravel_reserved_0"."p_2" = "laravel_reserved_0"."r_2")';
        $this->assertEquals($expected, $sql);
    }

    public function testModelsAreMatchedToParents()
    {
        [$sut, $parentpdo, $relatedpdo] = $this->getSutAndMocks();
        $models = [
            new ParentBelongsToStub(['p_1' => 2, 'p_2' => 2]),
            new ParentBelongsToStub(['p_1' => 3, 'p_2' => 3]),
            new ParentBelongsToStub(['p_1' => 3, 'p_2' => 3]),
            new ParentBelongsToStub(['p_1' => 4, 'p_2' => 4]),
        ];
        $results = [
            new RelatedBelongsToStub(['id' => 1, 'r_1' => 2, 'r_2' => 2]),
            new RelatedBelongsToStub(['id' => 2, 'r_1' => 3, 'r_2' => 3]),
        ];
        $results = new Collection($results);
        $matches = $sut->match($models, $results, 'foo');
        $this->assertCount(4, $matches);
        foreach ($matches as $m) {
            if (($m->p_1 === 2 && $m->p_2 === 2) || ($m->p_1 === 3 && $m->p_2 === 3)) {
                $this->assertInstanceOf(RelatedBelongsToStub::class, $m->foo);
            } else {
                $this->assertNull($m->foo);
            }
        }
    }

    public function testRelationIsProperlyInitialized()
    {
        [$sut, $parentpdo, $relatedpdo] = $this->getSutAndMocks();
        $models = [
            new ParentBelongsToStub(['p_1' => 2, 'p_2' => 2]),
            new ParentBelongsToStub(['p_1' => 3, 'p_2' => 3]),
            new ParentBelongsToStub(['p_1' => 4, 'p_2' => 4]),
        ];
        $models = $sut->initRelation($models, 'foo');
        foreach ($models as $model) {
            $this->assertNull($model->foo);
        }
    }

    protected function getModel($model_class, $attrs = [])
    {
        $pdo = m::mock(\PDO::class);
        $connections = [
            'test' => new SQLiteConnection($pdo),
        ];
        $resolver = new ConnectionResolver($connections);
        $resolver->setDefaultConnection('test');
        $model = new $model_class($attrs);
        $model_class::setConnectionResolver($resolver);
        return [$model, $pdo];
    }

    protected function getSutAndMocks($parent = null)
    {
        [$pm, $parent_pdo] = $this->getModel(ParentBelongsToStub::class, ['p_1' => 1, 'p_2' => 1]);
        $parent = $parent ?: $pm;
        [$rm, $related_pdo] = $this->getModel(RelatedBelongsToStub::class);
        $builder = $rm->newQuery();
        $sut = new CompositeBelongsTo(
            $builder,
            $parent,
            ['p_1', 'p_2'],
            ['r_1', 'r_2'],
            'relation'
        );
        return [$sut, $parent_pdo, $related_pdo, $pm, $rm];
    }
}

class ParentBelongsToStub extends Model
{

    protected $connection = 'test';

    protected $fillable = ['p_1', 'p_2'];

    protected $table = 'parent';
}

class RelatedBelongsToStub extends Model
{

    protected $connection = 'test';

    protected $fillable = ['r_1', 'r_2'];

    protected $table = 'related';
}
