<?php

namespace Smorken\Ext\Database\Tests\Unit\Eloquent;

use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\Eloquent\ExpressionValue;

class EVModelStub
{

    public $test = 'foo';

    public $testlong = 'abcdefghijkl';
}

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/26/16
 * Time: 11:10 AM
 */
class ExpressionValueTest extends TestCase
{

    public function testGetColumnReturnsColumnWhenNoExpression()
    {
        $sut = new ExpressionValue('foo');
        $this->assertEquals('foo', $sut->getColumn());
    }

    public function testGetColumnReturnsExpressionWhenSet()
    {
        $sut = new ExpressionValue('foo', 'exp');
        $this->assertEquals('exp', $sut->getColumn());
    }

    public function testGetValueWithNestedClosure()
    {
        $outerfunc = function ($start, $length) {
            return function ($ev, $model) use ($start, $length) {
                return substr($model->testlong, $start, $length);
            };
        };
        $sut = new ExpressionValue('testlong', null, $outerfunc(0, 2));
        $this->assertEquals('ab', $sut->getValue(new EVModelStub()));
    }

    public function testGetValueWithNoEvaluation()
    {
        $sut = new ExpressionValue('test');
        $this->assertEquals('foo', $sut->getValue(new EVModelStub()));
    }

    public function testGetValueWithSimpleClosure()
    {
        $func = function ($ev, $model) {
            return substr($model->testlong, 0, 2);
        };
        $sut = new ExpressionValue('testlong', null, $func);
        $this->assertEquals('ab', $sut->getValue(new EVModelStub()));
    }
}
