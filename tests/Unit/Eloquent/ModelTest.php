<?php

namespace Smorken\Ext\Database\Tests\Unit\Eloquent;

/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 7:27 AM
 */

use Mockery as m;
use OutOfBoundsException;
use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\Eloquent\Model;
use Smorken\Ext\Database\Tests\Unit\PDOStub;

class ModelStub extends Model
{

}

class ModelTest extends TestCase
{

    /**
     * @var Model
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        [$conn, $grammar, $process] = $this->getConnectionMocks();
        $this->sut = m::mock('Smorken\Ext\Database\Eloquent\Model[resolveConnection]');
        $this->sut->shouldReceive('resolveConnection')->andReturn($conn);
    }

    public function tearDown(): void
    {
        m::close();
    }

    public function testBelongsToManyArrayReturnsBelongsToManyArray()
    {
        [$conn, $grammar, $process] = $this->getConnectionMocks();
        $m = m::mock('Smorken\Ext\Database\Eloquent\Model[resolveConnection]');
        $m->shouldReceive('resolveConnection')->andReturn($conn);
        $r = $this->sut->belongsToManyArray($m, ['foo', 'bar'], ['biz', 'baz']);
        $this->assertInstanceOf('Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray', $r);
    }

    public function testCompositeBelongsIsExceptionWithReversedRelation()
    {
        [$conn, $grammar, $process] = $this->getConnectionMocks();
        $m = m::mock('Smorken\Ext\Database\Eloquent\Model[resolveConnection]');
        $m->shouldReceive('resolveConnection')->andReturn($conn);
        $this->sut->biz = 1;
        $this->sut->baz = 2;
        $this->expectException(
            OutOfBoundsException::class
        );
        $this->expectExceptionMessage('No relation appears to have been loaded, check your keys.');
        $r = $this->sut->compositeBelongsTo($m, ['foo', 'bar'], ['biz', 'baz']);
    }

    public function testCompositeBelongsToReturnsCompositeBelongsTo()
    {
        [$conn, $grammar, $process] = $this->getConnectionMocks();
        $m = m::mock('Smorken\Ext\Database\Eloquent\Model[resolveConnection]');
        $m->shouldReceive('resolveConnection')->andReturn($conn);
        $this->sut->foo = 1;
        $this->sut->bar = 2;
        $r = $this->sut->compositeBelongsTo($m, ['foo', 'bar'], ['biz', 'baz']);
        $this->assertInstanceOf('Smorken\Ext\Database\Eloquent\Relations\CompositeBelongsTo', $r);
    }

    public function testCompositeHasManyIsExceptionWithReversedRelation()
    {
        [$conn, $grammar, $process] = $this->getConnectionMocks();
        $m = m::mock('Smorken\Ext\Database\Eloquent\Model[resolveConnection]');
        $m->shouldReceive('resolveConnection')->andReturn($conn);
        $this->sut->foo = 1;
        $this->sut->bar = 2;
        $this->expectException(
            OutOfBoundsException::class
        );
        $this->expectExceptionMessage('No relation appears to have been loaded, check your keys.');
        $r = $this->sut->compositeHasMany($m, ['foo', 'bar'], ['biz', 'baz']);
    }

    public function testCompositeHasManyReturnsCompositeHasMany()
    {
        [$conn, $grammar, $process] = $this->getConnectionMocks();
        $m = m::mock('Smorken\Ext\Database\Eloquent\Model[resolveConnection]');
        $m->shouldReceive('resolveConnection')->andReturn($conn);
        $this->sut->biz = 1;
        $this->sut->baz = 2;
        $r = $this->sut->compositeHasMany($m, ['foo', 'bar'], ['biz', 'baz']);
        $this->assertInstanceOf('Smorken\Ext\Database\Eloquent\Relations\CompositeHasMany', $r);
    }

    public function testCompositeHasOneIsExceptionWithReversedRelation()
    {
        [$conn, $grammar, $process] = $this->getConnectionMocks();
        $m = m::mock('Smorken\Ext\Database\Eloquent\Model[resolveConnection]');
        $m->shouldReceive('resolveConnection')->andReturn($conn);
        $this->sut->foo = 1;
        $this->sut->bar = 2;
        $this->expectException(
            OutOfBoundsException::class
        );
        $this->expectExceptionMessage('No relation appears to have been loaded, check your keys.');
        $r = $this->sut->compositeHasOne($m, ['foo', 'bar'], ['biz', 'baz']);
    }

    public function testCompositeHasOneReturnsCompositeHasOne()
    {
        [$conn, $grammar, $process] = $this->getConnectionMocks();
        $m = m::mock('Smorken\Ext\Database\Eloquent\Model[resolveConnection]');
        $m->shouldReceive('resolveConnection')->andReturn($conn);
        $this->sut->biz = 1;
        $this->sut->baz = 2;
        $r = $this->sut->compositeHasOne($m, ['foo', 'bar'], ['biz', 'baz']);
        $this->assertInstanceOf('Smorken\Ext\Database\Eloquent\Relations\CompositeHasOne', $r);
    }

    public function testEloquentQueryBuilder()
    {
        $qb = m::mock('Smorken\Ext\Database\Query\Builder');
        $eb = $this->sut->newEloquentBuilder($qb);
        $this->assertInstanceOf('Smorken\Ext\Database\Eloquent\Builder', $eb);
    }

    public function testQueryBuilder()
    {
        $qb = $this->sut->newQuery();
        $q = $qb->getQuery();
        $this->assertInstanceOf('Smorken\Ext\Database\Query\Builder', $q);
    }

    protected function getConnectionMocks()
    {
        $grammar = m::mock('Smorken\Ext\Database\Query\Grammars\MySqlGrammar');
        $conn = m::mock('Illuminate\Database\ConnectionInterface');
        $conn->shouldReceive('getQueryGrammar')->andReturn($grammar);
        $conn->shouldReceive('getPostProcessor')->andReturn(
            $processor = m::mock('Illuminate\Database\Query\Processors\Processor')
        );
        $grammar->shouldReceive('getBitwiseOperators')->andReturn([]);
        return [$conn, $grammar, $processor];
    }

    protected function getMockConnection($methods = [], $pdo = null)
    {
        $pdo = $pdo ?: new PDOStub();
        $defaults = ['getDefaultPostProcessor', 'getDefaultSchemaGrammar'];
        $methods = implode(',', array_merge($defaults, $methods));
        return m::mock(sprintf('Smorken\Ext\Database\MySqlConnection[%s]', $methods), [$pdo]);
    }
}
