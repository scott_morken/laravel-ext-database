<?php

namespace Smorken\Ext\Database\Tests\Unit\Eloquent;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\Eloquent\Relations\HasManyThroughArray;
use stdClass;

class EloquentHasManyThroughModelStub extends Model
{

    public $far_id = 'second.value';

    public $mid_id_1 = 'first.value';

    public $mid_id_2 = 'second.value';

    public $near_id = 'first.value';
}

class EloquentHasManyThroughSoftDeletingModelStub extends Model
{

    use SoftDeletes;

    public $table = 'users';
}

class HasManyThroughArrayTest extends TestCase
{

    public function tearDown(): void
    {
        m::close();
    }

    public function testAllColumnsAreSelectedByDefault()
    {
        $select = ['far.*', 'mid.mid_id_1', 'mid.mid_id_2'];
        $baseBuilder = m::mock('Illuminate\Database\Query\Builder');
        $relation = $this->getRelation();
        $relation->getRelated()->shouldReceive('newCollection')->once()->andReturn('results');
        $builder = $relation->getQuery();
        $builder->shouldReceive('applyScopes')->andReturnSelf();
        $builder->shouldReceive('getQuery')->andReturn($baseBuilder);
        $builder->shouldReceive('addSelect')->once()->with($select)->andReturn($builder);
        $builder->shouldReceive('getModels')->once()->andReturn([]);
        $results = $relation->get();
        $this->assertEquals('results', $results);
    }

    public function testEagerConstraintsAreProperlyAdded()
    {
        $select = ['far.*', 'mid.mid_id_1', 'mid.mid_id_2'];
        $baseBuilder = m::mock('Illuminate\Database\Query\Builder');
        $relation = $this->getRelation();
        $relation->getRelated()->shouldReceive('newCollection')->once()->andReturn('results');
        $relation->getQuery()->shouldReceive('whereIn')->once()->with('mid.mid_id_1', [1, 2]);
        $model1 = new EloquentHasManyThroughModelStub;
        $model1->setAttribute('near_id', 1);
        $model2 = new EloquentHasManyThroughModelStub;
        $model2->setAttribute('near_id', 2);
        $relation->addEagerConstraints([$model1, $model2]);
        $builder = $relation->getQuery();
        $builder->shouldReceive('applyScopes')->andReturnSelf();
        $builder->shouldReceive('getQuery')->andReturn($baseBuilder);
        $builder->shouldReceive('addSelect')->once()->with($select)->andReturn($builder);
        $builder->shouldReceive('getModels')->once()->andReturn([]);
        $relation->getQuery()->shouldReceive('get')->andReturn([]);
        $this->assertEquals('results', $relation->get());
    }

    public function testFindManyMethod()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\HasManyThroughArray[get]',
            $this->getRelationArguments()
        );
        $coll = new Collection(
            [
                'first',
                'second',
            ]
        );
        $relation->shouldReceive('get')->once()->andReturn($coll);
        $relation->shouldReceive('whereIn')->with('far.far_id', ['foo', 'bar'])->once()->andReturn($relation);
        $related = $relation->getRelated();
        $this->assertEquals($coll, $relation->findMany(['foo', 'bar']));
    }

    public function testFindMethod()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\HasManyThroughArray[first]',
            $this->getRelationArguments()
        );
        $relation->shouldReceive('where')->with('far.far_id', '=', 'foo')->once()->andReturn($relation);
        $m = new  stdClass();
        $relation->shouldReceive('first')->once()->andReturn($m);
        $related = $relation->getRelated();
        $this->assertEquals($m, $relation->find('foo'));
    }

    public function testFirstMethod()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\HasManyThroughArray[get]',
            $this->getRelationArguments()
        );
        $relation->shouldReceive('get')->once()->andReturn(
            new Collection(
                [
                    'first',
                    'second',
                ]
            )
        );
        $relation->shouldReceive('take')->with(1)->once()->andReturn($relation);
        $this->assertEquals('first', $relation->first());
    }

    public function testIgnoreSoftDeletingParent()
    {
        [$builder, $country, , $firstKey, $secondKey] = $this->getRelationArguments(true);
        $user = m::mock(EloquentHasManyThroughSoftDeletingModelStub::class.'[newQuery]');
        $user->shouldReceive('newQuery')->andReturn($builder);
        $builder->shouldReceive('join')->once()->with(
            m::type('Illuminate\Database\Query\Expression'),
            'users.mid_id_2',
            '=',
            'far.far_id'
        );
        $builder->shouldReceive('where')->with('users.mid_id_1', '=', 1);
        $builder->shouldReceive('withGlobalScope')
                ->once()
                ->with('SoftDeletableHasManyThrough', m::type('callable'))
                ->andReturnUsing(function (string $key, callable $function) use ($builder) {
                    $function($builder);
                    return true;
                });
        $builder->shouldReceive('whereNull')->with('users.deleted_at')->once()->andReturn($builder);//soft delete
        $relation = new HasManyThroughArray($builder, $country, $user, $firstKey, $secondKey);
        $this->assertTrue(true); //come up with an actual test
    }

    public function testModelsAreProperlyMatchedToParents()
    {
        $relation = $this->getRelation();
        $result1 = new EloquentHasManyThroughModelStub();
        $result1->mid_id_2 = 1;
        $result1->mid_id_1 = 1;
        $result1->far_id = 1;
        $result2 = new EloquentHasManyThroughModelStub();
        $result2->mid_id_2 = 2;
        $result2->mid_id_1 = 2;
        $result2->far_id = 2;
        $result3 = new EloquentHasManyThroughModelStub();
        $result3->mid_id_2 = 2;
        $result3->mid_id_1 = 2;
        $result3->far_id = 2;
        $model1 = new EloquentHasManyThroughModelStub();
        $model1->near_id = 1;
        $model2 = new EloquentHasManyThroughModelStub();
        $model2->near_id = 2;
        $model3 = new EloquentHasManyThroughModelStub();
        $model3->near_id = 3;
        $relation->getRelated()->shouldReceive('newCollection')->andReturnUsing(
            function ($array) {
                return new Collection($array);
            }
        );
        $models = $relation->match([$model1, $model2, $model3], new Collection([$result1, $result2, $result3]), 'foo');
        $this->assertEquals(1, $models[0]->foo[0]->far_id);
        $this->assertEquals(1, count($models[0]->foo));
        $this->assertEquals(2, $models[1]->foo[0]->far_id);
        $this->assertEquals(2, $models[1]->foo[1]->far_id);
        $this->assertEquals(2, count($models[1]->foo));
        $this->assertEquals(0, count($models[2]->foo));
    }

    public function testOnlyProperColumnsAreSelectedIfProvided()
    {
        $select = ['mid.mid_id_1', 'mid.mid_id_2'];
        $baseBuilder = m::mock('Illuminate\Database\Query\Builder');
        $baseBuilder->columns = ['foo', 'bar'];
        $relation = $this->getRelation();
        $relation->getRelated()->shouldReceive('newCollection')->once()->andReturn('results');
        $builder = $relation->getQuery();
        $builder->shouldReceive('applyScopes')->andReturnSelf();
        $builder->shouldReceive('getQuery')->andReturn($baseBuilder);
        $builder->shouldReceive('addSelect')->once()->with($select)->andReturn($builder);
        $builder->shouldReceive('getModels')->once()->andReturn([]);
        $this->assertEquals('results', $relation->get());
    }

    public function testRelationIsProperlyInitialized()
    {
        $relation = $this->getRelation();
        $model = m::mock('Illuminate\Database\Eloquent\Model');
        $relation->getRelated()->shouldReceive('newCollection')->andReturnUsing(
            function ($array = []) {
                return new Collection($array);
            }
        );
        $model->shouldReceive('setRelation')->once()->with('foo', m::type('Illuminate\Database\Eloquent\Collection'));
        $models = $relation->initRelation([$model], 'foo');
        $this->assertEquals([$model], $models);
    }

    protected function getRelation()
    {
        [$builder, $country, $user, $firstKey, $secondKey] = $this->getRelationArguments();
        return new HasManyThroughArray($builder, $country, $user, $firstKey, $secondKey);
    }

    protected function getRelationArguments($skip_builder = false)
    {
        $builder = m::mock('Illuminate\Database\Eloquent\Builder');
        if (!$skip_builder) {
            $builder->shouldReceive('join')->once()->with(
                m::type('Illuminate\Database\Query\Expression'),
                'mid.mid_id_2',
                '=',
                'far.far_id'
            );
            $builder->shouldReceive('where')->with('mid.mid_id_1', '=', 1);
        }
        $far = m::mock('Illuminate\Database\Eloquent\Model');
        //z$far->shouldReceive('getKeyName')->andReturn('id');
        $far->shouldReceive('offsetGet')->andReturn(1);
        //$far->shouldReceive('getForeignKey')->andReturn('country_id');
        $far->shouldReceive('getTable')->andReturn('far');
        $mid = m::mock('Illuminate\Database\Eloquent\Model');
        $mid->shouldReceive('getTable')->andReturn('mid');
        $mid->shouldReceive('newQuery')->andReturn($builder);
        $builder->shouldReceive('select')->with('mid_id_1', 'mid_id_2')->once()->andReturn($builder);
        $builder->shouldReceive('distinct')->once()->andReturn($builder);
        $builder->shouldReceive('toSql')->once()->andReturn('mid sql');
        //$mid->shouldReceive('getQualifiedKeyName')->andReturn('users.id');
        $near = m::mock('Illuminate\Database\Eloquent\Model');
        $near->shouldReceive('getTable')->andReturn('near');
        $builder->shouldReceive('getModel')->andReturn($far);
        $mid->shouldReceive('getKey')->andReturn(1);
        $mid->shouldReceive('getCreatedAtColumn')->andReturn('created_at');
        $mid->shouldReceive('getUpdatedAtColumn')->andReturn('updated_at');
        return [$builder, $far, $mid, ['near_id', 'mid_id_1'], ['mid_id_2', 'far_id']];
    }
}
