<?php

namespace Smorken\Ext\Database\Tests\Unit\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray;
use Smorken\Ext\Database\Tests\Integration\Stubs\BaseModel;
use stdClass;

class BelongsToManyArrayTest extends TestCase
{

    public function getRelation()
    {
        [$builder, $parent] = $this->getRelationArguments();
        return new BelongsToManyArray(
            $builder,
            $parent,
            'user_role',
            ['id', 'user_id'],
            ['role_id', 'id'],
            'relation_name'
        );
    }

    public function getRelationArguments()
    {
        $parent = m::mock(Model::class);
        $parent->shouldReceive('getKey')->andReturn(1);
        $parent->shouldReceive('getCreatedAtColumn')->andReturn('created_at');
        $parent->shouldReceive('getUpdatedAtColumn')->andReturn('updated_at');
        $query = m::mock(\Illuminate\Database\Query\Builder::class);
        $builder = m::mock(Builder::class, [$query])->makePartial();
        $related = m::mock(Model::class);
        $builder->shouldReceive('getModel')->andReturn($related);
        $builder->shouldReceive('applyScopes')->andReturn($builder);
        $related->shouldReceive('getTable')->andReturn('roles');
        $related->shouldReceive('getKeyName')->andReturn('id');
        $related->shouldReceive('newPivot')->andReturnUsing(
            function ($parent, $attributes, $table, $exists, $using) {
                return Pivot::fromAttributes($parent, $attributes, $table,
                    $exists);
            }
        );
        $builder->shouldReceive('join')->once()->with('user_role', 'roles.id', '=', 'user_role.role_id');
        $parent->shouldReceive('offsetGet')->with('id')->andReturn(1);
        $builder->shouldReceive('where')->once()->with('user_role.user_id', '=', 1);
        return [$builder, $parent, 'user_role', ['id', 'user_id'], ['role_id', 'id'], 'relation_name'];
    }

    public function syncMethodListProvider()
    {
        return [
            [[2, 3, 'x']],
            [['2', '3', 'x']],
        ];
    }

    public function tearDown(): void
    {
        m::close();
    }

    public function testAttachInsertsPivotTableRecord()
    {
        $relation = $this->getMockBuilder('Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray')
                         ->setMethods(['touchIfTouching'])
                         ->setConstructorArgs($this->getRelationArguments())
                         ->getMock();
        $query = m::mock('stdClass');
        $query->shouldReceive('from')->once()->with('user_role')->andReturn($query);
        $query->shouldReceive('insert')->once()->with([['user_id' => 1, 'role_id' => 2, 'foo' => 'bar']])->andReturn(
            true
        );
        $relation->getQuery()->shouldReceive('getQuery')->andReturn($mockQueryBuilder = m::mock('stdClass'));
        $mockQueryBuilder->shouldReceive('newQuery')->once()->andReturn($query);
        $relation->expects($this->once())->method('touchIfTouching');
        $relation->attach(2, ['foo' => 'bar']);
        $this->assertTrue(true);//todo come up with a "real" test
    }

    public function testAttachInsertsPivotTableRecordWithACreatedAtTimestamp()
    {
        $relation = $this->getMockBuilder('Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray')
                         ->setMethods(['touchIfTouching'])
                         ->setConstructorArgs($this->getRelationArguments())
                         ->getMock();
        $relation->withPivot('created_at');
        $query = m::mock('stdClass');
        $query->shouldReceive('from')->once()->with('user_role')->andReturn($query);
        $query->shouldReceive('insert')->once()->with(
            [['user_id' => 1, 'role_id' => 2, 'foo' => 'bar', 'created_at' => 'time']]
        )->andReturn(true);
        $relation->getQuery()->shouldReceive('getQuery')->andReturn($mockQueryBuilder = m::mock('stdClass'));
        $mockQueryBuilder->shouldReceive('newQuery')->once()->andReturn($query);
        $relation->getParent()->shouldReceive('freshTimestamp')->once()->andReturn('time');
        $relation->expects($this->once())->method('touchIfTouching');
        $relation->attach(2, ['foo' => 'bar']);
        $this->assertTrue(true);//todo come up with a "real" test
    }

    public function testAttachInsertsPivotTableRecordWithAnUpdatedAtTimestamp()
    {
        $relation = $this->getMockBuilder('Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray')
                         ->setMethods(['touchIfTouching'])
                         ->setConstructorArgs($this->getRelationArguments())
                         ->getMock();
        $relation->withPivot('updated_at');
        $query = m::mock('stdClass');
        $query->shouldReceive('from')->once()->with('user_role')->andReturn($query);
        $query->shouldReceive('insert')->once()->with(
            [['user_id' => 1, 'role_id' => 2, 'foo' => 'bar', 'updated_at' => 'time']]
        )->andReturn(true);
        $relation->getQuery()->shouldReceive('getQuery')->andReturn($mockQueryBuilder = m::mock('stdClass'));
        $mockQueryBuilder->shouldReceive('newQuery')->once()->andReturn($query);
        $relation->getParent()->shouldReceive('freshTimestamp')->once()->andReturn('time');
        $relation->expects($this->once())->method('touchIfTouching');
        $relation->attach(2, ['foo' => 'bar']);
        $this->assertTrue(true);//todo come up with a "real" test
    }

    public function testAttachInsertsPivotTableRecordWithCustomTimestampColumns()
    {
        $relation = $this->getMockBuilder('Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray')
                         ->setMethods(['touchIfTouching'])
                         ->setConstructorArgs($this->getRelationArguments())
                         ->getMock();
        $relation->withTimestamps('custom_created_at', 'custom_updated_at');
        $query = m::mock('stdClass');
        $query->shouldReceive('from')->once()->with('user_role')->andReturn($query);
        $query->shouldReceive('insert')->once()->with(
            [
                [
                    'user_id' => 1,
                    'role_id' => 2,
                    'foo' => 'bar',
                    'custom_created_at' => 'time',
                    'custom_updated_at' => 'time',
                ],
            ]
        )->andReturn(true);
        $relation->getQuery()->shouldReceive('getQuery')->andReturn($mockQueryBuilder = m::mock('stdClass'));
        $mockQueryBuilder->shouldReceive('newQuery')->once()->andReturn($query);
        $relation->getParent()->shouldReceive('freshTimestamp')->once()->andReturn('time');
        $relation->expects($this->once())->method('touchIfTouching');
        $relation->attach(2, ['foo' => 'bar']);
        $this->assertTrue(true);//todo come up with a "real" test
    }

    public function testAttachInsertsPivotTableRecordWithTimestampsWhenNecessary()
    {
        $relation = $this->getMockBuilder('Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray')
                         ->setMethods(['touchIfTouching'])
                         ->setConstructorArgs($this->getRelationArguments())
                         ->getMock();
        $relation->withTimestamps();
        $query = m::mock('stdClass');
        $query->shouldReceive('from')->once()->with('user_role')->andReturn($query);
        $query->shouldReceive('insert')->once()->with(
            [['user_id' => 1, 'role_id' => 2, 'foo' => 'bar', 'created_at' => 'time', 'updated_at' => 'time']]
        )->andReturn(true);
        $relation->getQuery()->shouldReceive('getQuery')->andReturn($mockQueryBuilder = m::mock('stdClass'));
        $mockQueryBuilder->shouldReceive('newQuery')->once()->andReturn($query);
        $relation->getParent()->shouldReceive('freshTimestamp')->once()->andReturn('time');
        $relation->expects($this->once())->method('touchIfTouching');
        $relation->attach(2, ['foo' => 'bar']);
        $this->assertTrue(true);//todo come up with a "real" test
    }

    public function testAttachMultipleInsertsPivotTableRecord()
    {
        $relation = $this->getMockBuilder('Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray')
                         ->setMethods(['touchIfTouching'])
                         ->setConstructorArgs($this->getRelationArguments())
                         ->getMock();
        $query = m::mock('stdClass');
        $query->shouldReceive('from')->once()->with('user_role')->andReturn($query);
        $query->shouldReceive('insert')->once()->with(
            [
                ['user_id' => 1, 'role_id' => 2, 'foo' => 'bar'],
                ['user_id' => 1, 'role_id' => 3, 'baz' => 'boom', 'foo' => 'bar'],
            ]
        )->andReturn(true);
        $relation->getQuery()->shouldReceive('getQuery')->andReturn($mockQueryBuilder = m::mock('stdClass'));
        $mockQueryBuilder->shouldReceive('newQuery')->once()->andReturn($query);
        $relation->expects($this->once())->method('touchIfTouching');
        $relation->attach([2, 3 => ['baz' => 'boom']], ['foo' => 'bar']);
        $this->assertTrue(true);//todo come up with a "real" test
    }

    public function testCreateMethodCreatesNewModelAndInsertsAttachmentRecord()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[attach]',
            $this->getRelationArguments()
        );
        $relation->getRelated()->shouldReceive('newInstance')->once()->andReturn($model = m::mock('stdClass'))->with(
            ['attributes']
        );
        $model->shouldReceive('save')->once();
        $model->id = 'foo';
        //$model->shouldReceive('getKey')->andReturn('foo');
        $relation->shouldReceive('attach')->once()->with('foo', ['joining'], true);
        $this->assertEquals($model, $relation->create(['attributes'], ['joining']));
    }

    public function testDetachMethodClearsAllPivotRecordsWhenNoIDsAreGiven()
    {
        $relation = $this->getMockBuilder('Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray')
                         ->setMethods(['touchIfTouching'])
                         ->setConstructorArgs($this->getRelationArguments())
                         ->getMock();
        $query = m::mock('stdClass');
        $query->shouldReceive('from')->once()->with('user_role')->andReturn($query);
        $query->shouldReceive('where')->once()->with('user_id', 1)->andReturn($query);
        $query->shouldReceive('whereIn')->never();
        $query->shouldReceive('delete')->once()->andReturn(true);
        $relation->getQuery()->shouldReceive('getQuery')->andReturn($mockQueryBuilder = m::mock('stdClass'));
        $mockQueryBuilder->shouldReceive('newQuery')->once()->andReturn($query);
        $relation->expects($this->once())->method('touchIfTouching');
        $this->assertTrue($relation->detach());
    }

    public function testDetachRemovesPivotTableRecord()
    {
        $relation = $this->getMockBuilder('Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray')
                         ->setMethods(['touchIfTouching'])
                         ->setConstructorArgs($this->getRelationArguments())
                         ->getMock();
        $query = m::mock('stdClass');
        $query->shouldReceive('from')->once()->with('user_role')->andReturn($query);
        $query->shouldReceive('where')->once()->with('user_id', 1)->andReturn($query);
        $query->shouldReceive('whereIn')->once()->with('role_id', [1, 2, 3]);
        $query->shouldReceive('delete')->once()->andReturn(true);
        $relation->getQuery()->shouldReceive('getQuery')->andReturn($mockQueryBuilder = m::mock('stdClass'));
        $mockQueryBuilder->shouldReceive('newQuery')->once()->andReturn($query);
        $relation->expects($this->once())->method('touchIfTouching');
        $this->assertTrue($relation->detach([1, 2, 3]));
        $this->assertTrue(true);//todo come up with a "real" test
    }

    public function testDetachWithSingleIDRemovesPivotTableRecord()
    {
        $relation = $this->getMockBuilder('Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray')
                         ->setMethods(['touchIfTouching'])
                         ->setConstructorArgs($this->getRelationArguments())
                         ->getMock();
        $query = m::mock('stdClass');
        $query->shouldReceive('from')->once()->with('user_role')->andReturn($query);
        $query->shouldReceive('where')->once()->with('user_id', 1)->andReturn($query);
        $query->shouldReceive('whereIn')->once()->with('role_id', [1]);
        $query->shouldReceive('delete')->once()->andReturn(true);
        $relation->getQuery()->shouldReceive('getQuery')->andReturn($mockQueryBuilder = m::mock('stdClass'));
        $mockQueryBuilder->shouldReceive('newQuery')->once()->andReturn($query);
        $relation->expects($this->once())->method('touchIfTouching');
        $this->assertTrue($relation->detach([1]));
        $this->assertTrue(true);//todo come up with a "real" test
    }

    public function testEagerConstraintsAreProperlyAdded()
    {
        $relation = $this->getRelation();
        $relation->getParent()->shouldReceive('getKeyName')->once()->andReturn('id');
        $relation->getQuery()->shouldReceive('whereIn')->once()->with('user_role.user_id', [1, 2]);
        $model1 = new EloquentBelongsToManyModelStub;
        $model1->id = 1;
        $model2 = new EloquentBelongsToManyModelStub;
        $model2->id = 2;
        $relation->addEagerConstraints([$model1, $model2]);
        $this->assertTrue(true);//todo come up with a "real" test
    }

    public function testFindManyMethod()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[get]',
            $this->getRelationArguments()
        );
        $relation->shouldReceive('get')->once()->andReturn(
            new Collection([new stdClass, new stdClass])
        );
        $relation->shouldReceive('whereIn')->with('roles.id', ['foo', 'bar'])->once()->andReturn($relation);
        $result = $relation->findMany(['foo', 'bar']);
        $this->assertEquals(2, count($result));
        $this->assertInstanceOf(stdClass::class, $result->first());
    }

    public function testFindMethod()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[first]',
            $this->getRelationArguments()
        );
        $relation->shouldReceive('first')->once()->andReturn(new stdClass);
        $relation->shouldReceive('where')->with('roles.id', '=', 'foo')->once()->andReturn($relation);
        $this->assertInstanceOf(stdClass::class, $relation->find('foo'));
    }

    public function testFindOrNewMethodFindsModel()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[find]',
            $this->getRelationArguments()
        );
        $relation->shouldReceive('find')->once()->with('foo', ['*'])->andReturn($model = m::mock('stdClass'));
        $relation->getRelated()->shouldReceive('newInstance')->never();
        $this->assertInstanceOf(stdClass::class, $relation->findOrNew('foo'));
    }

    public function testFindOrNewMethodReturnsNewModel()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[find]',
            $this->getRelationArguments()
        );
        $relation->shouldReceive('find')->once()->with('foo', ['*'])->andReturn(null);
        $relation->getRelated()->shouldReceive('newInstance')->once()->andReturn($model = m::mock('stdClass'));
        $this->assertInstanceOf(stdClass::class, $relation->findOrNew('foo'));
    }

    public function testFirstMethod()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[get]',
            $this->getRelationArguments()
        );
        $relation->shouldReceive('get')
                 ->once()
                 ->andReturn(new Collection([new stdClass]));
        $relation->shouldReceive('take')->once()->with(1)->andReturn($relation);
        $this->assertInstanceOf(stdClass::class, $relation->first());
    }

    public function testFirstOrCreateMethodFindsFirstModel()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[where,create]',
            $this->getRelationArguments()
        );
        $relation->shouldReceive('where')->once()->with(['foo'])->andReturn($relation->getQuery());
        $relation->getQuery()->shouldReceive('first')->once()->andReturn(null);
        $relation->getRelated()->shouldReceive('where')->once()->with(['foo'])->andReturn($relation->getQuery());
        $relation->getQuery()->shouldReceive('first')->once()->andReturn(new BaseModel(['id' => 1]));
        $relation->shouldReceive('create')->never();
        $query = $relation->getQuery()->getQuery();
        $query->shouldReceive('newQuery->from')->once()->andReturn($query);
        $query->shouldReceive('insert')->once()
              ->with([
                  [
                      'user_id' => 1,
                      'role_id' => 1,
                  ],
              ])
              ->andReturn(1);
        $relation->getRelated()->shouldReceive('touches')
                 ->once()
                 ->with(m::type('string'))
                 ->andReturn(false);
        $relation->getParent()->shouldReceive('touches')
                 ->once()
                 ->with('relation_name')
                 ->andReturn(false);
        $this->assertInstanceOf(BaseModel::class, $relation->firstOrCreate(['foo']));
    }

    public function testFirstOrCreateMethodReturnsNewModel()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[where,create]',
            $this->getRelationArguments()
        );
        $relation->shouldReceive('where')->once()->with(['foo'])->andReturn($relation->getQuery());
        $relation->getQuery()->shouldReceive('first')->once()->andReturn(null);
        $relation->getRelated()->shouldReceive('where')->once()->with(['foo'])->andReturn($relation->getQuery());
        $relation->getQuery()->shouldReceive('first')->once()->andReturn(null);
        $relation->shouldReceive('create')->once()->with(['foo'], [], true)->andReturn($model = m::mock('stdClass'));
        $this->assertInstanceOf(stdClass::class, $relation->firstOrCreate(['foo']));
    }

    public function testFirstOrNewMethodFindsFirstModel()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[where]',
            $this->getRelationArguments()
        );
        $relation->getRelated()->shouldReceive('where')->once()->with(['foo'])->andReturn($relation->getQuery());
        $relation->getQuery()->shouldReceive('first')->once()->andReturn($model = m::mock('stdClass'));
        $relation->getRelated()->shouldReceive('newInstance')->never();
        $this->assertInstanceOf(stdClass::class, $relation->firstOrNew(['foo']));
    }

    public function testFirstOrNewMethodReturnsNewModel()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[where]',
            $this->getRelationArguments()
        );
        $relation->getRelated()->shouldReceive('where')->once()->with(['foo'])->andReturn($relation->getQuery());
        $relation->getQuery()->shouldReceive('first')->once()->andReturn(null);
        $relation->getRelated()->shouldReceive('newInstance')->once()->andReturn($model = m::mock('stdClass'));
        $this->assertInstanceOf(stdClass::class, $relation->firstOrNew(['foo']));
    }

    public function testModelsAreProperlyHydrated()
    {
        $model1 = new EloquentBelongsToManyModelStub;
        $model1->fill(['name' => 'taylor', 'pivot_user_id' => 1, 'pivot_role_id' => 2]);
        $model2 = new EloquentBelongsToManyModelStub;
        $model2->fill(['name' => 'dayle', 'pivot_user_id' => 3, 'pivot_role_id' => 4]);
        $models = [$model1, $model2];
        $baseBuilder = m::mock('Illuminate\Database\Query\Builder');
        $relation = $this->getRelation();
        $relation->getParent()->shouldReceive('getConnectionName')->andReturn('foo.connection');
        $relation->getQuery()->shouldReceive('addSelect')->once()->with(
            ['roles.*', 'user_role.user_id as pivot_user_id', 'user_role.role_id as pivot_role_id']
        )->andReturn($relation->getQuery());
        $relation->getQuery()->shouldReceive('getModels')->once()->andReturn($models);
        $relation->getQuery()->shouldReceive('eagerLoadRelations')->once()->with($models)->andReturn($models);
        $relation->getRelated()->shouldReceive('newCollection')->andReturnUsing(
            function ($array) {
                return new Collection($array);
            }
        );
        $relation->getQuery()->shouldReceive('getQuery')->once()->andReturn($baseBuilder);
        $results = $relation->get();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $results);
        // Make sure the foreign keys were set on the pivot models...
        $this->assertEquals('user_id', $results[0]->pivot->getForeignKey());
        $this->assertEquals('role_id', $results[0]->pivot->getOtherKey());
        $this->assertEquals('taylor', $results[0]->name);
        $this->assertEquals(1, $results[0]->pivot->user_id);
        $this->assertEquals(2, $results[0]->pivot->role_id);
        $this->assertEquals('foo.connection', $results[0]->pivot->getConnectionName());
        $this->assertEquals('dayle', $results[1]->name);
        $this->assertEquals(3, $results[1]->pivot->user_id);
        $this->assertEquals(4, $results[1]->pivot->role_id);
        $this->assertEquals('foo.connection', $results[1]->pivot->getConnectionName());
        $this->assertEquals('user_role', $results[0]->pivot->getTable());
        $this->assertTrue($results[0]->pivot->exists);
    }

    public function testModelsAreProperlyMatchedToParents()
    {
        $relation = $this->getRelation();
        $result1 = new EloquentBelongsToManyModelPivotStub;
        $result1->id = 1;
        $result1->pivot->user_id = 1;
        $result2 = new EloquentBelongsToManyModelPivotStub;
        $result2->id = 2;
        $result2->pivot->user_id = 2;
        $result3 = new EloquentBelongsToManyModelPivotStub;
        $result3->id = 3;
        $result3->pivot->user_id = 2;
        $model1 = new EloquentBelongsToManyModelStub;
        $model1->id = 1;
        $model2 = new EloquentBelongsToManyModelStub;
        $model2->id = 2;
        $model3 = new EloquentBelongsToManyModelStub;
        $model3->id = 3;
        $relation->getRelated()->shouldReceive('newCollection')->andReturnUsing(
            function ($array) {
                return new Collection($array);
            }
        );
        $models = $relation->match([$model1, $model2, $model3], new Collection([$result1, $result2, $result3]), 'foo');
        $this->assertEquals(1, $models[0]->foo[0]->pivot->user_id);
        $this->assertEquals(1, count($models[0]->foo));
        $this->assertEquals(2, $models[1]->foo[0]->pivot->user_id);
        $this->assertEquals(2, $models[1]->foo[1]->pivot->user_id);
        $this->assertEquals(2, count($models[1]->foo));
        $this->assertEquals(0, count($models[2]->foo));
    }

    public function testRelationIsProperlyInitialized()
    {
        $relation = $this->getRelation();
        $relation->getRelated()->shouldReceive('newCollection')->andReturnUsing(
            function ($array = []) {
                return new Collection($array);
            }
        );
        $model = m::mock('Illuminate\Database\Eloquent\Model');
        $model->shouldReceive('setRelation')->once()->with('foo', m::type('Illuminate\Database\Eloquent\Collection'));
        $models = $relation->initRelation([$model], 'foo');
        $this->assertEquals([$model], $models);
    }

    public function testSyncMethodConvertsCollectionToArrayOfKeys()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[attach,detach,touchIfTouching,formatSyncList]',
            $this->getRelationArguments()
        )->shouldAllowMockingProtectedMethods();
        $query = m::mock('stdClass');
        $query->shouldReceive('from')->once()->with('user_role')->andReturn($query);
        $query->shouldReceive('where')->once()->with('user_id', 1)->andReturn($query);
        $relation->getQuery()->shouldReceive('getQuery')->andReturn($mockQueryBuilder = m::mock('stdClass'));
        $mockQueryBuilder->shouldReceive('newQuery')->once()->andReturn($query);
        $query->shouldReceive('lists')->once()->with('role_id')->andReturn([1, 2, 3]);
        $collection = m::mock('Illuminate\Database\Eloquent\Collection');
        $collection->shouldReceive('pluck')->once()->with('id')->andReturn($collection);
        $collection->shouldReceive('toArray')->once()->andReturn([1, 2, 3]);
        $relation->shouldReceive('formatSyncList')->once()->with([1, 2, 3])->andReturn([1 => [], 2 => [], 3 => []]);
        $relation->sync($collection);
        $this->assertTrue(true);//todo come up with a "real" test
    }

    public function testSyncMethodDoesntReturnValuesThatWereNotUpdated()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[attach,detach,touchIfTouching,updateExistingPivot]',
            $this->getRelationArguments()
        );
        $query = m::mock('stdClass');
        $query->shouldReceive('from')->once()->with('user_role')->andReturn($query);
        $query->shouldReceive('where')->once()->with('user_id', 1)->andReturn($query);
        $relation->getQuery()->shouldReceive('getQuery')->andReturn($mockQueryBuilder = m::mock('stdClass'));
        $mockQueryBuilder->shouldReceive('newQuery')->once()->andReturn($query);
        $query->shouldReceive('lists')->once()->with('role_id')->andReturn([1, 2, 3]);
        $relation->shouldReceive('attach')->once()->with(4, ['foo' => 'bar'], false);
        $relation->shouldReceive('updateExistingPivot')->once()->with(3, ['baz' => 'qux'], false)->andReturn(false);
        $relation->shouldReceive('detach')->once()->with([1]);
        $relation->shouldReceive('touchIfTouching')->once();

        $this->assertEquals(
            ['attached' => [4], 'detached' => [1], 'updated' => []],
            $relation->sync([2, 3 => ['baz' => 'qux'], 4 => ['foo' => 'bar']])
        );
    }

    /**
     * @dataProvider syncMethodListProvider
     */
    public function testSyncMethodSyncsIntermediateTableWithGivenArray($list)
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[attach,detach]',
            $this->getRelationArguments()
        );
        $query = m::mock('stdClass');
        $query->shouldReceive('from')->once()->with('user_role')->andReturn($query);
        $query->shouldReceive('where')->once()->with('user_id', 1)->andReturn($query);
        $relation->getQuery()->shouldReceive('getQuery')->andReturn($mockQueryBuilder = m::mock('stdClass'));
        $mockQueryBuilder->shouldReceive('newQuery')->once()->andReturn($query);
        $query->shouldReceive('lists')->once()->with('role_id')->andReturn([1, 2, 3]);
        $relation->shouldReceive('attach')->once()->with('x', [], false);
        $relation->shouldReceive('detach')->once()->with([1]);
        $relation->getRelated()->shouldReceive('touches')->andReturn(false);
        $relation->getParent()->shouldReceive('touches')->andReturn(false);
        $this->assertEquals(['attached' => ['x'], 'detached' => [1], 'updated' => []], $relation->sync($list));
    }

    public function testSyncMethodSyncsIntermediateTableWithGivenArrayAndAttributes()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[attach,detach,touchIfTouching,updateExistingPivot]',
            $this->getRelationArguments()
        );
        $query = m::mock('stdClass');
        $query->shouldReceive('from')->once()->with('user_role')->andReturn($query);
        $query->shouldReceive('where')->once()->with('user_id', 1)->andReturn($query);
        $relation->getQuery()->shouldReceive('getQuery')->andReturn($mockQueryBuilder = m::mock('stdClass'));
        $mockQueryBuilder->shouldReceive('newQuery')->once()->andReturn($query);
        $query->shouldReceive('lists')->once()->with('role_id')->andReturn([1, 2, 3]);
        $relation->shouldReceive('attach')->once()->with(4, ['foo' => 'bar'], false);
        $relation->shouldReceive('updateExistingPivot')->once()->with(3, ['baz' => 'qux'], false)->andReturn(true);
        $relation->shouldReceive('detach')->once()->with([1]);
        $relation->shouldReceive('touchIfTouching')->once();
        $this->assertEquals(
            ['attached' => [4], 'detached' => [1], 'updated' => [3]],
            $relation->sync([2, 3 => ['baz' => 'qux'], 4 => ['foo' => 'bar']])
        );
    }

    public function testTimestampsCanBeRetrievedProperly()
    {
        $model1 = new EloquentBelongsToManyModelStub;
        $model1->fill(['name' => 'taylor', 'pivot_user_id' => 1, 'pivot_role_id' => 2]);
        $model2 = new EloquentBelongsToManyModelStub;
        $model2->fill(['name' => 'dayle', 'pivot_user_id' => 3, 'pivot_role_id' => 4]);
        $models = [$model1, $model2];
        $baseBuilder = m::mock('Illuminate\Database\Query\Builder');
        $relation = $this->getRelation()->withTimestamps();
        $relation->getParent()->shouldReceive('getConnectionName')->andReturn('foo.connection');
        $relation->getQuery()->shouldReceive('addSelect')->once()->with(
            [
                'roles.*',
                'user_role.user_id as pivot_user_id',
                'user_role.role_id as pivot_role_id',
                'user_role.created_at as pivot_created_at',
                'user_role.updated_at as pivot_updated_at',
            ]
        )->andReturn($relation->getQuery());
        $relation->getQuery()->shouldReceive('getModels')->once()->andReturn($models);
        $relation->getQuery()->shouldReceive('eagerLoadRelations')->once()->with($models)->andReturn($models);
        $relation->getRelated()->shouldReceive('newCollection')->andReturnUsing(
            function ($array) {
                return new Collection($array);
            }
        );
        $relation->getQuery()->shouldReceive('getQuery')->once()->andReturn($baseBuilder);
        $results = $relation->get();
        $this->assertCount(2, $results);
    }

    public function testTouchIfTouching()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[touch]',
            $this->getRelationArguments()
        );
        $relation->getParent()->shouldReceive('touch')->once();
        $relation->getParent()->shouldReceive('touches')->once()->with('relation_name')->andReturn(true);
        $relation->getRelated()->shouldReceive('touches')->once()->with(m::type('string'))->andReturn(true);
        $relation->shouldReceive('touch')->once();
        $relation->touchIfTouching();
        $this->assertTrue(true);//todo come up with a "real" test
    }

    public function testTouchMethodSyncsTimestamps()
    {
        $relation = $this->getRelation();
        $relation->getRelated()->shouldReceive('getUpdatedAtColumn')->andReturn('updated_at');
        $relation->getRelated()->shouldReceive('freshTimestampString')->andReturn(100);
        $relation->getQuery()->shouldReceive('select')->once()->with('roles.id')->andReturn($relation->getQuery());
        $relation->getQuery()->shouldReceive('pluck')->once()->with('id')->andReturn([1, 2, 3]);
        $relation->getRelated()
                 ->shouldReceive('newQueryWithoutRelationships')
                 ->once()
                 ->andReturn($query = m::mock('stdClass'));
        $query->shouldReceive('whereIn')->once()->with('id', [1, 2, 3])->andReturn($query);
        $query->shouldReceive('update')->once()->with(['updated_at' => 100]);
        $relation->touch();
        $this->assertTrue(true);//todo come up with a "real" test
    }

    public function testUpdateOrCreateMethodFindsFirstModelAndUpdates()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[where,create]',
            $this->getRelationArguments()
        );
        $relation->shouldReceive('where')->once()->with(['foo'])->andReturn($relation->getQuery());
        $relation->getQuery()->shouldReceive('first')->once()->andReturn(null);
        $relation->getRelated()->shouldReceive('where')->once()->with(['foo'])->andReturn($relation->getQuery());
        $model = m::mock(BaseModel::class.'[save]');
        $model->id = 1;
        $relation->getQuery()->shouldReceive('first')->once()->andReturn($model);
        $model->shouldReceive('save')->once()->with(['touch' => false]);
        $relation->shouldReceive('create')->never();
        $query = $relation->getQuery()->getQuery();
        $query->shouldReceive('newQuery->from')->once()->andReturn($query);
        $query->shouldReceive('insert')->once()
              ->with([
                  [
                      'user_id' => 1,
                      'role_id' => 1,
                  ],
              ])
              ->andReturn(1);
        $relation->getRelated()->shouldReceive('touches')
                 ->once()
                 ->with(m::type('string'))
                 ->andReturn(false);
        $relation->getParent()->shouldReceive('touches')
                 ->once()
                 ->with('relation_name')
                 ->andReturn(false);
        $this->assertInstanceOf(BaseModel::class, $relation->updateOrCreate(['foo']));
    }

    public function testUpdateOrCreateMethodReturnsNewModel()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[where,create]',
            $this->getRelationArguments()
        );
        $relation->shouldReceive('where')->once()->with(['bar'])->andReturn($relation->getQuery());
        $relation->getQuery()->shouldReceive('first')->once()->andReturn(null);
        $relation->getRelated()->shouldReceive('where')->once()->with(['bar'])->andReturn($relation->getQuery());
        $relation->getQuery()->shouldReceive('first')->once()->andReturn(null);
        $relation->shouldReceive('create')
                 ->once()
                 ->with(['bar', 'foo'], [], true)
                 ->andReturn($model = m::mock('stdClass'));
        $this->assertInstanceOf(stdClass::class, $relation->updateOrCreate(['bar'], ['foo']));
    }

    public function testWherePivotParamsUsedForNewQueries()
    {
        $relation = m::mock(
            'Smorken\Ext\Database\Eloquent\Relations\BelongsToManyArray[formatSyncList]',
            $this->getRelationArguments()
        )->shouldAllowMockingProtectedMethods();
        // we expect to call $relation->wherePivot()
        // This is our test! The wherePivot() params also need to be called
        $relation->shouldReceive('where')->once()->with('user_role.foo', '=', 'bar', 'and')->andReturn($relation);
        $relation->wherePivot('foo', '=', 'bar');
        // Our sync() call will produce a new query
        $mockQueryBuilder = m::mock('stdClass');
        $query = m::mock('stdClass');
        $relation->getQuery()->shouldReceive('getQuery')->andReturn($mockQueryBuilder);
        $mockQueryBuilder->shouldReceive('newQuery')->once()->andReturn($query);
        // BelongsToMany::newPivotStatement() sets this
        $query->shouldReceive('from')->once()->with('user_role')->andReturn($query);
        // BelongsToMany::newPivotQuery() sets this
        $query->shouldReceive('where')->once()->with('user_id', 1)->andReturn($query);
        $query->shouldReceive('where')->once()->with('foo', '=', 'bar')->andReturn($query);
        // This is so $relation->sync() works
        $query->shouldReceive('lists')->once()->with('role_id')->andReturn([1, 2, 3]);
        $relation->shouldReceive('formatSyncList')->once()->with([1, 2, 3])->andReturn([1 => [], 2 => [], 3 => []]);
        $relation->sync([1, 2, 3]); // triggers the whole process above
        $this->assertTrue(true);//todo come up with a "real" test
    }
}

class EloquentBelongsToManyModelPivotStub extends Model
{

    public $pivot;

    public function __construct()
    {
        $this->pivot = new EloquentBelongsToManyPivotStub;
    }
}

class EloquentBelongsToManyModelStub extends Model
{

    protected $guarded = [];
}

class EloquentBelongsToManyPivotStub
{

    public $user_id;
}
