<?php

namespace Smorken\Ext\Database\Tests\Unit\Eloquent;

use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Eloquent\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\Eloquent\Model;
use Smorken\Ext\Database\Eloquent\Relations\CompositeHasOne;
use Smorken\Ext\Database\SQLiteConnection;

class CompositeHasOneTest extends TestCase
{

    public function setUp(): void
    {
        date_default_timezone_set('UTC');
    }

    public function tearDown(): void
    {
        m::close();
    }

    public function testAddEagerConstraints()
    {
        [$sut, $parentpdo, $relatedpdo] = $this->getSutAndMocks();
        $models = [
            new ParentHasOneStub(['p_1' => 2, 'p_2' => 2]),
            new ParentHasOneStub(['p_1' => 3, 'p_2' => 3]),
            new ParentHasOneStub(['p_1' => 4, 'p_2' => 4]),
        ];
        $results = [
            ['id' => 1, 'r_1' => 2, 'r_2' => 2],
            ['id' => 2, 'r_1' => 2, 'r_2' => 2],
        ];
        $pdostmt = m::mock('\PDOStatement[prepare,execute,fetchAll,bindValue,setFetchMode]');
        $pdostmt->shouldReceive('setFetchMode')
                ->once()
                ->with(5);
        $relatedpdo->shouldReceive('prepare')->with(
            'select * from "related" where (("related"."r_1" = ? and "related"."r_2" = ?) or ("related"."r_1" = ? and "related"."r_2" = ?) or ("related"."r_1" = ? and "related"."r_2" = ?) or ("related"."r_1" = ? and "related"."r_2" = ?))'
        )->andReturn($pdostmt);
        foreach ([0 => 1, 1 => 1, 2 => 2, 3 => 2, 4 => 3, 5 => 3, 6 => 4, 7 => 4] as $k => $v) {
            $pdostmt->shouldReceive('bindValue')->with($k + 1, $v, m::any());
        }
        $pdostmt->shouldReceive('execute')->once();
        $pdostmt->shouldReceive('fetchAll')->andReturn($results);
        $sut->addEagerConstraints($models);
        $r = $sut->getEager();
        $this->assertInstanceOf(RelatedHasOneStub::class, $r->first());
        $this->assertCount(2, $r);
    }

    public function testCreateSetsParentDataOnRelation()
    {
        [$sut, $parentpdo, $relatedpdo] = $this->getSutAndMocks();
        $pdostmt = m::mock('\PDOStatement[prepare,execute,fetchAll,bindValue,setFetchMode]');
        $relatedpdo->shouldReceive('prepare')->with(
            'insert into "related" ("r_1", "r_2", "updated_at", "created_at") values (?, ?, ?, ?)'
        )->andReturn($pdostmt);
        $pdostmt->shouldReceive('bindValue')->times(4)->withArgs(
            function ($i, $v, $type) {
                return $v === 1 || strpos($v, '-') !== false;
            }
        );
        $pdostmt->shouldReceive('execute')->once();
        $relatedpdo->shouldReceive('lastInsertId')->andReturn(99);
        $m = $sut->create([]);
        $this->assertInstanceOf(RelatedHasOneStub::class, $m);
        $this->assertEquals(99, $m->id);
    }

    public function testGetRelationCountQuery()
    {
        [$sut, $parentpdo, $relatedpdo, $parent_model, $related_model] = $this->getSutAndMocks();
        $q = $sut->getRelationCountQuery($related_model->newQuery(), $parent_model->newQuery());
        $sql = $q->toSql();
        $expected = 'select count(*) from "related" where ("parent"."p_1" = "related"."r_1" and "parent"."p_2" = "related"."r_2")';
        $this->assertEquals($expected, $sql);
    }

    public function testGetRelationCountQueryForSelf()
    {
        [$sut, $parentpdo, $relatedpdo, $parent_model, $related_model] = $this->getSutAndMocks();
        $q = $sut->getRelationCountQuery($parent_model->newQuery(), $parent_model->newQuery());
        $sql = $q->toSql();
        $reserved = $this->getCountHash($sut);
        $expected = sprintf('select count(*) from "parent" as "%1$s" where ("parent"."p_1" = "%1$s"."r_1" and "parent"."p_2" = "%1$s"."r_2")',
            $reserved);
        $this->assertEquals($expected, $sql);
    }

    public function testGetResultsWithDefaultConstraints()
    {
        [$sut, $parentpdo, $relatedpdo, $parent_model, $related_model] = $this->getSutAndMocks();
        $pdostmt = m::mock('\PDOStatement[prepare,execute,fetchAll,bindValue,setFetchMode]');
        $pdostmt->shouldReceive('setFetchMode')
                ->once()
                ->with(5);
        $relatedpdo->shouldReceive('prepare')->with(
            'select * from "related" where (("related"."r_1" = ? and "related"."r_2" = ?)) limit 1'
        )->andReturn($pdostmt);
        foreach ([0 => 1, 1 => 1] as $k => $v) {
            $pdostmt->shouldReceive('bindValue')->with($k + 1, $v, m::any());
        }
        $pdostmt->shouldReceive('execute')->once();
        $pdostmt->shouldReceive('fetchAll')->andReturn([]);
        $r = $sut->getResults();
        $this->assertNull($r);
    }

    public function testModelsAreMatchedToParents()
    {
        [$sut, $parentpdo, $relatedpdo] = $this->getSutAndMocks();
        $models = [
            new ParentHasOneStub(['p_1' => 2, 'p_2' => 2]),
            new ParentHasOneStub(['p_1' => 3, 'p_2' => 3]),
            new ParentHasOneStub(['p_1' => 4, 'p_2' => 4]),
        ];
        $results = [
            new RelatedHasOneStub(['id' => 1, 'r_1' => 2, 'r_2' => 2]),
            new RelatedHasOneStub(['id' => 2, 'r_1' => 3, 'r_2' => 3]),
            new RelatedHasOneStub(['id' => 3, 'r_1' => 3, 'r_2' => 3]),
        ];
        $results = new Collection($results);
        $sut->initRelation($models, 'foo');
        $matches = $sut->match($models, $results, 'foo');
        $this->assertCount(3, $matches);
        foreach ($matches as $m) {
            if (($m->p_1 === 2 && $m->p_2 === 2) || ($m->p_1 === 3 && $m->p_2 === 3)) {
                $this->assertInstanceOf(RelatedHasOneStub::class, $m->foo);
            } else {
                $this->assertNull($m->foo);
            }
        }
    }

    protected function getCountHash($sut): string
    {
        $reserved = $sut->getRelationCountHash(false);
        $parts = explode('_', $reserved);
        $num = $parts[2];
        $parts[2] = (int) $num - 1;
        return implode('_', $parts);
    }

    protected function getModel($model_class, $attrs = [])
    {
        $pdo = m::mock(\PDO::class);
        $connections = [
            'test' => new SQLiteConnection($pdo),
        ];
        $resolver = new ConnectionResolver($connections);
        $resolver->setDefaultConnection('test');
        $model = new $model_class($attrs);
        $model_class::setConnectionResolver($resolver);
        return [$model, $pdo];
    }

    protected function getSutAndMocks($parent = null)
    {
        [$pm, $parent_pdo] = $this->getModel(ParentHasOneStub::class, ['p_1' => 1, 'p_2' => 1]);
        $parent = $parent ?: $pm;
        [$rm, $related_pdo] = $this->getModel(RelatedHasOneStub::class);
        $builder = $rm->newQuery();
        $sut = new CompositeHasOne(
            $builder,
            $parent,
            ['r_1', 'r_2'],
            ['p_1', 'p_2'],
            'relation'
        );
        return [$sut, $parent_pdo, $related_pdo, $pm, $rm];
    }
}

class ParentHasOneStub extends Model
{

    protected $connection = 'test';

    protected $fillable = ['p_1', 'p_2'];

    protected $table = 'parent';
}

class RelatedHasOneStub extends Model
{

    protected $connection = 'test';

    protected $fillable = ['r_1', 'r_2'];

    protected $table = 'related';
}
