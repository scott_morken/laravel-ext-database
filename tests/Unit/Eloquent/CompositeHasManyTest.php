<?php

namespace Smorken\Ext\Database\Tests\Unit\Eloquent;

use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Expression;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\Eloquent\ExpressionValue;
use Smorken\Ext\Database\Eloquent\Model;
use Smorken\Ext\Database\Eloquent\Relations\CompositeHasMany;
use Smorken\Ext\Database\SQLiteConnection;

class CompositeHasManyTest extends TestCase
{

    public function setUp(): void
    {
        date_default_timezone_set('UTC');
    }

    public function tearDown(): void
    {
        m::close();
    }

    public function testAddEagerConstraints()
    {
        [$sut, $parentpdo, $relatedpdo] = $this->getSutAndMocks();
        $models = [
            new ParentHasManyStub(['p_1' => 2, 'p_2' => 2]),
            new ParentHasManyStub(['p_1' => 3, 'p_2' => 3]),
            new ParentHasManyStub(['p_1' => 4, 'p_2' => 4]),
        ];
        $results = [
            ['id' => 1, 'r_1' => 2, 'r_2' => 2],
            ['id' => 2, 'r_1' => 2, 'r_2' => 2],
        ];
        $pdostmt = m::mock('\PDOStatement[prepare,execute,fetchAll,bindValue,setFetchMode]');
        $pdostmt->shouldReceive('setFetchMode')
                ->once()
                ->with(5);
        $relatedpdo->shouldReceive('prepare')->with(
            'select * from "related" where (("related"."r_1" = ? and "related"."r_2" = ?) or ("related"."r_1" = ? and "related"."r_2" = ?) or ("related"."r_1" = ? and "related"."r_2" = ?) or ("related"."r_1" = ? and "related"."r_2" = ?))'
        )->andReturn($pdostmt);
        foreach ([0 => 1, 1 => 1, 2 => 2, 3 => 2, 4 => 3, 5 => 3, 6 => 4, 7 => 4] as $k => $v) {
            $pdostmt->shouldReceive('bindValue')->with($k + 1, $v, m::any());
        }
        $pdostmt->shouldReceive('execute')->once();
        $pdostmt->shouldReceive('fetchAll')->andReturn($results);
        $sut->addEagerConstraints($models);
        $r = $sut->getEager();
        $this->assertInstanceOf(RelatedHasManyStub::class, $r->first());
        $this->assertCount(2, $r);
    }

    public function testCreateMany()
    {
        [$sut, $parentpdo, $relatedpdo] = $this->getSutAndMocks();
        $pdostmt = m::mock('\PDOStatement[prepare,execute,fetchAll,bindValue,setFetchMode]');
        $relatedpdo->shouldReceive('prepare')->with(
            'insert into "related" ("r_1", "r_2", "updated_at", "created_at") values (?, ?, ?, ?)'
        )->andReturn($pdostmt);
        $pdostmt->shouldReceive('bindValue')->times(4)->withArgs(function ($i, $v, $type) {
            return $v === 1 || strpos($v, '-') !== false;
        });
        $pdostmt->shouldReceive('execute')->once();
        $relatedpdo->shouldReceive('lastInsertId')->andReturn(99);
        $m = $sut->createMany([[]]);
        $this->assertCount(1, $m);
        $this->assertInstanceOf(RelatedHasManyStub::class, $m[0]);
        $this->assertEquals(99, $m[0]->id);
    }

    public function testCreateSetsParentDataOnRelation()
    {
        [$sut, $parentpdo, $relatedpdo] = $this->getSutAndMocks();
        $pdostmt = m::mock('\PDOStatement[prepare,execute,fetchAll,bindValue,setFetchMode]');
        $relatedpdo->shouldReceive('prepare')->with(
            'insert into "related" ("r_1", "r_2", "updated_at", "created_at") values (?, ?, ?, ?)'
        )->andReturn($pdostmt);
        $pdostmt->shouldReceive('bindValue')->times(4);
        $pdostmt->shouldReceive('execute')->once()->andReturn(true);
        $relatedpdo->shouldReceive('lastInsertId')->andReturn(99);
        $m = $sut->create([]);
        $this->assertInstanceOf(RelatedHasManyStub::class, $m);
        $this->assertEquals(99, $m->id);
    }

    public function testFindOrNew()
    {
        [$sut, $parentpdo, $relatedpdo, $parent_model, $related_model] = $this->getSutAndMocks();
        $pdostmt = m::mock('\PDOStatement[prepare,execute,fetchAll,bindValue,setFetchMode]');
        $pdostmt->shouldReceive('setFetchMode')
                ->once()
                ->with(5);
        $relatedpdo->shouldReceive('prepare')->with(
            'select * from "related" where (("related"."r_1" = ? and "related"."r_2" = ?)) and "related"."id" = ? limit 1'
        )->andReturn($pdostmt);
        $pdostmt->shouldReceive('bindValue')->withArgs(
            function ($i, $v, $type) {
                return is_int($v) && $type === 1;
            }
        );
        $pdostmt->shouldReceive('execute')->once();
        $pdostmt->shouldReceive('fetchAll')->andReturn([]);
        $m = $sut->findOrNew(1);
        $this->assertNull($m->id);
        $this->assertEquals(1, $m->r_1);
        $this->assertEquals(1, $m->r_2);
    }

    public function testFirstOrNew()
    {
        [$sut, $parentpdo, $relatedpdo] = $this->getSutAndMocks();
        $pdostmt = m::mock('\PDOStatement[prepare,execute,fetchAll,bindValue,setFetchMode]');
        $pdostmt->shouldReceive('setFetchMode')
                ->once()
                ->with(5);
        $relatedpdo->shouldReceive('prepare')->with(
            'select * from "related" where (("related"."r_1" = ? and "related"."r_2" = ?)) and ("foo" = ?) limit 1'
        )->andReturn($pdostmt);
        $pdostmt->shouldReceive('bindValue')->withArgs(
            function ($i, $v, $type) {
                return $v === 1 || $v === 'bar';
            }
        );
        $pdostmt->shouldReceive('execute')->once();
        $pdostmt->shouldReceive('fetchAll')->andReturn([]);
        $m = $sut->firstOrNew(['foo' => 'bar']);
        $this->assertInstanceOf(RelatedHasManyStub::class, $m);
        $this->assertNull($m->id);
        $this->assertEquals(1, $m->r_1);
        $this->assertEquals(1, $m->r_2);
    }

    public function testGetRelationCountQuery()
    {
        [$sut, $parentpdo, $relatedpdo, $parent_model, $related_model] = $this->getSutAndMocks();
        $q = $sut->getRelationCountQuery($related_model->newQuery(), $parent_model->newQuery());
        $sql = $q->toSql();
        $expected = 'select count(*) from "related" where ("parent"."p_1" = "related"."r_1" and "parent"."p_2" = "related"."r_2")';
        $this->assertEquals($expected, $sql);
    }

    public function testGetRelationCountQueryForSelf()
    {
        [$sut, $parentpdo, $relatedpdo, $parent_model, $related_model] = $this->getSutAndMocks();
        $q = $sut->getRelationCountQuery($parent_model->newQuery(), $parent_model->newQuery());
        $sql = $q->toSql();
        $reserved = $this->getCountHash($sut);
        $expected = sprintf('select count(*) from "parent" as "%1$s" where ("parent"."p_1" = "%1$s"."r_1" and "parent"."p_2" = "%1$s"."r_2")',
            $reserved);
        $this->assertEquals($expected, $sql);
    }

    /**
     * $parent->p_1 = 12345
     * $related->r_1 = 123
     * $related->r_2 = 45
     */
    public function testHasManyWithExpressionValueOnParent()
    {
        [$sut, $parentpdo, $relatedpdo, $parent_model, $related_model] = $this->getSutAndMocks();
        $outerfunc = function ($start, $length) {
            return function ($ev, $model) use ($start, $length) {
                return substr($model->p_1, $start, $length);
            };
        };
        $e1 = new Expression('SUBSTR(p_1, 1, 3)');
        $e2 = new Expression('SUBSTR(p_1, 3, 2)');
        $p1_1 = new ExpressionValue('p_1', $e1, $outerfunc(0, 3));
        $p1_2 = new ExpressionValue('p_1', $e2, $outerfunc(3, 2));
        $parent_model->p_1 = '12345';
        $sut = new CompositeHasMany(
            $related_model->newQuery(),
            $parent_model,
            [
                'r_1',
                'r_2',
            ],
            [
                $p1_1,
                $p1_2,
            ],
            'relation'
        );
        $models = [
            new ParentHasManyStub(['p_1' => 23456, 'p_2' => 2]),
            new ParentHasManyStub(['p_1' => 34567, 'p_2' => 3]),
            new ParentHasManyStub(['p_1' => 45678, 'p_2' => 4]),
        ];
        $results = [
            ['id' => 1, 'r_1' => 234, 'r_2' => 56],
            ['id' => 2, 'r_1' => 234, 'r_2' => 56],
            ['id' => 3, 'r_1' => 456, 'r_2' => 78],
        ];
        $pdostmt = m::mock('\PDOStatement[prepare,execute,fetchAll,bindValue,setFetchMode]');
        $pdostmt->shouldReceive('setFetchMode')
                ->once()
                ->with(5);
        $relatedpdo->shouldReceive('prepare')->with(
            'select * from "related" where (("related"."r_1" = ? and "related"."r_2" = ?) or ("related"."r_1" = ? and "related"."r_2" = ?) or ("related"."r_1" = ? and "related"."r_2" = ?) or ("related"."r_1" = ? and "related"."r_2" = ?))'
        )->andReturn($pdostmt);
        foreach ([
                     0 => '123', 1 => '45', 2 => '234', 3 => '56', 4 => '345', 5 => '67', 6 => '456', 7 => '78',
                 ] as $k => $v) {
            $pdostmt->shouldReceive('bindValue')->with($k + 1, $v, m::any());
        }
        $pdostmt->shouldReceive('execute')->once();
        $pdostmt->shouldReceive('fetchAll')->andReturn($results);
        $sut->addEagerConstraints($models);
        $r = $sut->getEager();
        $this->assertCount(3, $r);
    }

    /**
     * $parent->p_1 = 123
     * $parent->p_2 = 45
     * $related->r_1 = 12345
     */
    public function testHasManyWithExpressionValueOnRelation()
    {
        [$sut, $parentpdo, $relatedpdo, $parent_model, $related_model] = $this->getSutAndMocks();
        $outerfunc = function ($start, $length) {
            return function ($ev, $model) use ($start, $length) {
                return substr($model->p_1, $start, $length);
            };
        };
        $e1 = new Expression('SUBSTR(r_1, 1, 3)');
        $e2 = new Expression('SUBSTR(r_1, 3, 2)');
        $r1_1 = new ExpressionValue('r_1', $e1, $outerfunc(0, 3));
        $r1_2 = new ExpressionValue('r_1', $e2, $outerfunc(3, 2));
        $parent_model->p_1 = '12345';
        $sut = new CompositeHasMany(
            $related_model->newQuery(),
            $parent_model,
            [
                $r1_1,
                $r1_2,
            ],
            [
                'p_1',
                'p_2',
            ],
            'relation'
        );
        $models = [
            new ParentHasManyStub(['p_1' => 234, 'p_2' => 56]),
            new ParentHasManyStub(['p_1' => 345, 'p_2' => 67]),
            new ParentHasManyStub(['p_1' => 456, 'p_2' => 78]),
        ];
        $results = [
            ['id' => 1, 'r_1' => 23456, 'r_2' => 99],
            ['id' => 2, 'r_1' => 23456, 'r_2' => 99],
            ['id' => 3, 'r_1' => 45678, 'r_2' => 99],
        ];
        $pdostmt = m::mock('\PDOStatement[prepare,execute,fetchAll,bindValue,setFetchMode]');
        $pdostmt->shouldReceive('setFetchMode')
                ->once()
                ->with(5);
        $relatedpdo->shouldReceive('prepare')->with(
            'select * from "related" where ((SUBSTR(r_1, 1, 3) = ? and SUBSTR(r_1, 3, 2) = ?) or (SUBSTR(r_1, 1, 3) = ? and SUBSTR(r_1, 3, 2) = ?) or (SUBSTR(r_1, 1, 3) = ? and SUBSTR(r_1, 3, 2) = ?) or (SUBSTR(r_1, 1, 3) = ? and SUBSTR(r_1, 3, 2) = ?))'
        )->andReturn($pdostmt);
        foreach ([0 => '12345', 1 => 1, 2 => 234, 3 => 56, 4 => 345, 5 => 67, 6 => 456, 7 => 78] as $k => $v) {
            $pdostmt->shouldReceive('bindValue')->with($k + 1, $v, m::any());
        }
        $pdostmt->shouldReceive('execute')->once();
        $pdostmt->shouldReceive('fetchAll')->andReturn($results);
        $sut->addEagerConstraints($models);
        $r = $sut->getEager();
        $this->assertCount(3, $r);
    }

    public function testModelsAreMatchedToParents()
    {
        [$sut, $parentpdo, $relatedpdo] = $this->getSutAndMocks();
        $models = [
            new ParentHasManyStub(['p_1' => 2, 'p_2' => 2]),
            new ParentHasManyStub(['p_1' => 3, 'p_2' => 3]),
            new ParentHasManyStub(['p_1' => 4, 'p_2' => 4]),
        ];
        $results = [
            new RelatedHasManyStub(['id' => 1, 'r_1' => 2, 'r_2' => 2]),
            new RelatedHasManyStub(['id' => 2, 'r_1' => 3, 'r_2' => 3]),
            new RelatedHasManyStub(['id' => 3, 'r_1' => 3, 'r_2' => 3]),
        ];
        $results = new Collection($results);
        $sut->initRelation($models, 'foo');
        $matches = $sut->match($models, $results, 'foo');
        $this->assertCount(3, $matches);
        foreach ($matches as $m) {
            $relation = $m->foo;
            if ($m->p_1 === 2) {
                $this->assertCount(1, $relation);
            }
            if ($m->p_1 === 3) {
                $this->assertCount(2, $relation);
            }
            if ($m->p_1 === 4) {
                $this->assertCount(0, $relation);
            }
        }
    }

    public function testSave()
    {
        [$sut, $parentpdo, $relatedpdo, $parent_model, $related_model] = $this->getSutAndMocks();
        $m = new RelatedHasManyStub();
        $pdostmt = m::mock('\PDOStatement[prepare,execute,fetchAll,bindValue,setFetchMode]');
        $relatedpdo->shouldReceive('prepare')->with(
            'insert into "related" ("r_1", "r_2", "updated_at", "created_at") values (?, ?, ?, ?)'
        )->andReturn($pdostmt);
        $pdostmt->shouldReceive('bindValue')->times(4);
        $pdostmt->shouldReceive('execute')->once();
        $relatedpdo->shouldReceive('lastInsertId')->andReturn(25);
        $r = $sut->save($m);
        $this->assertEquals(25, $r->id);
        $this->assertEquals(1, $r->r_1);
        $this->assertEquals(1, $r->r_2);
    }

    public function testSaveMany()
    {
        [$sut, $parentpdo, $relatedpdo, $parent_model, $related_model] = $this->getSutAndMocks();
        $m = [new RelatedHasManyStub()];
        $pdostmt = m::mock('\PDOStatement[prepare,execute,fetchAll,bindValue,setFetchMode]');
        $relatedpdo->shouldReceive('prepare')->with(
            'insert into "related" ("r_1", "r_2", "updated_at", "created_at") values (?, ?, ?, ?)'
        )->andReturn($pdostmt);
        $pdostmt->shouldReceive('bindValue')->times(4);
        $pdostmt->shouldReceive('execute')->once();
        $relatedpdo->shouldReceive('lastInsertId')->andReturn(25);
        $r = $sut->saveMany($m);
        $this->assertCount(1, $r);
        $this->assertEquals(25, $r[0]->id);
        $this->assertEquals(1, $r[0]->r_1);
        $this->assertEquals(1, $r[0]->r_2);
    }

    public function testUpdate()
    {
        [$sut, $parentpdo, $relatedpdo] = $this->getSutAndMocks();
        $pdostmt = m::mock('\PDOStatement[prepare,execute,fetchAll,bindValue,rowCount,setFetchMode]');
        $relatedpdo->shouldReceive('prepare')->with(
            'update "related" set "updated_at" = ? where (("related"."r_1" = ? and "related"."r_2" = ?))'
        )->andReturn($pdostmt);
        $pdostmt->shouldReceive('bindValue')->times(3)->withArgs(
            function ($i, $v, $type) {
                return $v === 1 || strpos($v, '-') !== false;
            }
        );
        $pdostmt->shouldReceive('execute')->once();
        $pdostmt->shouldReceive('rowCount')->andReturn(2);
        $m = $sut->update([]);
        $this->assertEquals(2, $m);
    }

    protected function getCountHash($sut): string
    {
        $reserved = $sut->getRelationCountHash(false);
        $parts = explode('_', $reserved);
        $num = $parts[2];
        $parts[2] = (int) $num - 1;
        return implode('_', $parts);
    }

    protected function getModel($model_class, $attrs = [])
    {
        $pdo = m::mock(\PDO::class);
        $connections = [
            'test' => new SQLiteConnection($pdo),
        ];
        $resolver = new ConnectionResolver($connections);
        $resolver->setDefaultConnection('test');
        $model = new $model_class($attrs);
        $model_class::setConnectionResolver($resolver);
        return [$model, $pdo];
    }

    protected function getSutAndMocks($parent = null)
    {
        [$pm, $parent_pdo] = $this->getModel(ParentHasManyStub::class, ['p_1' => 1, 'p_2' => 1]);
        $parent = $parent ?: $pm;
        [$rm, $related_pdo] = $this->getModel(RelatedHasManyStub::class);
        $builder = $rm->newQuery();
        $sut = new CompositeHasMany(
            $builder,
            $parent,
            ['r_1', 'r_2'],
            ['p_1', 'p_2'],
            'relation'
        );
        return [$sut, $parent_pdo, $related_pdo, $pm, $rm];
    }
}

class ParentHasManyStub extends Model
{

    protected $connection = 'test';

    protected $fillable = ['p_1', 'p_2'];

    protected $table = 'parent';
}

class RelatedHasManyStub extends Model
{

    protected $connection = 'test';

    protected $fillable = ['r_1', 'r_2'];

    protected $table = 'related';
}
