<?php

namespace Smorken\Ext\Database\Tests\Unit\Connectors;

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 9/7/16
 * Time: 12:51 PM
 */

use Illuminate\Database\Query\Processors\SQLiteProcessor;
use Mockery as m;
use PDO;
use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\Connectors\OdbcConnector;
use Smorken\Ext\Database\Initializers\InitializerInterface;
use Smorken\Ext\Database\Query\Grammars\SQLiteGrammar;

class OdbcConnectorTest extends TestCase
{

    public function tearDown(): void
    {
        m::close();
    }

    public function testCreatesConnectionWithInitializer()
    {
        $pdo = m::mock('PDO');
        $config = [
            'driver' => 'odbc',
            'dsn' => 'sqlite3',
            'database' => ':memory:',
            'query_grammar' => SQLiteGrammar::class,
            'post_processor' => SQLiteProcessor::class,
            'initializer' => StubInit::class,
        ];
        $sut = $this->getMockBuilder(OdbcConnector::class)
                    ->setMethods(['createConnection'])->getMock();
        $sut->expects($this->once())->method('createConnection')
            ->with($this->equalTo('odbc:'.$config['dsn']), $config, $this->isType('array'))
            ->will($this->returnValue($pdo));
        $pdo->shouldReceive('foo')->once()->with('bar');
        $conn = $sut->connect($config);
        $this->assertInstanceOf('PDO', $conn);
    }

    public function testCreatesConnectionWithoutInitializer()
    {
        $pdo = m::mock('PDO');
        $config = [
            'driver' => 'odbc',
            'dsn' => 'sqlite3',
            'database' => ':memory:',
            'query_grammar' => SQLiteGrammar::class,
            'post_processor' => SQLiteProcessor::class,
        ];
        $sut = $this->getMockBuilder(OdbcConnector::class)
                    ->setMethods(['createConnection'])->getMock();
        $sut->expects($this->once())->method('createConnection')
            ->with($this->equalTo('odbc:'.$config['dsn']), $config, $this->isType('array'))
            ->will($this->returnValue($pdo));
        $conn = $sut->connect($config);
        $this->assertInstanceOf('PDO', $conn);
    }

    public function testNoDsnThrowsException()
    {
        $config = [
            'driver' => 'odbc',
            'database' => ':memory:',
            'query_grammar' => SQLiteGrammar::class,
            'post_processor' => SQLiteProcessor::class,
        ];
        $sut = new OdbcConnector();
        $this->expectException('\InvalidArgumentException');
        $this->expectExceptionMessage('DSN is required for an ODBC connection.');
        $sut->connect($config);
    }
}

class StubInit implements InitializerInterface
{

    /**
     * @param  PDO  $connection
     * @param $config
     * @param $options
     * @return null
     */
    public function init($connection, $config, $options)
    {
        $connection->foo('bar');
    }
}
