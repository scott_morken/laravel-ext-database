<?php

namespace Smorken\Ext\Database\Tests\Unit;

/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 7:46 AM
 */

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Yajra\Oci8\Oci8Connection;

class OracleConnectionTest extends TestCase
{

    public function setUp(): void
    {
        if (!class_exists(Oci8Connection::class)) {
            $this->markTestIncomplete('Oci8Connection not available.');
        }
        parent::setUp();
    }

    public function tearDown(): void
    {
        m::close();
    }

    public function testQueryBuilderInstanceOf()
    {
        $conn = $this->getMockConnection();
        $g = $conn->getQueryGrammar();
        $this->assertInstanceOf('Smorken\Ext\Database\Query\Grammars\Oci8Grammar', $g);
    }

    protected function getMockConnection($methods = [], $pdo = null)
    {
        $pdo = $pdo ?: new PDOStub;
        $defaults = ['getDefaultPostProcessor', 'getDefaultSchemaGrammar'];
        $methods = implode(',', array_merge($defaults, $methods));
        return m::mock(sprintf('Smorken\Ext\Database\Oci8Connection[%s]', $methods), [$pdo]);
    }
}
