<?php

namespace Smorken\Ext\Database\Tests\Unit;

/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 7:46 AM
 */

use Mockery as m;
use PHPUnit\Framework\TestCase;

class SqliteConnectionTest extends TestCase
{

    public function tearDown(): void
    {
        m::close();
    }

    public function testQueryBuilderInstanceOf()
    {
        $conn = $this->getMockConnection();
        $g = $conn->getQueryGrammar();
        $this->assertInstanceOf('Smorken\Ext\Database\Query\Grammars\SQLiteGrammar', $g);
    }

    protected function getMockConnection($methods = [], $pdo = null)
    {
        $pdo = $pdo ?: new PDOStub;
        $defaults = ['getDefaultPostProcessor', 'getDefaultSchemaGrammar'];
        $methods = implode(',', array_merge($defaults, $methods));
        return m::mock(sprintf('Smorken\Ext\Database\SQLiteConnection[%s]', $methods), [$pdo]);
    }
}
