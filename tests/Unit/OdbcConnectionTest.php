<?php

namespace Smorken\Ext\Database\Tests\Unit;

/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 7:46 AM
 */

use Illuminate\Database\Query\Processors\PostgresProcessor;
use Illuminate\Database\Schema\Builder;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\OdbcConnection;
use Smorken\Ext\Database\Query\Grammars\SQLiteGrammar;

class OdbcConnectionTest extends TestCase
{

    public function tearDown(): void
    {
        m::close();
    }

    public function testDefaultInstanceOf()
    {
        [$sut, $pdo] = $this->getSutAndMocks();
        $this->assertInstanceOf('Illuminate\Database\Query\Grammars\Grammar', $sut->getQueryGrammar());
        $this->assertNull($sut->getSchemaGrammar());
        $this->assertInstanceOf('Illuminate\Database\Schema\Builder', $sut->getSchemaBuilder());
        $this->assertInstanceOf('Illuminate\Database\Query\Processors\Processor', $sut->getPostProcessor());
    }

    public function testSetGrammar()
    {
        $config = [
            'query_grammar' => SQLiteGrammar::class,
        ];
        [$sut, $pdo] = $this->getSutAndMocks($config);
        $this->assertInstanceOf('Smorken\Ext\Database\Query\Grammars\SQLiteGrammar', $sut->getQueryGrammar());
    }

    public function testSetProcessor()
    {
        $config = [
            'post_processor' => PostgresProcessor::class,
        ];
        [$sut, $pdo] = $this->getSutAndMocks($config);
        $this->assertInstanceOf('Illuminate\Database\Query\Processors\PostgresProcessor', $sut->getPostProcessor());
    }

    public function testSetSchemaBuilder()
    {
        $config = [
            'schema_builder' => Builder::class,
        ];
        [$sut, $pdo] = $this->getSutAndMocks($config);
        $this->assertInstanceOf('Illuminate\Database\Schema\Builder', $sut->getSchemaBuilder());
    }

    protected function getMockConnection($methods = [], $pdo = null)
    {
        $pdo = $pdo ?: new PDOStub;
        $defaults = ['getDefaultPostProcessor', 'getDefaultSchemaGrammar'];
        $methods = implode(',', array_merge($defaults, $methods));
        return m::mock(sprintf('Smorken\Ext\Database\OdbcConnection[%s]', $methods), [$pdo]);
    }

    protected function getSutAndMocks($config = [])
    {
        $pdo = m::mock('\PDO');
        $sut = new OdbcConnection($pdo, 'db', '', $config);
        return [$sut, $pdo];
    }
}
