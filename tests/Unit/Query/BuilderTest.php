<?php

namespace Smorken\Ext\Database\Tests\Unit\Query;

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/29/16
 * Time: 10:38 AM
 */

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\Query\Builder;

class BuilderTest extends TestCase
{

    public function tearDown(): void
    {
        m::close();
    }

    public function testComplexJoin()
    {
        [$conn, $grammar, $proc] = $this->getMocks();
        $builder = $this->getBuilder($conn, $grammar, $proc);
        $builder->complexJoin('foo', 'bar', ['foo1' => 'bar1', 'foo2' => 'bar2']);
        $this->assertCount(1, $builder->joins);
        $this->assertEquals('inner', $builder->joins[0]->type);
        $this->assertEquals('foo', $builder->joins[0]->table);
        $this->assertCount(2, $builder->joins[0]->wheres);
        $this->assertEquals('foo.foo1', $builder->joins[0]->wheres[0]['first']);
        $this->assertEquals('bar.bar1', $builder->joins[0]->wheres[0]['second']);
    }

    public function testComplexJoinLeftJoin()
    {
        [$conn, $grammar, $proc] = $this->getMocks();
        $builder = $this->getBuilder($conn, $grammar, $proc);
        $builder->complexJoin('foo', 'bar', ['foo1' => 'bar1', 'foo2' => 'bar2'], 'leftJoin');
        $this->assertEquals('left', $builder->joins[0]->type);
    }

    public function testConcatenateSimple()
    {
        $values = ['foo', 'bar'];
        [$conn, $grammar, $proc] = $this->getMocks();
        $builder = $this->getBuilder($conn, $grammar, $proc);
        $r = $builder->concatenate($values);
        $this->assertEquals('CONCAT(foo, bar)', $r);
    }

    public function testConcatenateSingleReturnsColumn()
    {
        $values = ['foo'];
        [$conn, $grammar, $proc] = $this->getMocks();
        $builder = $this->getBuilder($conn, $grammar, $proc);
        $r = $builder->concatenate($values);
        $this->assertEquals('foo', $r);
    }

    public function testConcatenateWithSeparatorAddsToString()
    {
        $values = ['foo', 'bar'];
        [$conn, $grammar, $proc] = $this->getMocks();
        $grammar->shouldReceive('wrap')->once()->with('-')->andReturn("'-'");
        $builder = $this->getBuilder($conn, $grammar, $proc);
        $r = $builder->concatenate($values, '-');
        $this->assertEquals("CONCAT(foo, '-', bar)", $r);
    }

    public function testCreateOrUpdateCallsGrammarMethod()
    {
        [$conn, $grammar, $proc] = $this->getMocks();
        $builder = $this->getBuilder($conn, $grammar, $proc);
        $keys = ['foo', 'bar'];
        $values = ['a' => 1, 'b' => 2];
        $grammar->shouldReceive('compileCreateOrUpdate')->once()->with($builder, $keys, $values, [])->andReturn(
            ['sql', ['params']]
        );
        $conn->shouldReceive('affectingStatement')->with('sql', [':0' => 'params'])->andReturn(true);
        $this->assertTrue($builder->createOrUpdate($keys, $values));
    }

    public function testTemp(): void
    {
        [$conn, $grammar, $proc] = $this->getMocks();
        $builder = $this->getBuilder($conn, $grammar, $proc);
        $grammar->shouldReceive('compileTemp')->once()->with($builder, 'table', 'table_temp')->andReturn(
            ['sql', []]
        );
        $conn->shouldReceive('affectingStatement')->with('sql', [])->andReturn(true);
        $this->assertTrue($builder->temp('table', 'table_temp'));
    }

    protected function getBuilder($conn, $grammar, $processor)
    {
        return new Builder($conn, $grammar, $processor);
    }

    protected function getMocks()
    {
        $c = m::mock('Illuminate\Database\ConnectionInterface');
        $g = m::mock('Illuminate\Database\Query\Grammars\Grammar');
        $p = m::mock('Illuminate\Database\Query\Processors\Processor');
        return [$c, $g, $p];
    }
}
