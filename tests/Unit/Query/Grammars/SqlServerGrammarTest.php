<?php

namespace Smorken\Ext\Database\Tests\Unit\Query\Grammars;

/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 8:01 AM
 */

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\Query\Grammars\SqlServerGrammar;

class SqlServerGrammarTest extends TestCase
{

    /**
     * @var SqlServerGrammar
     *
     */
    protected $sut;

    public function setUp(): void
    {
        parent::setUp();
        $this->sut = new SqlServerGrammar();
    }

    public function tearDown(): void
    {
        m::close();
    }

    public function testCompileCreateOrUpdate()
    {
        $expected = "MERGE [tablename] AS target
        USING (VALUES((:0_test, :0_test1, :0_test2)))
          AS source([test], [test1], [test2])
          ON [target].[test]=[source].[test]
        WHEN MATCHED THEN
          UPDATE
          SET [target].[test1]=[source].[test1], [target].[test2]=[source].[test2]
        WHEN NOT MATCHED THEN
          INSERT ([test], [test1], [test2])
          VALUES([source].[test], [source].[test1], [source].[test2]);";
        $expected_bindings = [
            ':0_test' => 0,
            ':0_test1' => 1,
            ':0_test2' => 2,
        ];
        $keys = 'test';
        $data = ['test' => 0, 'test1' => 1, 'test2' => 2];
        $query = $this->setupQueryMock();
        $query->from = 'tablename';
        $sql = $this->sut->compileCreateOrUpdate($query, $keys, $data);
        $this->assertEquals($expected, $sql[0]);
        $this->assertEquals($expected_bindings, $sql[1]);
    }

    public function testCompileCreateOrUpdateLimitUpdate()
    {
        $expected = "MERGE [tablename] AS target
        USING (VALUES((:0_test, :0_test1, :0_test2)))
          AS source([test], [test1], [test2])
          ON [target].[test]=[source].[test]
        WHEN MATCHED THEN
          UPDATE
          SET [target].[test2]=[source].[test2]
        WHEN NOT MATCHED THEN
          INSERT ([test], [test1], [test2])
          VALUES([source].[test], [source].[test1], [source].[test2]);";
        $expected_bindings = [
            ':0_test' => 0,
            ':0_test1' => 1,
            ':0_test2' => 2,
        ];
        $keys = 'test';
        $data = ['test' => 0, 'test1' => 1, 'test2' => 2];
        $update = ['test2'];
        $query = $this->setupQueryMock();
        $query->from = 'tablename';
        $sql = $this->sut->compileCreateOrUpdate($query, $keys, $data, $update);
        $this->assertEquals($expected, $sql[0]);
        $this->assertEquals($expected_bindings, $sql[1]);
    }

    public function testCompileCreateOrUpdateMultipleKeys()
    {
        $expected = "MERGE [tablename] AS target
        USING (VALUES((:0_id1, :0_id2, :0_id3, :0_test, :0_test1, :0_test2)))
          AS source([id1], [id2], [id3], [test], [test1], [test2])
          ON [target].[id1]=[source].[id1] AND [target].[id2]=[source].[id2] AND [target].[id3]=[source].[id3]
        WHEN MATCHED THEN
          UPDATE
          SET [target].[test]=[source].[test], [target].[test1]=[source].[test1], [target].[test2]=[source].[test2]
        WHEN NOT MATCHED THEN
          INSERT ([id1], [id2], [id3], [test], [test1], [test2])
          VALUES([source].[id1], [source].[id2], [source].[id3], [source].[test], [source].[test1], [source].[test2]);";
        $expected_bindings = [
            ':0_test' => 0,
            ':0_test1' => 1,
            ':0_test2' => 2,
            ':0_id1' => '+1',
            ':0_id2' => '+2',
            ':0_id3' => '+3',
        ];
        $keys = ['id1', 'id2', 'id3'];
        $data = ['id1' => '+1', 'id2' => '+2', 'id3' => '+3', 'test' => 0, 'test1' => 1, 'test2' => 2];
        $query = $this->setupQueryMock();
        $query->from = 'tablename';
        $sql = $this->sut->compileCreateOrUpdate($query, $keys, $data);
        $this->assertEquals($expected, $sql[0]);
        $this->assertEquals($expected_bindings, $sql[1]);
    }

    public function testCompileCreateOrUpdateMultipleKeysLimitUpdate()
    {
        $expected = "MERGE [tablename] AS target
        USING (VALUES((:0_id1, :0_id2, :0_id3, :0_test, :0_test1, :0_test2)))
          AS source([id1], [id2], [id3], [test], [test1], [test2])
          ON [target].[id1]=[source].[id1] AND [target].[id2]=[source].[id2] AND [target].[id3]=[source].[id3]
        WHEN MATCHED THEN
          UPDATE
          SET [target].[test]=[source].[test], [target].[test2]=[source].[test2]
        WHEN NOT MATCHED THEN
          INSERT ([id1], [id2], [id3], [test], [test1], [test2])
          VALUES([source].[id1], [source].[id2], [source].[id3], [source].[test], [source].[test1], [source].[test2]);";
        $expected_bindings = [
            ':0_test' => 0,
            ':0_test1' => 1,
            ':0_test2' => 2,
            ':0_id1' => '+1',
            ':0_id2' => '+2',
            ':0_id3' => '+3',
        ];
        $keys = ['id1', 'id2', 'id3'];
        $data = ['id1' => '+1', 'id2' => '+2', 'id3' => '+3', 'test' => 0, 'test1' => 1, 'test2' => 2];
        $update = ['test', 'test2'];
        $query = $this->setupQueryMock();
        $query->from = 'tablename';
        $sql = $this->sut->compileCreateOrUpdate($query, $keys, $data, $update);
        $this->assertEquals($expected, $sql[0]);
        $this->assertEquals($expected_bindings, $sql[1]);
    }

    public function testCompileCreateOrUpdateMultipleKeysMultipleRows()
    {
        $expected = "MERGE [tablename] AS target
        USING (VALUES((:0_id1, :0_id2, :0_id3, :0_test, :0_test1, :0_test2), (:1_id1, :1_id2, :1_id3, :1_test, :1_test1, :1_test2), (:2_id1, :2_id2, :2_id3, :2_test, :2_test1, :2_test2)))
          AS source([id1], [id2], [id3], [test], [test1], [test2])
          ON [target].[id1]=[source].[id1] AND [target].[id2]=[source].[id2] AND [target].[id3]=[source].[id3]
        WHEN MATCHED THEN
          UPDATE
          SET [target].[test]=[source].[test], [target].[test1]=[source].[test1], [target].[test2]=[source].[test2]
        WHEN NOT MATCHED THEN
          INSERT ([id1], [id2], [id3], [test], [test1], [test2])
          VALUES([source].[id1], [source].[id2], [source].[id3], [source].[test], [source].[test1], [source].[test2]);";
        $expected_bindings = [
            ':0_test' => 0,
            ':0_test1' => 1,
            ':0_test2' => 2,
            ':0_id1' => '+1',
            ':0_id2' => '+2',
            ':0_id3' => '+3',
            ':1_id1' => '+1-1',
            ':1_id2' => '+2-1',
            ':1_id3' => '+3-1',
            ':1_test' => '0-1',
            ':1_test1' => '1-1',
            ':1_test2' => '2-1',
            ':2_id1' => '+1-2',
            ':2_id2' => '+2-2',
            ':2_id3' => '+3-2',
            ':2_test' => '0-2',
            ':2_test1' => '1-2',
            ':2_test2' => '2-2',
        ];
        $keys = ['id1', 'id2', 'id3'];
        $data = [
            ['id1' => '+1', 'id2' => '+2', 'id3' => '+3', 'test' => 0, 'test1' => 1, 'test2' => 2],
            ['id1' => '+1-1', 'id2' => '+2-1', 'id3' => '+3-1', 'test' => '0-1', 'test1' => '1-1', 'test2' => '2-1'],
            ['id1' => '+1-2', 'id2' => '+2-2', 'id3' => '+3-2', 'test' => '0-2', 'test1' => '1-2', 'test2' => '2-2'],
        ];
        $query = $this->setupQueryMock();
        $query->from = 'tablename';
        $sql = $this->sut->compileCreateOrUpdate($query, $keys, $data);
        $this->assertEquals($expected, $sql[0]);
        $this->assertEquals($expected_bindings, $sql[1]);
    }

    public function testCompileCreateOrUpdateMultipleRows()
    {
        $expected = "MERGE [tablename] AS target
        USING (VALUES((:0_test, :0_test1, :0_test2), (:1_test, :1_test1, :1_test2), (:2_test, :2_test1, :2_test2)))
          AS source([test], [test1], [test2])
          ON [target].[test]=[source].[test]
        WHEN MATCHED THEN
          UPDATE
          SET [target].[test1]=[source].[test1], [target].[test2]=[source].[test2]
        WHEN NOT MATCHED THEN
          INSERT ([test], [test1], [test2])
          VALUES([source].[test], [source].[test1], [source].[test2]);";
        $expected_bindings = [
            ':0_test' => 0,
            ':0_test1' => 1,
            ':0_test2' => 2,
            ':1_test' => '0-1',
            ':1_test1' => '1-1',
            ':1_test2' => '2-1',
            ':2_test' => '0-2',
            ':2_test1' => '1-2',
            ':2_test2' => '2-2',
        ];
        $keys = 'test';
        $data = [
            ['test' => 0, 'test1' => 1, 'test2' => 2],
            ['test' => '0-1', 'test1' => '1-1', 'test2' => '2-1'],
            ['test' => '0-2', 'test1' => '1-2', 'test2' => '2-2'],
        ];
        $query = $this->setupQueryMock();
        $query->from = 'tablename';
        $sql = $this->sut->compileCreateOrUpdate($query, $keys, $data);
        $this->assertEquals($expected, $sql[0]);
        $this->assertEquals($expected_bindings, $sql[1]);
    }

    public function testCreateBindParams()
    {
        $arr = [
            [
                ['test' => 1, 'test2' => 2, 'test3' => 3],
                'pre_',
            ],
            [
                [['test' => 1, 'test2' => 2, 'test3' => 3], ['test' => '1-1', 'test2' => '2-1', 'test3' => '3-1']],
            ],
        ];
        $expected = [
            ':0_pre_test' =>
                1,
            ':0_pre_test2' =>
                2,
            ':0_pre_test3' =>
                3,
            ':0_test' =>
                1,
            ':0_test2' =>
                2,
            ':0_test3' =>
                3,
            ':1_test' => '1-1',
            ':1_test2' => '2-1',
            ':1_test3' => '3-1',
        ];
        $bp = $this->sut->createBindParams($arr);
        $this->assertEquals($expected, $bp);
    }

    public function testTemp(): void
    {
        $expected = 'SELECT TOP 0 * INTO [table_temp] FROM [table]';
        $query = $this->setupQueryMock();
        [$sql, $params] = $this->sut->compileTemp($query, 'table', 'table_temp');
        $this->assertEquals($expected, $sql);
        $this->assertEmpty($params);
    }

    protected function setupQueryMock()
    {
        $query = m::mock('Smorken\Ext\Database\Query\Builder');
        $conn = m::mock('stdClass');
        $conn->shouldReceive('quote')
             ->andReturn('foo');
        $query->shouldReceive('getConnection')
              ->andReturn($conn);
        return $query;
    }
}
