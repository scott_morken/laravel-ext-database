<?php

namespace Smorken\Ext\Database\Tests\Unit\Query\Grammars;

/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/8/14
 * Time: 7:08 AM
 */

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\Query\Grammars\SQLiteGrammar;

class SqliteGrammarTest extends TestCase
{

    /**
     * @var SQLiteGrammar
     *
     */
    protected $sut;

    public function setUp(): void
    {
        $this->sut = new SQLiteGrammar();
    }

    public function tearDown(): void
    {
        m::close();
    }

    public function testCompileConcatenate()
    {
        $values = $this->sut->compileConcatenate(['foo', 'bar']);
        $this->assertEquals('foo || bar', $values);
    }

    public function testCompileConcatenateWithSeparator()
    {
        $values = $this->sut->compileConcatenate(['foo', 'bar'], ' ');
        $this->assertEquals('foo || " " || bar', $values);
    }

    public function testCompileCreateOrUpdate()
    {
        $expected = 'INSERT OR REPLACE INTO "tablename"
          ("test", "test1", "test2")
          VALUES (:0_test, :0_test1, :0_test2);';
        $expected_bindings = [
            ':0_test' => 0,
            ':0_test1' => 1,
            ':0_test2' => 2,
        ];
        $keys = 'test';
        $data = ['test' => 0, 'test1' => 1, 'test2' => 2];
        $query = $this->setupQueryMock();
        $query->from = 'tablename';
        $sql = $this->sut->compileCreateOrUpdate($query, $keys, $data);
        $this->assertEquals($expected, $sql[0]);
        $this->assertEquals($expected_bindings, $sql[1]);
    }

    public function testCompileCreateOrUpdateMultipleKeys()
    {
        $expected = "INSERT OR REPLACE INTO \"tablename\"
          (\"id1\", \"id2\", \"id3\", \"test\", \"test1\", \"test2\")
          VALUES (:0_id1, :0_id2, :0_id3, :0_test, :0_test1, :0_test2);";
        $expected_bindings = [
            ':0_test' => 0,
            ':0_test1' => 1,
            ':0_test2' => 2,
            ':0_id1' => '+1',
            ':0_id2' => '+2',
            ':0_id3' => '+3',
        ];
        $keys = ['id1', 'id2', 'id3'];
        $data = ['id1' => '+1', 'id2' => '+2', 'id3' => '+3', 'test' => 0, 'test1' => 1, 'test2' => 2];
        $query = $this->setupQueryMock();
        $query->from = 'tablename';
        $sql = $this->sut->compileCreateOrUpdate($query, $keys, $data);
        $this->assertEquals($expected, $sql[0]);
        $this->assertEquals($expected_bindings, $sql[1]);
    }

    public function testCompileCreateOrUpdateMultipleKeysMultipleRows()
    {
        $expected = "INSERT OR REPLACE INTO \"tablename\"
          (\"id1\", \"id2\", \"id3\", \"test\", \"test1\", \"test2\")
          VALUES (:0_id1, :0_id2, :0_id3, :0_test, :0_test1, :0_test2), (:1_id1, :1_id2, :1_id3, :1_test, :1_test1, :1_test2), (:2_id1, :2_id2, :2_id3, :2_test, :2_test1, :2_test2);";
        $expected_bindings = [
            ':0_test' => 0,
            ':0_test1' => 1,
            ':0_test2' => 2,
            ':0_id1' => '+1',
            ':0_id2' => '+2',
            ':0_id3' => '+3',
            ':1_id1' => '+1-1',
            ':1_id2' => '+2-1',
            ':1_id3' => '+3-1',
            ':1_test' => '0-1',
            ':1_test1' => '1-1',
            ':1_test2' => '2-1',
            ':2_id1' => '+1-2',
            ':2_id2' => '+2-2',
            ':2_id3' => '+3-2',
            ':2_test' => '0-2',
            ':2_test1' => '1-2',
            ':2_test2' => '2-2',
        ];
        $keys = ['id1', 'id2', 'id3'];
        $data = [
            ['id1' => '+1', 'id2' => '+2', 'id3' => '+3', 'test' => 0, 'test1' => 1, 'test2' => 2],
            ['id1' => '+1-1', 'id2' => '+2-1', 'id3' => '+3-1', 'test' => '0-1', 'test1' => '1-1', 'test2' => '2-1'],
            ['id1' => '+1-2', 'id2' => '+2-2', 'id3' => '+3-2', 'test' => '0-2', 'test1' => '1-2', 'test2' => '2-2'],
        ];
        $query = $this->setupQueryMock();
        $query->from = 'tablename';
        $sql = $this->sut->compileCreateOrUpdate($query, $keys, $data);
        $this->assertEquals($expected, $sql[0]);
        $this->assertEquals($expected_bindings, $sql[1]);
    }

    public function testCompileCreateOrUpdateMultipleRows()
    {
        $expected = 'INSERT OR REPLACE INTO "tablename"
          ("test", "test1", "test2")
          VALUES (:0_test, :0_test1, :0_test2), (:1_test, :1_test1, :1_test2), (:2_test, :2_test1, :2_test2);';
        $expected_bindings = [
            ':0_test' => 0,
            ':0_test1' => 1,
            ':0_test2' => 2,
            ':1_test' => '0-1',
            ':1_test1' => '1-1',
            ':1_test2' => '2-1',
            ':2_test' => '0-2',
            ':2_test1' => '1-2',
            ':2_test2' => '2-2',
        ];
        $keys = 'test';
        $data = [
            ['test' => 0, 'test1' => 1, 'test2' => 2],
            ['test' => '0-1', 'test1' => '1-1', 'test2' => '2-1'],
            ['test' => '0-2', 'test1' => '1-2', 'test2' => '2-2'],
        ];
        $query = $this->setupQueryMock();
        $query->from = 'tablename';
        $sql = $this->sut->compileCreateOrUpdate($query, $keys, $data);
        $this->assertEquals($expected, $sql[0]);
        $this->assertEquals($expected_bindings, $sql[1]);
    }

    public function testTemp(): void
    {
        $expected = 'CREATE TABLE "table_temp" AS SELECT * FROM "table" WHERE 0';
        $query = $this->setupQueryMock();
        [$sql, $params] = $this->sut->compileTemp($query, 'table', 'table_temp');
        $this->assertEquals($expected, $sql);
        $this->assertEmpty($params);
    }

    protected function setupQueryMock()
    {
        $query = m::mock('Smorken\Ext\Database\Query\Builder');
        $conn = m::mock('stdClass');
        $conn->shouldReceive('quote')
             ->andReturn('foo');
        $query->shouldReceive('getConnection')
              ->andReturn($conn);
        return $query;
    }
}
