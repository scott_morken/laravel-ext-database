<?php

namespace Smorken\Ext\Database\Tests\Unit;

/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 7:46 AM
 */

use Mockery as m;
use PDO;
use PDOException;
use PDOStatement;
use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\SqlServerConnection;

class SqlServerConnectionTest extends TestCase
{

    public function tearDown(): void
    {
        m::close();
    }

    public function testQueryBuilderInstanceOf()
    {
        $conn = $this->getMockConnection();
        $g = $conn->getQueryGrammar();
        $this->assertInstanceOf('Smorken\Ext\Database\Query\Grammars\SqlServerGrammar', $g);
    }

    public function testRetryWithOverride()
    {
        $pdo = m::mock(PDO::class);
        $statement = m::mock(PDOStatement::class);
        $statement->shouldReceive('execute')
                  ->once()
                  ->andThrow(
                      new PDOException(
                          'SQLSTATE[08S02]: [Microsoft][ODBC Driver 13 for SQL Server]SMux Provider: Physical connection is not usable [xFFFFFFFF]'
                      )
                  );
        $statement->shouldReceive('execute')
                  ->once()
                  ->andReturn(true);
        $pdo->shouldReceive('prepare')
            ->twice()
            ->andReturn($statement);
        $connection = new SqlServerConnection($pdo);
        $called = false;
        $connection->setReconnector(
            function ($connection) use (&$called) {
                $called = true;
            }
        );
        $this->assertTrue($connection->statement('foo'));
        $this->assertTrue($called);
    }

    protected function getMockConnection($methods = [], $pdo = null)
    {
        $pdo = $pdo ?: new PDOStub;
        $defaults = ['getDefaultPostProcessor', 'getDefaultSchemaGrammar'];
        $methods = implode(',', array_merge($defaults, $methods));
        return m::mock(sprintf('Smorken\Ext\Database\SqlServerConnection[%s]', $methods), [$pdo]);
    }
}
