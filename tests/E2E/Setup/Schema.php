<?php

namespace Smorken\Ext\Database\Tests\E2E\Setup;

use Illuminate\Database\Schema\Builder;

class Schema
{

    public static function __callStatic($method, $args)
    {
        $builder = self::getBuilder();
        return $builder->$method(...$args);
    }

    /**
     * Get a schema builder instance for a connection.
     *
     * @param  string  $name
     * @return Builder
     */
    public static function connection($name)
    {
        return self::getBuilder($name);
    }

    protected static function getBuilder($name = null): Builder
    {
        return Resolver::get()->connection($name)->getSchemaBuilder();
    }
}
