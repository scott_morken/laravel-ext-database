<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/24/16
 * Time: 2:04 PM
 */

namespace Smorken\Ext\Database\Tests\E2E\Setup;

use Illuminate\Database\Query\Expression;
use Smorken\Ext\Database\Eloquent\ExpressionValue;
use Smorken\Ext\Database\Eloquent\Model;

include_once __DIR__.'/TestTwo.php';

class TestOne extends Model
{

    protected $fillable = ['t1_join_1', 't1_join_2', 't1_join_3', 't1_join_4', 't1_extended'];

    public function testTwos()
    {
        return $this->compositeHasMany(
            TestTwo::class,
            ['t2_join_1', 't2_join_2', 't2_join_3', 't2_join_4'],
            ['t1_join_1', 't1_join_2', 't1_join_3', 't1_join_4']
        );
    }

    public function testTwosExpression()
    {
        $func = function ($start, $count) {
            return function ($ev, $model) use ($start, $count) {
                return substr($model->t2_join_1, $start, $count);
            };
        };
        $exp1 = new Expression('SUBSTR(t2_join_1, 1, 3)');
        $exp2 = new Expression('SUBSTR(t2_join_1, 4, 6)');
        $col1 = new ExpressionValue('t2_join_1', $exp1, $func(0, 3));
        $col2 = new ExpressionValue('t2_join_1', $exp2, $func(3, 3));
        return $this->compositeHasMany(
            TestTwo::class,
            [$col1, $col2],
            ['t1_join_1', 't1_join_2']
        );
    }
}
