<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/20/16
 * Time: 7:40 AM
 */

namespace Smorken\Ext\Database\Tests\E2E\Setup;

use Illuminate\Database\ConnectionResolver;
use PDO;
use Smorken\Ext\Database\SQLiteConnection;

class Resolver
{

    /**
     * @var ConnectionResolver
     */
    protected static $resolver;

    public static function get($dsn = 'sqlite::memory:', $username = null, $pw = null, $opts = [])
    {
        if (!self::$resolver) {
            $pdo = new PDO($dsn, $username, $pw, $opts);
            $connections = [
                'test' => new SQLiteConnection($pdo),
            ];
            $resolver = new ConnectionResolver($connections);
            $resolver->setDefaultConnection('test');
            self::$resolver = $resolver;
        }
        return self::$resolver;
    }

    public static function reset()
    {
        self::$resolver = null;
    }
}
