<?php

namespace Smorken\Ext\Database\Tests\E2E\Setup;

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/20/16
 * Time: 7:39 AM
 */


use Illuminate\Database\Schema\Blueprint;

class CreateTablesForTest
{

    protected string $connection = 'test';

    public function create()
    {
        Schema::create(
            'test_ones',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('t1_join_1', 4);
                $table->string('t1_join_2', 4);
                $table->string('t1_join_3', 4);
                $table->string('t1_join_4', 4);
                $table->string('t1_extended')->nullable();
                $table->timestamps();
            }
        );

        Schema::create(
            'test_twos',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('t2_join_1', 4);
                $table->string('t2_join_2', 4);
                $table->string('t2_join_3', 4);
                $table->string('t2_join_4', 4);
                $table->string('t2_extended')->nullable();
                $table->timestamps();
            }
        );
    }

    public function seed(TestOne $t1, TestTwo $t2)
    {
        for ($i = 1; $i < 10; $i++) {
            $t1->create(
                [
                    "t1_join_1" => "t1_$i",
                    "t1_join_2" => "t2_$i",
                    "t1_join_3" => "t3_$i",
                    "t1_join_4" => "t4_$i",
                ]
            );
            for ($j = 1; $j < 3; $j++) {
                $t2->create(
                    [
                        "t2_join_1" => "t1_$i",
                        "t2_join_2" => "t2_$i",
                        "t2_join_3" => "t3_$i",
                        "t2_join_4" => "t4_$i",
                    ]
                );
            }
        }
    }
}
