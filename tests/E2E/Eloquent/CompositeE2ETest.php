<?php

namespace Smorken\Ext\Database\Tests\E2E\Eloquent;

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/24/16
 * Time: 2:02 PM
 */

use Mockery as m;
use OutOfBoundsException;
use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\Tests\E2E\Setup\CreateTablesForTest;
use Smorken\Ext\Database\Tests\E2E\Setup\Resolver;
use Smorken\Ext\Database\Tests\E2E\Setup\TestOne;
use Smorken\Ext\Database\Tests\E2E\Setup\TestTwo;

class CompositeE2ETest extends TestCase
{

    protected $resolver;

    public function setUp(): void
    {
        parent::setUp();
        date_default_timezone_set('UTC');
        $this->setupDB();
    }

    public function tearDown(): void
    {
        m::close();
        $this->resolver = null;
        Resolver::reset();
    }

    public function testTestOneHasIsFalse()
    {
        $t = $this->getModel(TestOne::class);
        $t->create(
            [
                't1_join_1' => 99,
                't1_join_2' => 98,
                't1_join_3' => 97,
                't1_join_4' => 96,
                't1_extended' => 'foo',
            ]
        );
        $t1 = $t->has('testTwos')->where('t1_extended', 'foo')->first();
        $this->assertNull($t1);
    }

    public function testTestOneHasIsTrue()
    {
        $t = $this->getModel(TestOne::class);
        $t1 = $t->has('testTwos')->first();
        $sql = $t->has('testTwos')->toSql();
        $expected = 'select * from "test_ones" where exists (select * from "test_twos" where ("test_ones"."t1_join_1" = "test_twos"."t2_join_1" and "test_ones"."t1_join_2" = "test_twos"."t2_join_2" and "test_ones"."t1_join_3" = "test_twos"."t2_join_3" and "test_ones"."t1_join_4" = "test_twos"."t2_join_4"))';
        $this->assertEquals($expected, $sql);
        $this->assertEquals(1, $t1->id);
        $this->assertCount(2, $t1->testTwos);
    }

    public function testTestOneHasManyTestTwo()
    {
        $t = $this->getModel(TestOne::class);
        $t1 = $t->find(1);
        $sql = $t1->testTwos()->toSql();
        $expected = 'select * from "test_twos" where (("test_twos"."t2_join_1" = ? and "test_twos"."t2_join_2" = ? and "test_twos"."t2_join_3" = ? and "test_twos"."t2_join_4" = ?))';
        $t2s = $t1->testTwos;
        $this->assertEquals($expected, $sql);
        $this->assertCount(2, $t2s);
        foreach ($t2s as $t2) {
            $this->assertEquals($t1->t1_join_1, $t2->t2_join_1);
            $this->assertEquals($t1->t1_join_2, $t2->t2_join_2);
            $this->assertEquals($t1->t1_join_3, $t2->t2_join_3);
            $this->assertEquals($t1->t1_join_4, $t2->t2_join_4);
        }
    }

    public function testTestOneHasManyTestTwoEagerLoadMultipleResults()
    {
        $t = $this->getModel(TestOne::class);
        $t1s = $t->with('testTwos')->get();
        $this->assertCount(9, $t1s);
        foreach ($t1s as $t1) {
            $t2s = $t1->testTwos;
            $this->assertCount(2, $t2s);
            foreach ($t2s as $t2) {
                $this->assertEquals($t1->t1_join_1, $t2->t2_join_1);
                $this->assertEquals($t1->t1_join_2, $t2->t2_join_2);
                $this->assertEquals($t1->t1_join_3, $t2->t2_join_3);
                $this->assertEquals($t1->t1_join_4, $t2->t2_join_4);
            }
        }
    }

    public function testTestOneWhereHasIsTrue()
    {
        $t = $this->getModel(TestOne::class);
        $t2 = $this->getModel(TestTwo::class);
        $t->create(
            [
                't1_join_1' => 99,
                't1_join_2' => 98,
                't1_join_3' => 97,
                't1_join_4' => 96,
            ]
        );
        $t2->create(
            [
                't2_join_1' => 99,
                't2_join_2' => 98,
                't2_join_3' => 97,
                't2_join_4' => 96,
                't2_extended' => 'foo',
            ]
        );
        $q = $t->whereHas(
            'testTwos',
            function ($q) {
                $q->where('t2_extended', '=', 'foo');
            }
        );
        $sql = $q->toSql();
        $t1 = $q->first();
        $expected = 'select * from "test_ones" where exists (select * from "test_twos" where ("test_ones"."t1_join_1" = "test_twos"."t2_join_1" and "test_ones"."t1_join_2" = "test_twos"."t2_join_2" and "test_ones"."t1_join_3" = "test_twos"."t2_join_3" and "test_ones"."t1_join_4" = "test_twos"."t2_join_4") and "t2_extended" = ?)';
        $this->assertEquals($expected, $sql);
        $this->assertInstanceOf(TestOne::class, $t1);
        $this->assertCount(1, $t1->testTwos);
    }

    public function testTestOneWithExpressionKeys()
    {
        $t = $this->getModel(TestOne::class);
        $t2 = $this->getModel(TestTwo::class);
        $t->create(
            [
                't1_join_1' => 123,
                't1_join_2' => 456,
                't1_join_3' => 97,
                't1_join_4' => 96,
            ]
        );
        $t2->create(
            [
                't2_join_1' => 123456,
                't2_join_2' => 98,
                't2_join_3' => 97,
                't2_join_4' => 96,
                't2_extended' => 'foo',
            ]
        );
        $t1 = $t->where('t1_join_1', '123')->first();
        $sql = $t1->testTwosExpression()->toSql();
        $expected = 'select * from "test_twos" where ((SUBSTR(t2_join_1, 1, 3) = ? and SUBSTR(t2_join_1, 4, 6) = ?))';
        $t2s = $t1->testTwosExpression;
        $this->assertEquals($expected, $sql);
        foreach ($t2s as $t2) {
            $this->assertEquals($t2->t2_join_1, $t1->t1_join_1.$t1->t1_join_2);
        }
    }

    public function testTestTwoBelongsToReversedKeys()
    {
        $t = $this->getModel(TestTwo::class);
        $t2 = $t->find(1);
        $this->expectException(OutOfBoundsException::class);
        $this->expectExceptionMessage('No relation appears to have been loaded, check your keys.');
        $t1 = $t2->testOneReversed;
    }

    public function testTestTwoBelongsToTestOne()
    {
        $t = $this->getModel(TestTwo::class);
        $t2 = $t->find(1);
        $sql = $t2->testOne()->toSql();
        $expected = 'select * from "test_ones" where (("test_ones"."t1_join_1" = ? and "test_ones"."t1_join_2" = ? and "test_ones"."t1_join_3" = ? and "test_ones"."t1_join_4" = ?))';
        $t1 = $t2->testOne;
        $this->assertEquals($expected, $sql);
        $this->assertEquals(1, $t2->id);
        $this->assertEquals('t1_1', $t2->t2_join_1);
        $this->assertEquals('t2_1', $t2->t2_join_2);
        $this->assertEquals('t3_1', $t2->t2_join_3);
        $this->assertEquals('t4_1', $t2->t2_join_4);
        $this->assertInstanceOf(TestOne::class, $t1);
        $this->assertEquals(1, $t1->id);
        $this->assertEquals('t1_1', $t1->t1_join_1);
        $this->assertEquals('t2_1', $t1->t1_join_2);
        $this->assertEquals('t3_1', $t1->t1_join_3);
        $this->assertEquals('t4_1', $t1->t1_join_4);
    }

    public function testTestTwoHasIsFalse()
    {
        $t = $this->getModel(TestTwo::class);
        $t->create(
            [
                't2_join_1' => 99,
                't2_join_2' => 98,
                't2_join_3' => 97,
                't2_join_4' => 96,
                't2_extended' => 'foo',
            ]
        );
        $expected = 'select * from "test_twos" where exists '.
            '(select * from "test_ones" where '.
            '("test_twos"."t2_join_1" = "test_ones"."t1_join_1" and '.
            '"test_twos"."t2_join_2" = "test_ones"."t1_join_2" and '.
            '"test_twos"."t2_join_3" = "test_ones"."t1_join_3" and '.
            '"test_twos"."t2_join_4" = "test_ones"."t1_join_4")) and "t2_extended" = ?';
        $sql = $t->has('testOne')->where('t2_extended', 'foo')->toSql();
        $this->assertEquals($expected, $sql);
        $t2 = $t->has('testOne')->where('t2_extended', 'foo')->first();
        $this->assertNull($t2);
    }

    public function testTestTwoWithExpressionKeys()
    {
        $t = $this->getModel(TestOne::class);
        $t2 = $this->getModel(TestTwo::class);
        $t->create(
            [
                't1_join_1' => 123,
                't1_join_2' => 456,
                't1_join_3' => 97,
                't1_join_4' => 96,
            ]
        );
        $t2->create(
            [
                't2_join_1' => 123456,
                't2_join_2' => 98,
                't2_join_3' => 97,
                't2_join_4' => 96,
                't2_extended' => 'foo',
            ]
        );
        $t2m = $t2->where('t2_join_1', '123456')->first();
        $sql = $t2m->testOneExpression()->toSql();
        $expected = 'select * from "test_ones" where (("test_ones"."t1_join_1" = ? and "test_ones"."t1_join_2" = ?))';
        $t1 = $t2m->testOneExpression;
        $this->assertEquals($expected, $sql);
        $this->assertEquals(123, $t1->t1_join_1);
        $this->assertEquals(456, $t1->t1_join_2);
    }

    protected function getConnectionResolver()
    {
        if (!$this->resolver) {
            $this->resolver = Resolver::get();
        }
        return $this->resolver;
    }

    protected function getModel($model_class, $attrs = [])
    {
        $model = new $model_class($attrs);
        forward_static_call([$model_class, 'setConnectionResolver'], $this->getConnectionResolver());
        return $model;
    }

    protected function setupDB()
    {
        $m = new CreateTablesForTest();
        $m->create();
        $t1 = $this->getModel(TestOne::class);
        $t2 = $this->getModel(TestTwo::class);
        $m->seed($t1, $t2);
    }
}
