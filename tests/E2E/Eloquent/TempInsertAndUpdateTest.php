<?php

namespace Smorken\Ext\Database\Tests\E2E\Eloquent;

use Illuminate\Database\ConnectionResolverInterface;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\Tests\E2E\Setup\CreateTablesForTest;
use Smorken\Ext\Database\Tests\E2E\Setup\Resolver;
use Smorken\Ext\Database\Tests\E2E\Setup\TestOne;
use Smorken\Ext\Database\Tests\E2E\Setup\TestTwo;

class TempInsertAndUpdateTest extends TestCase
{

    protected $resolver;

    public function setUp(): void
    {
        parent::setUp();
        date_default_timezone_set('UTC');
        $this->setupDB();
    }

    public function tearDown(): void
    {
        m::close();
        $this->resolver = null;
        Resolver::reset();
    }

    public function testInsertOnly(): void
    {
        $t = $this->getModel(TestOne::class);
        $temp = $t->temp();
        $data = [
            [
                't1_join_1' => 'tj1-1',
                't1_join_2' => 'tj2-1',
                't1_join_3' => 'tj3-1',
                't1_join_4' => 'tj4-1',
                't1_extended' => 'foo',
            ],
            [
                't1_join_1' => 'tj1-2',
                't1_join_2' => 'tj2-2',
                't1_join_3' => 'tj3-2',
                't1_join_4' => 'tj4-2',
                't1_extended' => 'foo bar',
            ],
        ];
        $t->getConnection()->table($temp)->insert($data);
        $updates = $t->newQuery()->updateFromComplex(
            $temp,
            ['t1_join_1', 't1_join_2'],
            ['t1_join_3', 't1_join_4', 't1_extended']
        );
        $inserts = $t->newQuery()->insertFrom(
            $temp,
            ['t1_join_1', 't1_join_2'],
            ['t1_join_1', 't1_join_2', 't1_join_3', 't1_join_4', 't1_extended']
        );
        $this->assertEquals(0, $updates);
        $this->assertEquals(2, $inserts);
        $all = $t->newQuery()->get();
        $this->assertCount(11, $all); // 9 seeded
        $t1Coll = $t->newQuery()->where('t1_join_1', '=', 'tj1-1')->get();
        $this->assertCount(1, $t1Coll);
        $this->assertDataMatchesModel($data[0], $t1Coll->first());
        $t2Coll = $t->newQuery()->where('t1_join_1', '=', 'tj1-2')->get();
        $this->assertCount(1, $t2Coll);
        $this->assertDataMatchesModel($data[1], $t2Coll->first());
        $t->dropTemp();
    }

    public function testTemp(): void
    {
        $t = $this->getModel(TestOne::class);
        $temp = $t->temp();
        $this->assertTrue($t->getConnection()->getSchemaBuilder()->hasTable($temp));
        $attrs = $t->getConnection()->getSchemaBuilder()->getColumnListing($temp);
        $expected = [
            'id',
            't1_join_1',
            't1_join_2',
            't1_join_3',
            't1_join_4',
            't1_extended',
            'created_at',
            'updated_at',
        ];
        $this->assertEquals($expected, $attrs);
        $t->dropTemp();
        $this->assertFalse($t->getConnection()->getSchemaBuilder()->hasTable($temp));
    }

    public function testUpdateAndInsert(): void
    {
        $t = $this->getModel(TestOne::class);
        $temp = $t->temp();
        $this->assertTrue($t->getConnection()->getSchemaBuilder()->hasTable($temp));
        $attrs = $t->getConnection()->getSchemaBuilder()->getColumnListing($temp);
        $expected = [
            'id',
            't1_join_1',
            't1_join_2',
            't1_join_3',
            't1_join_4',
            't1_extended',
            'created_at',
            'updated_at',
        ];
        $this->assertEquals($expected, $attrs);
        $data = [
            [
                't1_join_1' => 't1_9',
                't1_join_2' => 't2_9',
                't1_join_3' => 'tj3-9',
                't1_join_4' => 'tj4-9',
                't1_extended' => 'foo 9',
            ],
            [
                't1_join_1' => 'tj1-1',
                't1_join_2' => 'tj2-1',
                't1_join_3' => 'tj3-1',
                't1_join_4' => 'tj4-1',
                't1_extended' => 'foo',
            ],
            [
                't1_join_1' => 'tj1-2',
                't1_join_2' => 'tj2-2',
                't1_join_3' => 'tj3-2',
                't1_join_4' => 'tj4-2',
                't1_extended' => 'foo bar',
            ],
        ];
        $t->getConnection()->table($temp)->insert($data);
        $updates = $t->newQuery()->updateFromComplex(
            $temp,
            ['t1_join_1', 't1_join_2'],
            ['t1_join_3', 't1_join_4', 't1_extended']
        );
        $inserts = $t->newQuery()->insertFrom(
            $temp,
            ['t1_join_1', 't1_join_2'],
            ['t1_join_1', 't1_join_2', 't1_join_3', 't1_join_4', 't1_extended']
        );
        $this->assertEquals(1, $updates);
        $this->assertEquals(2, $inserts);
        $all = $t->newQuery()->get();
        $this->assertCount(11, $all); // 9 seeded
        $t9Coll = $t->newQuery()->where('t1_join_1', '=', 't1_9')->get();
        $this->assertCount(1, $t9Coll);
        $this->assertDataMatchesModel($data[0], $t9Coll->first());
        $t1Coll = $t->newQuery()->where('t1_join_1', '=', 'tj1-1')->get();
        $this->assertCount(1, $t1Coll);
        $this->assertDataMatchesModel($data[1], $t1Coll->first());
        $t2Coll = $t->newQuery()->where('t1_join_1', '=', 'tj1-2')->get();
        $this->assertCount(1, $t2Coll);
        $this->assertDataMatchesModel($data[2], $t2Coll->first());
        $t->dropTemp();
    }

    public function testUpdateOnly(): void
    {
        $t = $this->getModel(TestOne::class);
        $temp = $t->temp();
        $this->assertTrue($t->getConnection()->getSchemaBuilder()->hasTable($temp));
        $attrs = $t->getConnection()->getSchemaBuilder()->getColumnListing($temp);
        $expected = [
            'id',
            't1_join_1',
            't1_join_2',
            't1_join_3',
            't1_join_4',
            't1_extended',
            'created_at',
            'updated_at',
        ];
        $this->assertEquals($expected, $attrs);
        $data = [
            [
                't1_join_1' => 't1_9',
                't1_join_2' => 't2_9',
                't1_join_3' => 'tj3-9',
                't1_join_4' => 'tj4-9',
                't1_extended' => 'foo 9',
            ],
        ];
        $t->getConnection()->table($temp)->insert($data);
        $updates = $t->newQuery()->updateFromComplex(
            $temp,
            ['t1_join_1', 't1_join_2'],
            ['t1_join_3', 't1_join_4', 't1_extended']
        );
        $inserts = $t->newQuery()->insertFrom(
            $temp,
            ['t1_join_1', 't1_join_2'],
            ['t1_join_1', 't1_join_2', 't1_join_3', 't1_join_4', 't1_extended']
        );
        $this->assertEquals(1, $updates);
        $this->assertEquals(0, $inserts);
        $all = $t->newQuery()->get();
        $this->assertCount(9, $all); // 9 seeded
        $t1Coll = $t->newQuery()->where('t1_join_1', '=', 't1_9')->get();
        $this->assertCount(1, $t1Coll);
        $this->assertDataMatchesModel($data[0], $t1Coll->first());
        $t->dropTemp();
    }

    protected function assertDataMatchesModel(array $data, $model): void
    {
        foreach ($data as $key => $value) {
            $this->assertEquals($value, $model->$key, 'Key: '.$key);
        }
    }

    protected function getConnectionResolver(): ConnectionResolverInterface
    {
        if (!$this->resolver) {
            $this->resolver = Resolver::get();
        }
        return $this->resolver;
    }

    protected function getModel($model_class, $attrs = [])
    {
        $model = new $model_class($attrs);
        forward_static_call([$model_class, 'setConnectionResolver'], $this->getConnectionResolver());
        return $model;
    }

    protected function setupDB(): void
    {
        $m = new CreateTablesForTest();
        $m->create();
        $t1 = $this->getModel(TestOne::class);
        $t2 = $this->getModel(TestTwo::class);
        $m->seed($t1, $t2);
    }
}
