<?php

namespace Smorken\Ext\Database\Tests\Integration\Stubs;

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/12/17
 * Time: 7:23 AM
 */

use Smorken\Ext\Database\Eloquent\Model;

/**
 * Class RelatedModel
 * @package integration\stubs
 *
 * @property int $id
 * @property int $base_model_id
 * @property int $key_1_related
 * @property int $key_2_related
 * @property string $related_descr
 */
class RelatedModel extends Model
{

    protected $fillable = ['id', 'base_model_id', 'key_1_related', 'key_2_related', 'related_descr'];

    public function baseModel()
    {
        return $this->belongsTo(BaseModel::class);
    }

    public function compositeBelongsToRelation()
    {
        return $this->compositeBelongsTo(
            BaseModel::class,
            ['key_1_related', 'key_2_related'],
            ['key_1_base', 'key_2_base']
        );
    }
}
