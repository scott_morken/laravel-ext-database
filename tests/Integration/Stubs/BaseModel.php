<?php

namespace Smorken\Ext\Database\Tests\Integration\Stubs;

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/12/17
 * Time: 7:22 AM
 */

use Smorken\Ext\Database\Eloquent\Model;

/**
 * Class BaseModel
 * @package integration\stubs
 *
 * @property int $id
 * @property int $key_1_base
 * @property int $key_2_base
 * @property string $base_descr
 * @property string $other
 *
 */
class BaseModel extends Model
{

    protected $fillable = ['id', 'key_1_base', 'key_2_base', 'base_descr', 'other'];

    public function compositeHasManyRelations()
    {
        return $this->compositeHasMany(
            RelatedModel::class,
            ['key_1_related', 'key_2_related'],
            ['key_1_base', 'key_2_base']
        );
    }

    public function simpleRelations()
    {
        return $this->hasMany(RelatedModel::class);
    }
}
