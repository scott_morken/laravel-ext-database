<?php

namespace Smorken\Ext\Database\Tests\Integration;

use Illuminate\Database\Query\Processors\SQLiteProcessor;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\Query\Grammars\SQLiteGrammar;
use Smorken\Ext\Database\SQLiteConnection;
use Smorken\Ext\Database\Tests\Integration\Stubs\BaseModel;
use Smorken\Ext\Database\Tests\Unit\PDOStub;

class SqliteTest extends TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testUpdateFromComplex(): void
    {
        [$sut, $conn, $grammar, $process, $pdo] = $this->getModel(BaseModel::class);
        $expected_sql = 'update "base_models" set "base_descr" = (select "base_models_temp"."base_descr" from "base_models_temp" where "base_models_temp"."key_1_base" = "base_models"."key_1_base" and "base_models_temp"."key_2_base" = "base_models"."key_2_base"), "other" = (select "base_models_temp"."other" from "base_models_temp" where "base_models_temp"."key_1_base" = "base_models"."key_1_base" and "base_models_temp"."key_2_base" = "base_models"."key_2_base") where exists (select "base_models_temp"."base_descr", "base_models_temp"."other" from "base_models_temp" where "base_models_temp"."key_1_base" = "base_models"."key_1_base" and "base_models_temp"."key_2_base" = "base_models"."key_2_base")';
        $stmt = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with($expected_sql)->andReturn($stmt);
        $stmt->shouldReceive('execute')->once();
        $stmt->shouldReceive('rowCount')->once()->andReturn(1);
        $this->assertEquals(
            1,
            $sut->newQuery()->updateFromComplex(
                'base_models_temp',
                ['key_1_base', 'key_2_base'],
                ['base_descr', 'other']
            )
        );
    }

    public function testUpdateFromComplexKeyed(): void
    {
        [$sut, $conn, $grammar, $process, $pdo] = $this->getModel(BaseModel::class);
        $expected_sql = 'update "base_models" set "base_descr" = (select "base_models_temp"."other_descr" from "base_models_temp" where "base_models_temp"."other1" = "base_models"."key_1_base" and "base_models_temp"."key_2_base" = "base_models"."key_2_base"), "other" = (select "base_models_temp"."other" from "base_models_temp" where "base_models_temp"."other1" = "base_models"."key_1_base" and "base_models_temp"."key_2_base" = "base_models"."key_2_base") where exists (select "base_models_temp"."other_descr", "base_models_temp"."other" from "base_models_temp" where "base_models_temp"."other1" = "base_models"."key_1_base" and "base_models_temp"."key_2_base" = "base_models"."key_2_base")';
        $stmt = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with($expected_sql)->andReturn($stmt);
        $stmt->shouldReceive('execute')->once();
        $stmt->shouldReceive('rowCount')->once()->andReturn(1);
        $this->assertEquals(
            1,
            $sut->newQuery()->updateFromComplex(
                'base_models_temp',
                ['other1' => 'key_1_base', 'key_2_base'],
                ['other_descr' => 'base_descr', 'other']
            )
        );
    }

    protected function getConnectionMocks()
    {
        $pdo = m::mock(PDOStub::class);
        $grammar = new SQLiteGrammar();
        $processor = new SQLiteProcessor();
        $connection = m::mock(new SQLiteConnection($pdo));
        $conn = m::mock('Illuminate\Database\ConnectionResolverInterface');
        $conn->shouldReceive('getQueryGrammar')->andReturn($grammar);
        $conn->shouldReceive('getPostProcessor')->andReturn($processor);
        $conn->shouldReceive('connection')->andReturn($connection);
        return [$conn, $grammar, $processor, $pdo];
    }

    protected function getModel($cls)
    {
        [$conn, $grammar, $process, $pdo] = $this->getConnectionMocks();
        $cls::setConnectionResolver($conn);
        return [new $cls, $conn, $grammar, $process, $pdo];
    }
}
