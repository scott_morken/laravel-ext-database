<?php

namespace Smorken\Ext\Database\Tests\Integration;

use Illuminate\Database\Query\Processors\MySqlProcessor;
use Illuminate\Support\Facades\DB;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\MySqlConnection;
use Smorken\Ext\Database\Query\Grammars\MySqlGrammar;
use Smorken\Ext\Database\Tests\Integration\Stubs\BaseModel;
use Smorken\Ext\Database\Tests\Unit\PDOStub;

class MysqlSqlTest extends TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testCreateOrUpdate()
    {
        $data = [
            [
                'key_1_base' => 'foo',
                'key_2_base' => 'bar',
                'base_descr' => 'foo-bar',
            ],
            [
                'key_1_base' => 'fiz',
                'key_2_base' => 'buz',
                'base_descr' => 'fiz-buz',
            ],
        ];
        [$sut, $conn, $grammar, $process, $pdo] = $this->getModel(BaseModel::class);
        $stmt = m::mock(\PDOStatement::class);
        $expected_sql = 'INSERT INTO `base_models`
          (`key_1_base`, `key_2_base`, `base_descr`, `updated_at`, `created_at`)
          VALUES (:0_key_1_base, :0_key_2_base, :0_base_descr, :0_updated_at, :0_created_at), (:1_key_1_base, :1_key_2_base, :1_base_descr, :1_updated_at, :1_created_at)
        ON DUPLICATE KEY UPDATE
          `base_descr`=VALUES(`base_descr`), `updated_at`=VALUES(`updated_at`);';
        $pdo->shouldReceive('prepare')->once()->with($expected_sql)->andReturn($stmt);
        $stmt->shouldReceive('bindValue')->with(':0_key_1_base', 'foo', 2);
        $stmt->shouldReceive('bindValue')->with(':0_key_2_base', 'bar', 2);
        $stmt->shouldReceive('bindValue')->with(':0_base_descr', 'foo-bar', 2);
        $stmt->shouldReceive('bindValue')->with(':0_updated_at', m::any(), 2);
        $stmt->shouldReceive('bindValue')->with(':0_created_at', m::any(), 2);
        $stmt->shouldReceive('bindValue')->with(':1_key_1_base', 'fiz', 2);
        $stmt->shouldReceive('bindValue')->with(':1_key_2_base', 'buz', 2);
        $stmt->shouldReceive('bindValue')->with(':1_base_descr', 'fiz-buz', 2);
        $stmt->shouldReceive('bindValue')->with(':1_updated_at', m::any(), 2);
        $stmt->shouldReceive('bindValue')->with(':1_created_at', m::any(), 2);
        $stmt->shouldReceive('execute')->once();
        $stmt->shouldReceive('rowCount')->once()->andReturn(2);
        $this->assertEquals(2, $sut->createOrUpdate(['key_1_base', 'key_2_base'], $data, ['base_descr']));
    }

    public function testInsertFrom(): void
    {
        [$sut, $conn, $grammar, $process, $pdo] = $this->getModel(BaseModel::class);
        $stmt = m::mock(\PDOStatement::class);
        $expected_sql = 'insert into `base_models` (`key_1_base`, `key_2_base`, `base_descr`, `other`) select `base_models_temp`.`key_1_base`, `base_models_temp`.`key_2_base`, `base_models_temp`.`base_descr`, `base_models_temp`.`other` from `base_models_temp` left join `base_models` on `base_models`.`key_1_base` = `base_models_temp`.`key_1_base` and `base_models`.`key_2_base` = `base_models_temp`.`key_2_base` where `base_models`.`key_1_base` is null and `base_models`.`key_2_base` is null';
        $pdo->shouldReceive('prepare')->once()->with($expected_sql)->andReturn($stmt);
        $stmt->shouldReceive('execute')->once();
        $stmt->shouldReceive('rowCount')->once()->andReturn(1);
        $this->assertEquals(
            1,
            $sut->newQuery()->insertFrom(
                'base_models_temp',
                ['key_1_base', 'key_2_base'],
                ['key_1_base', 'key_2_base', 'base_descr', 'other']
            )
        );
    }

    public function testInsertFromKeyed(): void
    {
        [$sut, $conn, $grammar, $process, $pdo] = $this->getModel(BaseModel::class);
        $stmt = m::mock(\PDOStatement::class);
        $expected_sql = 'insert into `base_models` (`key_1_base`, `key_2_base`, `base_descr`, `other`) select `others`.`others_1`, `others`.`key_2_base`, `others`.`other_descr`, `others`.`other` from `others` left join `base_models` on `base_models`.`key_1_base` = `others`.`others_1` and `base_models`.`key_2_base` = `others`.`key_2_base` where `base_models`.`key_1_base` is null and `base_models`.`key_2_base` is null';
        $pdo->shouldReceive('prepare')->once()->with($expected_sql)->andReturn($stmt);
        $stmt->shouldReceive('execute')->once();
        $stmt->shouldReceive('rowCount')->once()->andReturn(1);
        $this->assertEquals(
            1,
            $sut->newQuery()->insertFrom(
                'others',
                ['others_1' => 'key_1_base', 'key_2_base'],
                ['others_1' => 'key_1_base', 'key_2_base', 'other_descr' => 'base_descr', 'other']
            )
        );
    }

    public function testInsertOrUpdateViaTemp(): void
    {
        [$sut, $conn, $grammar, $process, $pdo] = $this->getModel(BaseModel::class);
        $stmt = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with(
            'select * from information_schema.tables where table_schema = ? and table_name = ? and table_type = \'BASE TABLE\''
        )->andReturn($stmt);
        $stmt->shouldReceive('setFetchMode')->once();
        $stmt->shouldReceive('bindValue')->once()->with(1, '', 2);
        $stmt->shouldReceive('bindValue')->once()->with(2, 'base_models_temp', 2);
        $stmt->shouldReceive('execute')->once();
        $stmt->shouldReceive('fetchAll')->once()->andReturn([]);
        $stmt2 = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with('insert into `base_models_temp` (`base_descr`, `key_1_base`, `key_2_base`, `other`) values (?, ?, ?, ?), (?, ?, ?, ?)')->andReturn($stmt2);
        $stmt2->shouldReceive('bindValue')->once()->with(1, '1 descr', 2);
        $stmt2->shouldReceive('bindValue')->once()->with(2, 'k1-1', 2);
        $stmt2->shouldReceive('bindValue')->once()->with(3, 'k2-1', 2);
        $stmt2->shouldReceive('bindValue')->once()->with(4, '1 other', 2);
        $stmt2->shouldReceive('bindValue')->once()->with(5, '2 descr', 2);
        $stmt2->shouldReceive('bindValue')->once()->with(6, 'k1-2', 2);
        $stmt2->shouldReceive('bindValue')->once()->with(7, 'k2-2', 2);
        $stmt2->shouldReceive('bindValue')->once()->with(8, '2 other', 2);
        $stmt2->shouldReceive('execute')->once();
        $expected_sql = 'CREATE TABLE `base_models_temp` (LIKE `base_models`)';
        $stmt3 = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with($expected_sql)->andReturn($stmt3);
        $stmt3->shouldReceive('execute')->once();
        $stmt3->shouldReceive('rowCount')->once()->andReturn(1);
        $expected_sql = 'update `base_models` inner join `base_models_temp` on `base_models_temp`.`key_1_base` = `base_models`.`key_1_base` and `base_models_temp`.`key_2_base` = `base_models`.`key_2_base` set `base_models`.`base_descr` = `base_models_temp`.`base_descr`, `base_models`.`other` = `base_models_temp`.`other`';
        $stmt4 = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with($expected_sql)->andReturn($stmt4);
        $stmt4->shouldReceive('execute')->once();
        $stmt4->shouldReceive('rowCount')->once()->andReturn(2);
        $expected_sql = 'insert into `base_models` (`key_1_base`, `key_2_base`, `base_descr`, `other`) select `base_models_temp`.`key_1_base`, `base_models_temp`.`key_2_base`, `base_models_temp`.`base_descr`, `base_models_temp`.`other` from `base_models_temp` left join `base_models` on `base_models`.`key_1_base` = `base_models_temp`.`key_1_base` and `base_models`.`key_2_base` = `base_models_temp`.`key_2_base` where `base_models`.`key_1_base` is null and `base_models`.`key_2_base` is null';
        $stmt5 = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with($expected_sql)->andReturn($stmt5);
        $stmt5->shouldReceive('execute')->once();
        $stmt5->shouldReceive('rowCount')->once()->andReturn(1);
        $this->assertEquals(
            [1, 2],
            $sut->insertOrUpdateViaTemp(
                [
                    ['key_1_base' => 'k1-1', 'key_2_base' => 'k2-1', 'base_descr' => '1 descr', 'other' => '1 other'],
                    ['key_1_base' => 'k1-2', 'key_2_base' => 'k2-2', 'base_descr' => '2 descr', 'other' => '2 other'],
                ],
                ['key_1_base', 'key_2_base'],
                ['key_1_base', 'key_2_base', 'base_descr', 'other'],
                ['base_descr', 'other']
            )
        );
    }

    public function testInsertOrUpdateViaTempExisting(): void
    {
        [$sut, $conn, $grammar, $process, $pdo] = $this->getModel(BaseModel::class);
        $stmt = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with(
            'select * from information_schema.tables where table_schema = ? and table_name = ? and table_type = \'BASE TABLE\''
        )->andReturn($stmt);
        $stmt->shouldReceive('setFetchMode')->once();
        $stmt->shouldReceive('bindValue')->once()->with(1, '', 2);
        $stmt->shouldReceive('bindValue')->once()->with(2, 'base_models_temp', 2);
        $stmt->shouldReceive('execute')->once();
        $stmt->shouldReceive('fetchAll')->once()->andReturn([['table_name' => 'base_models_temp']]);
        $stmt2 = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with('truncate table `base_models_temp`')->andReturn($stmt2);
        $stmt2->shouldReceive('execute')->once();
        $expected_sql = 'CREATE TABLE `base_models_temp` (LIKE `base_models`)';
        $pdo->shouldReceive('prepare')->never()->with($expected_sql);
        $stmt3 = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with('insert into `base_models_temp` (`base_descr`, `key_1_base`, `key_2_base`, `other`) values (?, ?, ?, ?), (?, ?, ?, ?)')->andReturn($stmt3);
        $stmt3->shouldReceive('bindValue')->once()->with(1, '1 descr', 2);
        $stmt3->shouldReceive('bindValue')->once()->with(2, 'k1-1', 2);
        $stmt3->shouldReceive('bindValue')->once()->with(3, 'k2-1', 2);
        $stmt3->shouldReceive('bindValue')->once()->with(4, '1 other', 2);
        $stmt3->shouldReceive('bindValue')->once()->with(5, '2 descr', 2);
        $stmt3->shouldReceive('bindValue')->once()->with(6, 'k1-2', 2);
        $stmt3->shouldReceive('bindValue')->once()->with(7, 'k2-2', 2);
        $stmt3->shouldReceive('bindValue')->once()->with(8, '2 other', 2);
        $stmt3->shouldReceive('execute')->once();
        $expected_sql = 'update `base_models` inner join `base_models_temp` on `base_models_temp`.`key_1_base` = `base_models`.`key_1_base` and `base_models_temp`.`key_2_base` = `base_models`.`key_2_base` set `base_models`.`base_descr` = `base_models_temp`.`base_descr`, `base_models`.`other` = `base_models_temp`.`other`';
        $stmt4 = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with($expected_sql)->andReturn($stmt4);
        $stmt4->shouldReceive('execute')->once();
        $stmt4->shouldReceive('rowCount')->once()->andReturn(2);
        $expected_sql = 'insert into `base_models` (`key_1_base`, `key_2_base`, `base_descr`, `other`) select `base_models_temp`.`key_1_base`, `base_models_temp`.`key_2_base`, `base_models_temp`.`base_descr`, `base_models_temp`.`other` from `base_models_temp` left join `base_models` on `base_models`.`key_1_base` = `base_models_temp`.`key_1_base` and `base_models`.`key_2_base` = `base_models_temp`.`key_2_base` where `base_models`.`key_1_base` is null and `base_models`.`key_2_base` is null';
        $stmt5 = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with($expected_sql)->andReturn($stmt5);
        $stmt5->shouldReceive('execute')->once();
        $stmt5->shouldReceive('rowCount')->once()->andReturn(1);
        $this->assertEquals(
            [1, 2],
            $sut->insertOrUpdateViaTemp(
                [
                    ['key_1_base' => 'k1-1', 'key_2_base' => 'k2-1', 'base_descr' => '1 descr', 'other' => '1 other'],
                    ['key_1_base' => 'k1-2', 'key_2_base' => 'k2-2', 'base_descr' => '2 descr', 'other' => '2 other'],
                ],
                ['key_1_base', 'key_2_base'],
                ['key_1_base', 'key_2_base', 'base_descr', 'other'],
                ['base_descr', 'other']
            )
        );
    }

    public function testInsertOrUpdateViaTempKeyed(): void
    {
        [$sut, $conn, $grammar, $process, $pdo] = $this->getModel(BaseModel::class);
        $stmt = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with(
            'select * from information_schema.tables where table_schema = ? and table_name = ? and table_type = \'BASE TABLE\''
        )->andReturn($stmt);
        $stmt->shouldReceive('setFetchMode')->once();
        $stmt->shouldReceive('bindValue')->once()->with(1, '', 2);
        $stmt->shouldReceive('bindValue')->once()->with(2, 'base_models_temp', 2);
        $stmt->shouldReceive('execute')->once();
        $stmt->shouldReceive('fetchAll')->once()->andReturn([]);
        $expected_sql = 'CREATE TABLE `base_models_temp` (LIKE `base_models`)';
        $stmt2 = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with($expected_sql)->andReturn($stmt2);
        $stmt2->shouldReceive('execute')->once();
        $stmt2->shouldReceive('rowCount')->once()->andReturn(1);
        $stmt3 = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with('insert into `base_models_temp` (`key_2_base`, `other`, `other_descr`, `others_1`) values (?, ?, ?, ?), (?, ?, ?, ?)')->andReturn($stmt3);
        $stmt3->shouldReceive('bindValue')->once()->with(1, 'k2-1', 2);
        $stmt3->shouldReceive('bindValue')->once()->with(2, '1 other', 2);
        $stmt3->shouldReceive('bindValue')->once()->with(3, '1 descr', 2);
        $stmt3->shouldReceive('bindValue')->once()->with(4, 'k1-1', 2);
        $stmt3->shouldReceive('bindValue')->once()->with(5, 'k2-2', 2);
        $stmt3->shouldReceive('bindValue')->once()->with(6, '2 other', 2);
        $stmt3->shouldReceive('bindValue')->once()->with(7, '2 descr', 2);
        $stmt3->shouldReceive('bindValue')->once()->with(8, 'k1-2', 2);
        $stmt3->shouldReceive('execute')->once();
        $expected_sql = 'update `base_models` inner join `base_models_temp` on `base_models_temp`.`others_1` = `base_models`.`key_1_base` and `base_models_temp`.`key_2_base` = `base_models`.`key_2_base` set `base_models`.`base_descr` = `base_models_temp`.`other_descr`, `base_models`.`other` = `base_models_temp`.`other`';
        $stmt4 = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with($expected_sql)->andReturn($stmt4);
        $stmt4->shouldReceive('execute')->once();
        $stmt4->shouldReceive('rowCount')->once()->andReturn(2);
        $expected_sql = 'insert into `base_models` (`key_1_base`, `key_2_base`, `base_descr`, `other`) select `base_models_temp`.`others_1`, `base_models_temp`.`key_2_base`, `base_models_temp`.`other_descr`, `base_models_temp`.`other` from `base_models_temp` left join `base_models` on `base_models`.`key_1_base` = `base_models_temp`.`others_1` and `base_models`.`key_2_base` = `base_models_temp`.`key_2_base` where `base_models`.`key_1_base` is null and `base_models`.`key_2_base` is null';
        $stmt5 = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with($expected_sql)->andReturn($stmt5);
        $stmt5->shouldReceive('execute')->once();
        $stmt5->shouldReceive('rowCount')->once()->andReturn(1);
        $this->assertEquals(
            [1, 2],
            $sut->insertOrUpdateViaTemp(
                [
                    ['others_1' => 'k1-1', 'key_2_base' => 'k2-1', 'other_descr' => '1 descr', 'other' => '1 other'],
                    ['others_1' => 'k1-2', 'key_2_base' => 'k2-2', 'other_descr' => '2 descr', 'other' => '2 other'],
                ],
                ['others_1' => 'key_1_base', 'key_2_base'],
                ['others_1' => 'key_1_base', 'key_2_base', 'other_descr' => 'base_descr', 'other'],
                ['other_descr' => 'base_descr', 'other']
            )
        );
    }

    public function testTemp(): void
    {
        [$sut, $conn, $grammar, $process, $pdo] = $this->getModel(BaseModel::class);
        $stmt = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with(
            'select * from information_schema.tables where table_schema = ? and table_name = ? and table_type = \'BASE TABLE\''
        )->andReturn($stmt);
        $stmt->shouldReceive('setFetchMode')->once();
        $stmt->shouldReceive('bindValue')->once()->with(1, '', 2);
        $stmt->shouldReceive('bindValue')->once()->with(2, 'base_models_temp', 2);
        $stmt->shouldReceive('execute')->once();
        $stmt->shouldReceive('fetchAll')->once()->andReturn([]);
        $expected_sql = 'CREATE TABLE `base_models_temp` (LIKE `base_models`)';
        $stmt2 = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with($expected_sql)->andReturn($stmt2);
        $stmt2->shouldReceive('execute')->once();
        $stmt2->shouldReceive('rowCount')->once()->andReturn(1);
        $this->assertEquals('base_models_temp', $sut->temp());
    }

    public function testTempExists(): void
    {
        [$sut, $conn, $grammar, $process, $pdo] = $this->getModel(BaseModel::class);
        $stmt = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with(
            'select * from information_schema.tables where table_schema = ? and table_name = ? and table_type = \'BASE TABLE\''
        )->andReturn($stmt);
        $stmt->shouldReceive('setFetchMode')->once();
        $stmt->shouldReceive('bindValue')->once()->with(1, '', 2);
        $stmt->shouldReceive('bindValue')->once()->with(2, 'base_models_temp', 2);
        $stmt->shouldReceive('execute')->once();
        $stmt->shouldReceive('fetchAll')->once()->andReturn([['table_name' => 'base_models_temp']]);
        $expected_sql = 'CREATE TABLE `base_models_temp` (LIKE `base_models`)';
        $pdo->shouldReceive('prepare')->never()->with($expected_sql);
        $stmt2 = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with('truncate table `base_models_temp`')->andReturn($stmt2);
        $stmt2->shouldReceive('execute')->once();
        $this->assertEquals('base_models_temp', $sut->temp());
    }

    public function testUpdateFromComplex(): void
    {
        [$sut, $conn, $grammar, $process, $pdo] = $this->getModel(BaseModel::class);
        $expected_sql = 'update `base_models` inner join `base_models_temp` on `base_models_temp`.`key_1_base` = `base_models`.`key_1_base` and `base_models_temp`.`key_2_base` = `base_models`.`key_2_base` set `base_models`.`base_descr` = `base_models_temp`.`base_descr`, `base_models`.`other` = `base_models_temp`.`other`';
        $stmt = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with($expected_sql)->andReturn($stmt);
        $stmt->shouldReceive('execute')->once();
        $stmt->shouldReceive('rowCount')->once()->andReturn(1);
        $this->assertEquals(
            1,
            $sut->newQuery()->updateFromComplex(
                'base_models_temp',
                ['key_1_base', 'key_2_base'],
                ['base_descr', 'other']
            )
        );
    }

    public function testUpdateFromComplexMixedKey(): void
    {
        [$sut, $conn, $grammar, $process, $pdo] = $this->getModel(BaseModel::class);
        $expected_sql = 'update `base_models` inner join `others` on `others`.`others_1` = `base_models`.`key_1_base` and `others`.`key_2_base` = `base_models`.`key_2_base` set `base_models`.`base_descr` = `others`.`others_descr`, `base_models`.`other` = `others`.`other`';
        $stmt = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')->once()->with($expected_sql)->andReturn($stmt);
        $stmt->shouldReceive('execute')->once();
        $stmt->shouldReceive('rowCount')->once()->andReturn(1);
        $this->assertEquals(
            1,
            $sut->newQuery()->updateFromComplex(
                'others',
                ['others_1' => 'key_1_base', 'key_2_base'],
                ['others_descr' => 'base_descr', 'other']
            )
        );
    }

    protected function getConnectionMocks()
    {
        $pdo = m::mock(PDOStub::class);
        $grammar = new MySqlGrammar();
        $processor = new MySqlProcessor();
        $connection = m::mock(new MySqlConnection($pdo));
        $conn = m::mock('Illuminate\Database\ConnectionResolverInterface');
        $conn->shouldReceive('getQueryGrammar')->andReturn($grammar);
        $conn->shouldReceive('getPostProcessor')->andReturn($processor);
        $conn->shouldReceive('connection')->andReturn($connection);
        return [$conn, $grammar, $processor, $pdo];
    }

    protected function getModel($cls)
    {
        [$conn, $grammar, $process, $pdo] = $this->getConnectionMocks();
        $cls::setConnectionResolver($conn);
        return [new $cls, $conn, $grammar, $process, $pdo];
    }
}
