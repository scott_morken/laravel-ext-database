<?php

namespace Smorken\Ext\Database\Tests\Integration;

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/12/17
 * Time: 7:20 AM
 */

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Ext\Database\Oci8Connection;
use Smorken\Ext\Database\Query\Grammars\Oci8Grammar;
use Smorken\Ext\Database\Tests\Integration\Stubs\BaseModel;
use Smorken\Ext\Database\Tests\Integration\Stubs\RelatedModel;
use Smorken\Ext\Database\Tests\Unit\PDOStub;
use Yajra\Oci8\Query\Processors\OracleProcessor;

class OracleSqlTest extends TestCase
{

    public function tearDown(): void
    {
        m::close();
    }

    public function testCompositeBelongsToRelation()
    {
        [$related] = $this->getModel(RelatedModel::class);
        $rm1 = $related->newInstance([
            'id' => 2, 'base_model_id' => 1, 'key_1_related' => 'foo', 'key_2_related' => 'bar',
            'related_descr' => 'related m 1',
        ]);
        $sql = $rm1->compositeBelongsToRelation()->toSql();
        $this->assertEquals('select * from "BASE_MODELS" where (("BASE_MODELS"."KEY_1_BASE" = ? and "BASE_MODELS"."KEY_2_BASE" = ?))',
            $sql);
    }

    public function testCompositeHasManyRelations()
    {
        [$base] = $this->getModel(BaseModel::class);
        $bm1 = $base->newInstance([
            'id' => 1, 'key_1_base' => 'foo', 'key_2_base' => 'bar', 'base_descr' => 'base m 1',
        ]);
        $sql = $bm1->compositeHasManyRelations()->toSql();
        $this->assertEquals('select * from "RELATED_MODELS" where (("RELATED_MODELS"."KEY_1_RELATED" = ? and "RELATED_MODELS"."KEY_2_RELATED" = ?))',
            $sql);
    }

    public function testSimpleBelongsToRelation()
    {
        [$related] = $this->getModel(RelatedModel::class);
        $rm1 = $related->newInstance([
            'id' => 2, 'base_model_id' => 1, 'key_1_related' => 'foo', 'key_2_related' => 'bar',
            'related_descr' => 'related m 1',
        ]);
        $sql = $rm1->baseModel()->toSql();
        $this->assertEquals('select * from "BASE_MODELS" where "BASE_MODELS"."ID" = ?', $sql);
    }

    public function testSimpleRelations()
    {
        [$base] = $this->getModel(BaseModel::class);
        $bm1 = $base->newInstance([
            'id' => 1, 'key_1_base' => 'foo', 'key_2_base' => 'bar', 'base_descr' => 'base m 1',
        ]);
        $sql = $bm1->simpleRelations()->toSql();
        $this->assertEquals('select * from "RELATED_MODELS" where "RELATED_MODELS"."BASE_MODEL_ID" = ? and "RELATED_MODELS"."BASE_MODEL_ID" is not null',
            $sql);
    }

    protected function getConnectionMocks()
    {
        $grammar = new Oci8Grammar();
        $processor = new OracleProcessor();
        $connection = m::mock(new Oci8Connection(new PDOStub()));
        $conn = m::mock('Illuminate\Database\ConnectionResolverInterface');
        $conn->shouldReceive('getQueryGrammar')->andReturn($grammar);
        $conn->shouldReceive('getPostProcessor')->andReturn($processor);
        $conn->shouldReceive('connection')->andReturn($connection);
        return [$conn, $grammar, $processor];
    }

    protected function getModel($cls)
    {
        [$conn, $grammar, $process] = $this->getConnectionMocks();
        $cls::setConnectionResolver($conn);
        return [m::mock(new $cls), $conn, $grammar, $process];
    }
}

